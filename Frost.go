package main

import (
	"Frost/generate"
	"fmt"
	"os"
	"runtime"

	"github.com/ogier/pflag"
)

func main() {
	// workDir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	workDir, err := os.Getwd()
	if err != nil {
		fmt.Println(err)
		return
	}

	if len(os.Args) < 2 {
		helpMenu()
		return
	}

	var doDebug bool
	pflag.BoolVarP(
		&doDebug, "debug", "d", false,
		"create a debug build; the 'run' command is always 'debug'",
	)

	var doProfile bool
	pflag.BoolVarP(
		&doProfile, "profile", "p", false,
		"add profiling to the build",
	)

	var doCompress bool
	pflag.BoolVarP(
		&doCompress, "compress", "c", true,
		"compress HTML by removing whitespace, and unnecessary quotes and tags",
	)

	var verbose bool
	pflag.BoolVarP(
		&verbose, "verbose", "v", false,
		"print extra information while building",
	)

	var port uint
	pflag.UintVarP(
		&port, "port", "r", generate.DefaultRunPort,
		"the port on which to run the server (used only with the 'run' command)",
	)

	pflag.Parse()

	if verbose {
		fmt.Printf("Working directory: %s\n", workDir)
	}

	var errs []error

	switch cmd := os.Args[1]; cmd {
	case "version":
		fmt.Printf("%s, %s\n", runtime.Version(), version)

	case "init": // Generate files for a new application, page or frag
		fmt.Println("Enter a path to initialize: ")

		var pth string
		_, err := fmt.Scanln(&pth)
		if err != nil {
			fmt.Println(err)
			return
		}

		if err := generate.Init(workDir, pth); err != nil {
			fmt.Printf("Error: %s\n", err)
		}

	case "generate": // Generate files for the new app, but do not compile
		errs = generate.Create(
			workDir,
			doDebug, doProfile, doCompress, true, false, false,
			0,
			version,
		)

	case "build": // Generate files and compile to a binary
		errs = generate.Create(
			workDir,
			doDebug, doProfile, doCompress, true, true, false,
			0,
			version,
		)

	case "run": // Generate files, compile and run immediately

		doDebug = true // 'run' always produces a debug build

		errs = generate.Create(
			workDir,
			doDebug, doProfile, doCompress, true, true, true,
			port,
			version,
		)

	case "test": // Build app and run tests

	case "":
		helpMenu()

	default:
		fmt.Printf("\nUnknown command %q\n", cmd)
		helpMenu()
	}

	for _, err := range errs {
		fmt.Println(err.Error())
	}
}

func helpMenu() {
	fmt.Println("Frost component-based, web server framework")
	fmt.Println()
	fmt.Println("Usage:")
	fmt.Println("  Frost <command> [options]")
	fmt.Println()
	fmt.Println("Commands:")
	fmt.Println("  version | init | generate | build | run | test")
	fmt.Println()
	fmt.Println("Options:")
	pflag.PrintDefaults()
}
