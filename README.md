# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

###Documentation###

##Form Validation##

Validation for form fields is provided directly in the HTML via standard HTML5 attributes combined with custom attributes starting with `data-valid-`.

The validation is designed to work both on client and server side. On the client, the standard browser behavior is used when possible and supplemented with JavaScript based validation when needed. All validation is ultimately repeated on the front-end server using the same validation data.

#Associating form controls with validation rules#

A set of validation rules is associated with the proper form control via the form control's `name` attribute. Sometimes form controls are expected to share a `name`. In these cases, the validation found on the first form control with a particular name will be used and a warning will be given if validation rules are found on any other control that has the same `name`.

In the case where a control name has one or more markers, like `<input name="foo{{bar}}">`, if there exists *potential* to have the same name as another control, they are treated as though they do have the same name. These are called *"overlapping names"*.

Examples of overlapping names:

* `name="foobar"`  `name="foo{{insert}}"`
* `name="foobar"`  `name="f{{insert}}r"`
* `name="foobar"`  `name="{{insert}}"`
* `name="f{{insert}}r"`  `name="foo{{insert2}}bar"`
* `name="f{{insert}}r"`  `name="fo{{insert2}}b{{insert3}}{{insert4}}ar"`

In all the above pairs of `name` attributes, they could end up with the same `name` depending on what was supplied at runtime. This is not allowed because we wouldn't know which validation to use.

To get around this restriction, change one of the names such that the ambiguity is impossible.

* `name="foobar"` `name="fooxyz{{insert}}"`

Adding `xyz` in the placeholder with the `{{insert}}` made it impossible to match the first. As such, these names do not overlap.

Form controls must not rely on the original order of the fields in the HTML. This is never guaranteed.

If you load another component that has controls with the same name or overlapping names, they will be treated as though they were all part of the same component with a warning given if any of those controls have provided their own validation rules.

For example, if ComponentA has this:

```
<input name="foobar" maxlength=10>
```

And it loads ComponentB, which has this:

```
<input name="foo{{insert}}" maxlength=30>
```

Your program will not compile because it can not be known if `ComponentB` will be given `bar` in place of `{{insert}}`, and so it's impossible to know how to correctly set up validation.

Changing `ComponentA` to use `name="myfooar"` resolves the conflict.
