package generate

import (
	run_me "Frost/generate/targets/frontend_created_by_run_me_to_hold_files"
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"

	"bitbucket.org/Perelandric/frost/server/types"
)

type jsData struct {
	name    string
	content []byte
}

// CompileJS takes the given JS buffer, combines it with JS library files for
// IE8, Legacy and Modern, and writes the three files to the leadingPath/name.
func compileJS(
	cssLegacyBuf, cssModernBuf *bytes.Buffer,
	jsGlobal []byte,
	leadingPath, rootName string,
	isDebug bool,
) error {

	var setup = []byte(`
const __page = __setup_page()
const __fixes = __setup_fixes(__prelude_fixes)

if (!LEGACY_LIB || document.addEventListener) {
	document.addEventListener("DOMContentLoaded", Frost.loader, false);

} else {
	const ms = 250
	let counter = 10

	setTimeout(function __loader() {
		if (document.body) {
			Frost.loader(null)

		} else if (--counter) {
			setTimeout(__loader, ms)
		}
	}, ms)
}
`)

	data := []jsData{
		{"open", run_me.JS_Open},
		{"utilities", run_me.JS_Utilities},
		{"frost", run_me.JS_Frost},
		{"application", cssModernBuf.Bytes()},
		{"setup", setup},
		{"standard_lib", run_me.JS_Standard_lib},
		{"global", jsGlobal},
	}

	err := doCompileJS(
		leadingPath, rootName, false, isDebug, cssModernBuf, data,
	)
	if err != nil {
		return err
	}

	data = []jsData{
		{"open", run_me.JS_Open},

		{"legacy_es_methods", run_me.JS_Legacy_es_methods},
		{"selector_engine", run_me.JS_Selector_engine},

		{"utilities", run_me.JS_Utilities},
		{"frost", run_me.JS_Frost},
		{"application", cssLegacyBuf.Bytes()},
		{"setup", setup},
		{"standard_lib", run_me.JS_Standard_lib},
		{"global", jsGlobal},

		{"legacy_patches", run_me.JS_Legacy_patches},
	}

	return doCompileJS(
		leadingPath, rootName, true, isDebug, cssLegacyBuf, data,
	)
}

func doCompileJS(
	leadingPath, name string,
	isLegacy, isDebug bool,
	buf *bytes.Buffer,
	data []jsData,
) (err error) {

	const msg = "Compiling %q...\n"

	if isLegacy {
		name += "_legacy.js"
	} else {
		name += "_modern.js"
	}

	pth := filepath.Join(leadingPath, name)
	fmt.Printf(msg, name)

	outVsn := "ECMASCRIPT5"
	compLevel := "ADVANCED"
	if isLegacy {
		outVsn = "ECMASCRIPT3"
		compLevel = "SIMPLE"
	}

	// Write library JS
	//stdIn := bytes.Buffer{}
	stdOut := bytes.Buffer{}
	stdErr := bytes.Buffer{}

	/*********

	These need to be written to temp files in order to get
	meaningful line numbers in errors

	**********/
	fileNames := make([]string, 0, len(data))

	defer func() {
		for _, f := range fileNames {
			if f == "" {
				continue
			}

			if err := os.Remove(f); err != nil {
				fmt.Printf("Error removing temp JS file: %q", f)
			}
		}
	}()

	// TODO: I need to have three modes: `DEV`, `DEBUG`, `DEPLOY`

	if isDebug {
		f, err := os.Create(pth)
		if err != nil {
			return err
		}
		defer f.Close()

		for _, d := range data {
			if _, err = f.Write(d.content); err != nil {
				return err
			}

			if _, err = f.WriteString("\n;\n"); err != nil {
				return err
			}
		}

		f.Close()

		return nil
	}

	// Write Application JS
	for _, d := range data {
		tmpFile, err := ioutil.TempFile("", "frost_"+d.name+"_")
		if err != nil {
			return err
		}

		fileNames = append(fileNames, tmpFile.Name())

		_, err = tmpFile.Write(d.content)
		tmpFile.Close()
		if err != nil {
			return err
		}
	}

	var warnLvl = "DEFAULT"

	//warnLvl = "VERBOSE"

	cmd := exec.Command("java", append([]string{
		"-jar", os.Getenv("CLOSURE"),

		"--env", "browser",
		"--compilation_level", compLevel,
		"--new_type_inf", "true",
		"--warning_level", warnLvl,

		"--define", fmt.Sprintf("DEBUG_MODE=%t", isDebug),
		"--define", fmt.Sprintf("LEGACY_LIB=%t", isLegacy),
		"--define", fmt.Sprintf("DEBUG=%t", isDebug), // for selector engine
		"--define", fmt.Sprintf("LEGACY=%t", isLegacy), // for selector engine

		//			"--output_wrapper", string(run_me.JS_Wrapper),
		"--isolation_mode", "IIFE",

		"--language_in", "ECMASCRIPT_2015",
		"--language_out", outVsn,

		"--js_output_file", pth,
		"--js"}, fileNames...)...,
	)

	cmd.Stdout = &stdOut

	//cmd.Stdin = &stdIn
	cmd.Stderr = &stdErr

	if err := cmd.Run(); err != nil {
		fmt.Println(stdErr.String())
		return err
	}

	if stdOut.Len() != 0 {
		fmt.Println(stdOut.String())
	}

	return nil
}

// MakeCSSTags creates the HTML tags for loading the generated CSS
func MakeCSSTags(name string) types.CSSJsTagData {
	return types.CSSJsTagData{
		Path: fmt.Sprintf(`/res/gen/%s.css`, name),
		Tags: []byte(fmt.Sprintf(`
<![if !IE]><link rel=stylesheet type=text/css href=/res/gen/%s.css><![endif]>
<!--[if IE 9]><link rel=stylesheet type=text/css href=/res/gen/%s_9.css><![endif]-->
<!--[if IE 8]><link rel=stylesheet type=text/css href=/res/gen/%s_8.css><![endif]-->
<!--[if IE 7]><link rel=stylesheet type=text/css href=/res/gen/%s_7.css><![endif]-->
<!--[if lte IE 6]><link rel=stylesheet type=text/css href=/res/gen/%s_6.css><![endif]-->`,
			name, name, name, name, name)),
	}
}

// MakeJSTags creates the HTML tags for loading the generated JS
func MakeJSTags(name string) types.CSSJsTagData {
	return types.CSSJsTagData{
		Path: fmt.Sprintf(`/res/gen/%s.js`, name),
		Tags: []byte(fmt.Sprintf(
			`<script>%s</script>`,
			fmt.Sprintf(string(run_me.JSLoader), "/res/gen/"+name),
		)),
	}
}
