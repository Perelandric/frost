package generate

import (
	"Frost/generate/cssparser"
	"Frost/generate/htmlparser"
	util "Frost/utilities"
	"bytes"
	"encoding/json"
	"fmt"
	"go/ast"
	"os"
	"path"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"

	"bitbucket.org/Perelandric/frost/server/types"
)

const unreachable = "Internal error; unreachable but found: %s"

type sortable []*Component

func (s sortable) Len() int      { return len(s) }
func (s sortable) Swap(i, j int) { s[i], s[j] = s[j], s[i] }
func (s sortable) Less(i, j int) bool {
	if s[i].Config.Order != s[j].Config.Order {
		return s[i].Config.Order < s[j].Config.Order
	}
	return s[i].Config.DisplayName < s[j].Config.DisplayName
}

type jsState uint8

// dataKinds state items for Components
const (
	_jsDataWasChecked = 1 << jsState(iota)
	_childrenHaveJS
	_importsHaveJS
	_hasJsDOMReadyCode
	_hasJsForCSSFixes
)

type processState uint8

const (
	_doHTMLInjection = 1 << processState(iota)
	_doPartials
)

type injData struct {
	Start string `json:"start"`
	End   string `json:"end"`
}

type Component struct {
	g *Generator // Same for all components

	types.BasicCompRequest

	pageRoute string

	ast *ast.File

	Name           string
	substituteName string // for reference in JS

	Markers        []*types.MarkerIndices
	Partials       []*types.MarkerIndices

	imports map[string]*Component

	parent   *Component // Used when setting up pages that allow tail paths
	children []*Component

	Config types.Config `json:"-"`

	mux sync.Mutex

	// Reference to positions/frags to be statically injected
	htmlInjections []types.StaticHTMLInjection__
	HTMLArray      []string

	// Fixed CSS and corresponding JS for this Component
	cssData *cssparser.FixResult

	// Local JS for this Component (including CSS fix code)
	jsLegacyCSSFixes []byte
	jsModernCSSFixes []byte
	jsReadyCode      []byte
	jsLegacyState    jsState
	jsModernState    jsState

	processed processState

	IsTopPage        bool
	HasInitFunc      bool
	HasPartialStruct bool
	HasPersistStruct bool
}

func newComponent(
	g *Generator,
	isPage, isTopPage bool,
	localPath, name string,
	parent *Component,
) (c *Component) {

	c = &Component{
		g: g,
		BasicCompRequest: types.BasicCompRequest{
			IsPage:    isPage,
			RootIndex: 0xFFFF,
		},
		IsTopPage: isTopPage,
		pageRoute: filepath.Clean(localPath),
		Name:      name,

		parent: parent,

		// This is atomic instead of mutex because it may already be locked
		substituteName: strconv.FormatUint(
			uint64(atomic.AddUint32(&g.substituteNameIdx, 1)), 10,
		),
	}

	c.ServerPath = filepath.ToSlash(c.LocalPath())

	// strip away leading `frags` or `pages`
	c.ServerPath = c.ServerPath[5:]

	if !strings.HasSuffix(c.ServerPath, "/") {
		c.ServerPath += "/"
	}

	return c
}

func (c *Component) PageHandle() string {
	return c.Name + c.substituteName
}
func (c *Component) SourcePath() string {
	return c.g.sourcePathWithName(c.LocalPath())
}
func (c *Component) SourcePathWithName(name string) string {
	return filepath.Join(c.SourcePath(), name)
}

func (c *Component) LocalPath() string {
	return c.pageRoute
}
func (c *Component) localPathWithName(n string) string {
	return filepath.Join(c.LocalPath(), n)
}

func (c *Component) RelStaticPath() string {
	var res = ""
	var local = c.LocalPath()
	for len(local) > 0 {
		local = strings.Trim(local, string(filepath.Separator))
		local, _ = filepath.Split(local)
		res += ".." + string(filepath.Separator)
	}
	return filepath.Join(res, "frost")
}

func (c *Component) addAsRoot() {
	c.g.mux.Lock()
	c.g.roots = append(c.g.roots, c)
	c.g.mux.Unlock()
}

func (c *Component) normalizeItems(
	source *[]string, target *[]int, kind string) {

	var items = *source

	for i := 0; i < len(items); i++ {
		var item = strings.TrimSpace(items[i])

		if item == "" {
			items = append(items[0:i], items[i+1:]...) // Remove empty strings
			i--
		} else {
			items[i] = strings.Title(items[i]) // Convert to Title-Case style.
		}
	}

	*source = nil
}

func (c *Component) doProcess(p processState) bool {
	c.mux.Lock()
	defer c.mux.Unlock()

	if c.processed&p == 0 {
		c.processed |= p
		return true
	}
	return false
}

// Mux locking on these isn't needed because JS is processed synchronously
func (c *Component) getJSState(forLegacy bool) jsState {
	if forLegacy {
		return c.jsLegacyState
	}
	return c.jsModernState
}
func (c *Component) setJSState(forLegacy bool, state jsState) {
	if forLegacy {
		c.jsLegacyState |= state
	} else {
		c.jsModernState |= state
	}
}
func (c *Component) unsetJSState(forLegacy bool, state jsState) {
	if forLegacy {
		c.jsLegacyState &^= state
	} else {
		c.jsModernState &^= state
	}
}
func (c *Component) hasJSState(forLegacy bool, state jsState) bool {
	if forLegacy {
		return c.jsLegacyState&state == state
	}
	return c.jsModernState&state == state
}

// RootsLen is needed by the component code template.
func (c *Component) RootsLen() int {
	return len(c.g.roots)
}

func (c *Component) sortAll() {
	if c == nil {
		return
	}
	sort.Sort(sortable(c.children))

	for _, child := range c.children {
		child.sortAll()
	}
}

/// This is the starting point for processing a path into a Component. It does
/// a depth first traversal so that the children are started and fully
/// initialized before the given path is initialized. This is necessary because
/// some operations (sitemap) require initialized children.
func (c *Component) process() error {
	dir, err := os.Open(c.SourcePath())
	if err != nil {
		return err
	}
	defer dir.Close()

	files, err := dir.Readdir(-1)
	if err != nil {
		return err
	}

	if len(files) > 0 {
		//var thisWg = sync.WaitGroup{}

		c.children = make([]*Component, 0, len(files))

		for _, fileInfo := range files {
			if fileInfo.IsDir() {
				//	thisWg.Add(1)

				var name = fileInfo.Name()
				var newChild = newComponent(c.g, true,
					false,
					c.localPathWithName(name),
					name,
					c)

				c.children = append(c.children, newChild)

				/*go*/
				func() { // Immediately (and concurrently) recurse for each child.
					defer func() {
						if r := recover(); r != nil {
							if err == nil {
								err = fmt.Errorf("%s", r)
							}
						}
					}()

					if thisErr := newChild.process(); thisErr != nil {
						if err == nil {
							err = thisErr
						}
						return
					}

					//	thisWg.Done()
				}()

				if err != nil {
					return err
				}
			}
		}
		//	thisWg.Wait()
	}

	// Happens after all descendants (if any) are initialized.
	// Note that some `frags` could still be initializing.
	return c.initializeFromPath()
}

func (c *Component) addImportPath(impPath string) error {
	impPath = filepath.Clean(impPath)

	if _, ok := c.imports[impPath]; ok {
		return nil
	}

	// TODO: Need full checks for circular imports at some point.
	if c.g.sourcePathWithName(impPath) == c.SourcePath() {
		fmt.Printf("Ignoring self import: %q\n", impPath)
		return nil
	}

	if imp, err := c.g.getImport(impPath); err != nil {
		return err

	} else if imp != nil {
		if c.imports == nil {
			c.imports = make(map[string]*Component)
		}

		c.imports[impPath] = imp
	}

	return nil
}

func (c *Component) initializeFromPath() (err error) {
	fmt.Printf("  %s\n", c.LocalPath())

	if err = c.gatherConfig(); err != nil {
		return err
	}

	if c.IsPage {
		c.g.pages = append(c.g.pages, c)
	}

	hasGo := util.FileExists(c.SourcePathWithName(c.Name + ".go"))

	if !hasGo && c.IsPage {
		return fmt.Errorf("No '.go' file found for: %s", c.LocalPath())
	}

	hasHTML := util.FileExists(c.SourcePathWithName(c.Name + ".html"))

	if !hasHTML && !hasGo {
		return nil
	}

	if hasGo && c.g.hasState(doingUnusedFrags) == false {
		// Process the .go file and its imports
		if err := c.getImports(); err != nil {
			return err
		}
	}

	if hasHTML {
		// Process the component's HTML.
		data, wasMod := c.g.persist.dataAndModState(_html,
			c.SourcePathWithName(c.Name+".html"),
		)
		if wasMod {
			c.g.setState(needsRebuild | needsCSSProcess | needsJSProcess)
		}

		node, css, js := htmlparser.Parse(
			string(data),
			c.g.hasState(compressSpaceInHTML),
			c.localPathWithName(c.Name+".html"),
		)

		if c.g.hasState(doingUnusedFrags) == false {
			c.processCSS(css)
			c.processJS(js)
		}

		// The `c.substituteName` gets added as a `data-frost-id` attribute to the
		// first element node.
		addJSFrostID(node, c.IsPage, c.substituteName)

		c.Markers, c.HTMLArray, c.htmlInjections = types.GetMarkerData(node)

		// Make sure the htmlInjections are properly imported
		for _, inj := range c.htmlInjections {
			if err = c.addImportPath(inj.Data); err != nil {
				return err
			}
		}

		if c.g.hasState(doingUnusedFrags) == false {
			const mustHave = "A Component with 'partial application' markers must have %s"

			// Make sure a component with 'partial' markers has the required decls
			for _, m := range c.Markers {
				if m.IsPartial {
					c.HasPartialStruct = c.findTopStruct("Partial") != nil &&
						c.findTopFunc(
							"init",
							typ{"Partial", MAYBE},
							nil,
							typ{"partial", MUST},
						) != nil

					if !c.HasPartialStruct {
						return fmt.Errorf(
							mustHave,
							"a `type Partial struct` with an `init() *partial` method.",
						)
					}

					// Not required but we need to know if it's there.
					c.HasPersistStruct = c.findTopStruct("persist") != nil &&
						c.findTopStruct("Data", typ{"persist", MAYBE}) != nil

					break
				}
			}
		}
	}

	/* Should use this to verify if Data.Make exists
	if c.findTopFunc(
		"Make",
		typ{"Data", MAYBE},
		[]typ{{"Response", MUST}},
		typ{"", NO},
	) == nil {
		return fmt.Errorf(
			mustHave, "a `Make(*frost.Response, *persist)` method on `Data`",
		)
	}
	*/
	return nil
}

func (c *Component) processCSS(css []byte) {
	c.cssData = cssparser.ProcessCSS(
		css, c.substituteName, c.g.resourcesBase(),
	)

	if c.cssData != nil {
		c.jsLegacyCSSFixes, c.jsModernCSSFixes = c.cssData.FixesToJSON()

		if len(c.jsLegacyCSSFixes) != 0 {
			c.setJSState(true, _hasJsForCSSFixes)
		}
		if len(c.jsModernCSSFixes) != 0 {
			c.setJSState(false, _hasJsForCSSFixes)
		}
	}
}

func (c *Component) processJS(js []byte) {
	c.jsReadyCode = bytes.TrimSpace(js)

	if len(c.jsReadyCode) != 0 {
		c.setJSState(true, _hasJsDOMReadyCode)
		c.setJSState(false, _hasJsDOMReadyCode)
	}
}

/*
// JS files that have a `make(r, data)` method get modified so that the method
// is subsumed by a wrapper that has access to HTML/Marker data.
// `makeJsJSON` converts the HTMLArray and Marker Array into a single JS array.
func makeJsJSON(
	htmlArr []string, markers []*types.MarkerIndices,
) (string, error) {

	// Represents each item in the target JS array.
	type jsonData struct {
		MarkerName string `json:"key$"`
		Start      uint16 `json:"start$"`
		End        uint16 `json:"end$"`
		Html       string `json:"html$"`
	}

	var data = make([]*jsonData, len(htmlArr))
	var nextJ = 0 // Avoid reiterating the first part of `markers`

	for i, html := range htmlArr {
		data[i] = &jsonData{
			Html: html,
		}

		// Find the marker for the current HTML item (if any since some HTML items
		// align with marker end points)
		for j, marker := range markers[nextJ:] {
			if i == int(marker.Pair[0]) {
				data[i].MarkerName = marker.Name
				data[i].Start = marker.Pair[0]
				data[i].End = marker.Pair[1]

				nextJ = nextJ + j + 1
				break
			}
		}
	}

	if j, err := json.Marshal(data); err != nil {
		return "", err
	} else {
		return string(j), nil
	}
}
*/

// gatherConfig opens and process the optional `config.json` file for the
// Component. It returns unexpected file errors or errors due to invalid JSON.
func (c *Component) gatherConfig() (err error) {
	data, wasMod := c.g.persist.dataAndModState(
		_config, c.SourcePathWithName("config.json"),
	)
	if wasMod {
		c.g.setState(needsRebuild)
	}

	if len(data) != 0 {
		if err = json.Unmarshal(data, &c.Config); err != nil {
			return err
		}
	}

	if c.IsPage {
		if c.IsTopPage { // The topmost root must always have `IsRoot` set.
			c.Config.IsRoot = true
		}
		if c.Config.IsRoot { // Add `IsRoot` components to the `roots` array.
			c.addAsRoot()
		}
		c.BasicCompRequest.AllowTails = c.Config.AllowTails

		// If there's no DisplayName, create one from the directory name.
		if c.Config.DisplayName == "" {
			c.Config.DisplayName =
				strings.Title(strings.Replace(path.Base(c.ServerPath), "_", " ", -1))

			if c.Config.DisplayName == "." {
				c.Config.DisplayName = "/"
			}
		}
	}

	return nil
}

// Sets the state for if `c` has method data, has children that have method
// data, and has imports that have method data.
func (c *Component) setJSDataState(forLegacy bool) jsState {
	// Avoid checking this Component if it's already been checked
	if c.hasJSState(forLegacy, _jsDataWasChecked) {
		return c.getJSState(forLegacy)
	}
	c.setJSState(forLegacy, _jsDataWasChecked)

	for _, ch := range c.children {
		if ch.setJSDataState(forLegacy) != _jsDataWasChecked {
			c.setJSState(forLegacy, _childrenHaveJS)
		}
	}

	for _, imp := range c.imports {
		if imp.setJSDataState(forLegacy) != _jsDataWasChecked {
			c.setJSState(forLegacy, _importsHaveJS)
		}
	}
	return c.getJSState(forLegacy)
}

// This just clears the _dataChecked bit on all components so that we can do a
// simple `0` comparison to see if it had any useful data.
func (c *Component) clearJSDataChecked(forLegacy bool) {
	if !c.hasJSState(forLegacy, _jsDataWasChecked) {
		return
	}
	c.unsetJSState(forLegacy, _jsDataWasChecked)

	for _, ch := range c.children {
		ch.clearJSDataChecked(forLegacy)
	}

	for _, imp := range c.imports {
		imp.clearJSDataChecked(forLegacy)
	}
}

func (c *Component) doInjectionsAndPartials(wg *sync.WaitGroup) {
	c.fragsThenPages(_doHTMLInjection, (*Component).doInjections)
	c.fragsThenPages(_doPartials, (*Component).doPartials)
	wg.Done()
}

func (c *Component) fragsThenPages(state processState, cb func(c *Component)) {
	for _, frag := range c.imports {
		if frag.doProcess(state) {
			frag.fragsThenPages(state, cb)
		}
	}

	cb(c)

	// Start its children immediately in separate goroutines
	for _, ch := range c.children {
		if !ch.Config.IsRoot {
			ch.fragsThenPages(state, cb)
		}
	}
}

func (c *Component) doInjections() {
	// Inject the HTML into the appropriate index locations
	for _, inj := range c.htmlInjections {
		// Grab target fragment, and copy its HTML
		fragToInj, ok := c.imports[filepath.Clean(inj.Data)]

		if !ok {
			panic(fmt.Sprintf(
				"Internal error; HTMLInjection should have been imported: %q",
				filepath.Clean(inj.Data),
			))
		}

		// Used to align marker names with their HTML array index.
		fragMrkrIdxToName := map[uint16]*types.MarkerIndices(nil)

		// Holds supplied data for markers (if any)
		mrkrDataToInj := map[string]injData{}

		if len(inj.MarkersToFill) > 0 {
			temp := map[string]json.RawMessage{}

			// Marker data was supplied, so unmarshal it
			err := json.Unmarshal([]byte(inj.MarkersToFill), &temp)

			if err != nil {
				panic(fmt.Sprintf(
					"Internal error; Should have been caught while parsing HTML: %q",
					err,
				))
			}

			// Data for each marker may be a string for the start position, and
			// object with "start" and "end", or an array with 2 indices for the
			// start and end positions.
			for k, v := range temp {
				mrkrDataToInj[k] = rawMessageToStartEnd(v)
			}

			// Map injected Component markers by HtmlArray index to marker Name
			fragMrkrIdxToName = make(map[uint16]*types.MarkerIndices)
			for _, m := range fragToInj.Markers {
				fragMrkrIdxToName[m.Pair[0]] = m
				fragMrkrIdxToName[m.Pair[1]] = m
			}
		}

		htmlToInj := fragToInj.HTMLArray

		for i, html := range htmlToInj {
			// Inject supplied data for marker.
			mrkrForIndex := fragMrkrIdxToName[uint16(i)]

			if mrkrForIndex != nil {
				dataToInj := mrkrDataToInj[mrkrForIndex.Name]
				pair := mrkrForIndex.Pair

				if pair[0] == uint16(i) { // Is start pos
					c.HTMLArray[inj.LocalIndex] += dataToInj.Start
				}

				// No distinct end, or we're at the end, so add any end content
				if pair[0] == pair[1] || pair[1] == uint16(i) {
					c.HTMLArray[inj.LocalIndex] += dataToInj.End
					delete(mrkrDataToInj, mrkrForIndex.Name)
				}
			}

			c.HTMLArray[inj.LocalIndex] += html
		}

		for k, v := range mrkrDataToInj {
			fmt.Printf("Static injection found unknown marker %q with %q\n", k, v)
		}
	}

	c.removeStaticInjectionGaps()
}

func rawMessageToStartEnd(d json.RawMessage) (res injData) {
	switch d[0] {
	case '{': // Assume the props are "start" and "end". Ignore others.
		temp := struct{ Start, End json.RawMessage }{}

		if err := json.Unmarshal(d, &temp); err != nil {
			panic(fmt.Sprintf(unreachable, err))
		}
		res.Start = rawMessageToPos(temp.Start)
		res.End = rawMessageToPos(temp.End)

	case 'n': // Must be `null`, so return empty strings

	case '"':
		res.Start = rawMessageToPos(d)

	default: // Anything else simply gets its JSON injected into the start pos
		res.Start = string(d)
	}

	return res
}

func rawMessageToPos(d json.RawMessage) (s string) {
	if len(d) == 0 { // Unmarshaling didn't produce anything for this position
		return
	}

	// TODO: Anything strictly nonsensical here (Array or Object) should
	// panic in DEBUG mode.

	switch d[0] {
	case 'n':
		// Must be `null`, so return an empty string

	case '"':
		if err := json.Unmarshal(d, &s); err != nil {
			panic(fmt.Sprintf(unreachable, err))
		}

	default: // Anything else keeps is representation
		s = string(d)
	}

	return s
}

func (c *Component) removeStaticInjectionGaps() {
	// Join the adjacent HTML segments together and decrement all indices greater
	// than the index of the injection.
	for i := len(c.htmlInjections) - 1; i > -1; i-- {
		inj := c.htmlInjections[i]
		injIdx := inj.LocalIndex

		if injIdx+1 >= len(c.HTMLArray) {
			continue
		}

		c.HTMLArray[injIdx] += c.HTMLArray[injIdx+1]
		c.HTMLArray = append(c.HTMLArray[0:injIdx+1], c.HTMLArray[injIdx+2:]...)
	}

	// Decrease all markers to match the mutated Array
	for i := len(c.htmlInjections) - 1; i > -1; i-- {
		injIdx := c.htmlInjections[i].LocalIndex

		for _, m := range c.Markers {
			if m.Pair[0] > uint16(injIdx) {
				m.Pair[0]--
			}
			if m.Pair[1] > uint16(injIdx) {
				m.Pair[1]--
			}
		}
	}
}

func (c *Component) doPartials() {
	for i := len(c.Markers) - 1; i > -1; i-- {
		if !c.Markers[i].IsPartial {
			continue
		}

		p := c.Markers[i]

		// Add it to the Partials, and remove it from the Markers
		c.Partials = append([]*types.MarkerIndices{p}, c.Partials...)
		c.Markers = append(c.Markers[0:i], c.Markers[i+1:]...)

		// A Partial is removed from the htmlArray since its dynamic content will be
		// joined with the HTML before and after, so adjust subsequent indices.
		for j := i; j < len(c.Markers); j++ {
			m := c.Markers[j]
			if m.Pair[0] > p.Pair[0] {
				m.Pair[0]--
			}
			if m.Pair[1] > p.Pair[0] {
				m.Pair[1]--
			}
		}
	}
}

// Generates the necessary component-specific code file.
func (c *Component) createGoCompFile() error {

	var b bytes.Buffer

	if err := generateComponent(&b, c); err != nil {
		return err
	}

	// For now we write to the build target and also the source for debugging.
	var pth = c.SourcePathWithName("generated_____file.go")

	f, err := os.Create(pth)
	if err != nil {
		return err
	}
	defer f.Close()

	if _, err = f.Write(b.Bytes()); err != nil {
		return err
	}

	return nil
}

/*
 * CSS Accumulation
 */

func (c *Component) writeCSS(cf cssFiles) {
	if c.cssData == nil {
		return
	}
	for name, css := range c.cssData.CSSByVersion {
		if len(css) == 0 {
			continue
		}

		_, err := fmt.Fprintf(cf[name], cssStr, c.LocalPath(), css)
		if c.g.isError(err) {
			return
		}
	}
}

func (c *Component) accumulateCSSFrags(cf cssFiles, vis map[string]bool) {
	for impPath, frag := range c.imports {
		if !vis[impPath] {
			vis[impPath] = true
			frag.writeCSS(cf)
			frag.accumulateCSSFrags(cf, vis)
		}
	}

	// Do the imported frags for each child page
	for _, child := range c.children {
		if child.Config.IsRoot == false {
			child.accumulateCSSFrags(cf, vis)
		}
	}
}

func (c *Component) accumulateCSSPages(cf cssFiles) {
	c.writeCSS(cf)

	for _, child := range c.children {
		if child.Config.IsRoot == false {
			child.accumulateCSSPages(cf)
		}
	}
}

/*
 * JS Accumulation
 */

/*
 * Returns `false` if nothing was written
 */
func (c *Component) writeJSData(
	buf *bytes.Buffer, indent, name string, isPage, isLegacy, doComma bool,
) {
	if doComma {
		buf.WriteString(",\n")
	}

	// Start the object ctor, and write its ID
	var key = ""
	if !c.Config.IsRoot {
		key = fmt.Sprintf("%q:", name)
	}

	var kind = "Comp"
	if isPage {
		kind = "Page"
	}
	fmt.Fprintf(buf, "%s%snew Frost%s( // %s\n%s%s",
		indent, key, kind, c.LocalPath(),
		indent+_Indent, c.substituteName,
	)

	// Write imports, if any
	fmt.Fprintf(buf, ",\n%s", indent+_Indent)

	if c.hasJSState(isLegacy, _importsHaveJS) {
		found := false
		for _, imp := range c.imports {
			if imp.getJSState(isLegacy) == 0 {
				continue
			}

			if found {
				buf.WriteByte(',')
			} else {
				buf.WriteByte('[')
				found = true
			}
			buf.WriteString(imp.substituteName)
		}

		buf.WriteByte(']')

	} else {
		buf.WriteString("null")
	}

	// Write code, if any
	fmt.Fprintf(buf, ",\n%s", indent+_Indent)
	if c.hasJSState(isLegacy, _hasJsDOMReadyCode) {
		fmt.Fprintf(buf, "()=>{\n%s\n}", c.jsReadyCode)
	} else {
		buf.WriteString("null")
	}

	// Write CSS fixes, if any
	fmt.Fprintf(buf, ",\n%s", indent+_Indent)
	if c.hasJSState(isLegacy, _hasJsForCSSFixes) {
		buf.WriteString("/**@type{!FrostCSSFixByCatByVersion}*/")

		if isLegacy {
			fmt.Fprintf(buf, "(%s)", c.jsLegacyCSSFixes)
		} else {
			fmt.Fprintf(buf, "(%s)", c.jsModernCSSFixes)
		}
	} else {
		buf.WriteString("null")
	}
}

/*
 * Returns `true` if at least one fragment was written
 */
func (c *Component) accumulateJSFrags(
	buf *bytes.Buffer, vis map[string]bool, isLegacy, doComma bool,
) bool {

	if c.getJSState(isLegacy) == 0 {
		return doComma
	}

	if c.hasJSState(isLegacy, _importsHaveJS) {
		for importPath, frag := range c.imports {
			if vis[importPath] {
				continue
			}
			vis[importPath] = true

			if frag.getJSState(isLegacy) == 0 { // Skip entries with nothing useful
				continue
			}

			frag.writeJSData(
				buf, _Indent, frag.substituteName, false, isLegacy, doComma,
			)
			doComma = true

			fmt.Fprintf(buf, ",\n%snull\n)", _Indent)

			frag.accumulateJSFrags(buf, vis, isLegacy, doComma)
		}
	}

	if c.hasJSState(isLegacy, _childrenHaveJS) {
		// Do the imported frags for each child page
		for _, child := range c.children {
			if child.Config.IsRoot {
				continue
			}
			doComma = child.accumulateJSFrags(buf, vis, isLegacy, doComma)
		}
	}

	return doComma
}

func (c *Component) accumulateJSPages(
	buf *bytes.Buffer, indent string, isLegacy, doComma bool,
) bool {
	if c.getJSState(isLegacy) == 0 {
		if c.Config.IsRoot {
			buf.WriteString("{}")
		}
		return doComma
	}

	c.writeJSData(buf, indent, c.Name, true, isLegacy, doComma)
	doComma = true

	if c.hasJSState(isLegacy, _childrenHaveJS) {
		newIndent := indent + _Indent
		fmt.Fprintf(buf, ",\n%s {\n", newIndent)

		chDoComma := false

		for _, child := range c.children {
			if child.Config.IsRoot {
				continue
			}

			// Safe because the concurrent CSS setup doesn't touch `RootIndex`
			child.RootIndex = c.RootIndex // This is safe
			chDoComma = child.accumulateJSPages(
				buf, newIndent+_Indent, isLegacy, chDoComma,
			)
		}

		fmt.Fprintf(buf, "\n%s}", newIndent)
	}

	fmt.Fprintf(buf, "\n%s)", indent)

	return doComma
}

// Build code files for any remaining frags that were never imported. This helps
// with debugging unused components.
func (c *Component) processRemainingFrags(isRoot bool) (errs []error) {

	dir, err := os.Open(c.SourcePath())
	if err != nil {
		return append(errs, err)
	}
	defer dir.Close()

	files, err := dir.Readdir(-1)
	if err != nil {
		return append(errs, err)
	}

	if len(files) > 0 {
		/*
			var thisWg = sync.WaitGroup{}
			var errsMux sync.Mutex
		*/

		for _, fileInfo := range files {
			if fileInfo.IsDir() {
				//	thisWg.Add(1)

				var newChild = newComponent(c.g, false,
					false,
					c.localPathWithName(fileInfo.Name()),
					fileInfo.Name(),
					nil,
				)
				/*go*/ func() { // Immediately (and concurrently) recurse for each child.
					var newerrs []error

					defer func() {
						if r := recover(); r != nil {
							newerrs = []error{fmt.Errorf("%s", r)}
						}

						//	errsMux.Lock()
						errs = append(errs, newerrs...)
						//	errsMux.Unlock()

						//	thisWg.Done()
					}()

					newerrs = newChild.processRemainingFrags(false)
				}()
			}
		}

		//thisWg.Wait()
	}

	// Happens after all descendants (if any) are initialized.
	if !isRoot {
		c.g.mux.Lock()
		_, ok := c.g.frags[c.LocalPath()]

		if ok == false {
			c.g.frags[c.LocalPath()] = c
		}
		c.g.mux.Unlock()

		if ok == false {
			if err := c.initializeFromPath(); err != nil {
				errs = append(errs, err)

			} else if err = c.createGoCompFile(); err != nil {
				errs = append(errs, err)
			}
		}
	}
	return errs
}

/*
// Consider mangling paths just a bit when all characters are ASCII chars
// by combining every two characters into one JS char.
// This means I'd be flattening the `pages` structure out and referencing
// each by its full (mangled) path.

const n = "/foo/bar/hubba/baz/bubba.html"

function mangle(s) {
  return s.split("").reduce((res, c, i) =>
    i%2 ? res : res + String.fromCharCode(
    	c.charCodeAt() | (~~s.charCodeAt(i+1) << 8)
    )
  , "")
}

function unmangle(s) {
  return s.split("").reduce((res, c) =>
    res + String.fromCharCode((c=c.charCodeAt())&0xFF, (c&0xFF00)>>8)
  , "").slice(0, (s.length<<1)-!(s.charCodeAt(s.length-1) & 0xFF00))
}

const enc = mangle(n)
const dec = unmangle(enc)

// 是潯戯牡栯扵慢戯穡戯扵慢栮浴l /foo/bar/hubba/baz/bubba.html true
console.log(enc, dec, n == dec)



// Or simple mapping to a different alphabet, like Devanagari

const n = "/foo/bar/hubba/baz/bubba.html"

function mangle(s) {
  return String.fromCharCode(...s.split("").map(c=>c.charCodeAt()|0x900))
}

function unmangle(s) {
  return String.fromCharCode(...s.split("").map(c=>c.charCodeAt()&0xFF))
}

const enc = mangle(n)
const dec = unmangle(enc)

// य०९९यॢॡॲय२ॵॢॢॡयॢॡॺयॢॵॢॢॡम२ॴ७६ /foo/bar/hubba/baz/bubba.html true
console.log(enc, dec, n == dec)
*/
