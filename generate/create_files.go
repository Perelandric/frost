package generate

import (
	"bytes"
	"fmt"
	"go/ast"
	"go/format"
	"go/parser"
	"go/token"
	"io"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"text/template"

	"bitbucket.org/Perelandric/frost/server/types"
)

// Analyze the source for the given target component, and return its imports.
func (c *Component) getImports() error {
	name := c.Name

	goPath := c.SourcePathWithName(name + ".go")

	_, wasMod := c.g.persist.dataAndModState(_code, goPath)
	if wasMod {
		c.g.setState(needsRebuild)
	}

	file, err := parser.ParseFile(token.NewFileSet(), goPath, nil, 0)
	if file == nil {
		fmt.Printf("No Go code found at %q\n", goPath)
		return nil // A nil slice with no error means no code was found
	}

	if err != nil {
		fmt.Printf("Parse errors found in %q\n", goPath)
	}

	if file.Name.Name != name {
		return fmt.Errorf("expected package %q; found %q", name, file.Name.Name)
	}

	c.ast = file

	// Gather import strings.
	for _, imp := range c.ast.Imports {
		impPath, err := strconv.Unquote(imp.Path.Value)
		if err != nil {
			fmt.Println(err)
			return nil
		}

		c.addImportPath(impPath)
	}

	c.checkInitFunc()

	return nil
}

func (c *Component) checkInitFunc() {
	if c.ast == nil {
		return
	}

	f := c.findTopFunc("Init", typ{}, nil, typ{})
	if f == nil {
		return
	}

	c.HasInitFunc =
		f.Recv == nil &&
			f.Body != nil &&
			f.Name.Name == "Init" &&
			f.Type.Func != token.NoPos &&
			len(f.Type.Params.List) == 0 &&
			f.Type.Results == nil
}

type requireLevel uint8

const (
	NO = requireLevel(1) + iota
	MAYBE
	MUST
)

type typ struct {
	name           string
	requirePointer requireLevel
}

// Find a top level function declaration in the AST
func (c *Component) findTopFunc(
	name string,
	receiver typ,
	params []typ,
	returnType typ,
) *ast.FuncDecl {
	if c.ast == nil {
		return nil
	}

	for _, d := range c.ast.Decls {
		f, ok := d.(*ast.FuncDecl)
		if !ok || f.Name.Name != name {
			continue
		}
		if (f.Recv == nil) != (len(receiver.name) == 0) {
			continue
		}

		if f.Recv != nil {
			// Check if function recever matches any given receiver names
			for _, field := range f.Recv.List {
				if matchesIdent(field.Type, receiver) {
					goto CHECK_RETURN
				}
			}
			continue
		}

	CHECK_RETURN:
		if (f.Type.Results == nil) != (len(returnType.name) == 0) {
			continue
		}
		if f.Type.Results == nil {
			return f
		}
		if matchesIdent(f.Type.Results.List[0].Type, returnType) {
			return f
		}
	}

	return nil
}

func matchesIdent(exp ast.Expr, id typ) bool {
	switch t := exp.(type) {
	case *ast.Ident:
		if id.requirePointer != MUST {
			return t.Name == id.name
		}

	case *ast.StarExpr:
		if id.requirePointer == MAYBE || id.requirePointer == MUST {
			return matchesIdent(t.X, typ{id.name, MAYBE}) // The MUST has been satisfied
		}
	}
	return false
}

// Find a top level struct type declaration in the AST
func (c *Component) findTopStruct(name string, fields ...typ) *ast.StructType {
	if c.ast == nil {
		return nil
	}

	for _, d := range c.ast.Decls {
		g, ok := d.(*ast.GenDecl)
		if !ok || g.Tok != token.TYPE {
			continue
		}

		for _, s := range g.Specs {
			ts, ok := s.(*ast.TypeSpec)
			if !ok || ts.Name.Name != name {
				continue
			}

			st, ok := ts.Type.(*ast.StructType)
			if !ok {
				continue
			}

			if (len(fields) == 0) && (st.Fields == nil || len(st.Fields.List) == 0) {
				return st // No fields to check, so we're good
			}

		FIELDS: // This struct must match, so if any field fails, it all fails.
			for _, f := range fields {
				for _, stf := range st.Fields.List {
					if matchesIdent(stf.Type, f) {
						continue FIELDS
					}
				}
				return nil // No match found for the current field
			}
			return st // All the fields (if any) matched.
		}
	}
	return nil
}

func generateComponent(w io.Writer, c *Component) error {
	var buf bytes.Buffer
	if err := tmpl.Execute(&buf, c); err != nil {
		return err
	}

	// Run the Go code formatter to make sure syntax is correct before writing.
	b, err := format.Source(buf.Bytes())
	if err != nil {
		return err
	}

	_, err = w.Write(b)
	return err
}

var tmpl = template.Must(template.New("go_target").Funcs(template.FuncMap{
	"Title": strings.Title,
}).Parse(`package {{.Name}}

{{$self := .}}
{{$hasMarkers := gt (len .Markers) 0}}
{{$hasPartials := gt (len .Partials) 0}}


import (
	{{- if (or .IsPage $hasMarkers $hasPartials)}}
	frost "bitbucket.org/Perelandric/frost/server"
	{{- end}}

	{{- if .IsPage}}
	"bitbucket.org/Perelandric/frost/server/types"
	{{- end}}
)

var ____html_array = [][]byte {
{{range $html := .HTMLArray -}}
	{{printf "[]byte(%q)," $html}}
{{end}}
}

func (Data) Initialize____() [][]byte {
	return ____html_array
}


{{if .IsPage}}
func init() {
	frost.SetupPageHandler____(
		handleRequest,
		{{printf "%#v" .BasicCompRequest}},
	)
}
{{end}}



{{if $hasMarkers}}

// Markers
var marker = struct {
	{{range $m := .Markers}}
	{{$m.Name}} frost.Marker
	{{end}}
} {
	{{range $m := .Markers}}
	{{- $m.Name}}: frost.NewMarker____({{- index $m.Pair 0}}, {{index $m.Pair 1}}),
	{{end}}
}

{{end}}



{{if $hasPartials}}
// If Data is used directly as a Component with no partial application, it
// needs to use an array of HTML that has had empty values applied.
var ____orig_html_for_partials = ____html_array

func init() { // Because we can't do a reassignment at package level
	____html_array = frost.ApplyPartial____(
		____orig_html_for_partials,

		{{- range $p := .Partials}}
		frost.ApplyData____ {M: [2]uint16{ {{index $p.Pair 0}}, {{index $p.Pair 1}} }, V: frost.Apply{} },
		{{- end}}
	)
}

type partial struct {

	{{- if .HasPersistStruct}}
	persist *persist
	{{- end}}

	{{- range $p := .Partials}}
	{{$p.Name}} frost.ApplyData
	{{- end}}
}


type Applied struct {
	{{- if .HasPersistStruct}}
	____persist *persist
	{{end -}}

  ____htmlArray [][]byte
}

func Apply(p *Partial) (a Applied) {
	pp := p.init()

	{{if .HasPersistStruct}}
	a.____persist = pp.persist
	{{end}}

	a.____htmlArray = frost.ApplyPartial____(
		____orig_html_for_partials,

		{{- range $p := .Partials}}
		frost.ApplyData____ {M: [2]uint16{ {{index $p.Pair 0}}, {{index $p.Pair 1}} }, V: pp.{{$p.Name}} },
		{{- end}}
	)

	return a
}


// The Applied type can itself be used as a Component. As such, it will simply
// produce the original HTML with the partially applied content.
func (a Applied) Initialize____() [][]byte {
	return a.____htmlArray
}

func (Applied) Make(*frost.Response) {
	// Nothing to do here. The HTML will be written in full.
}

func (a Applied) Complete(d *Data) frost.Component {
	{{if .HasPersistStruct -}}
	d.persist = a.____persist
	{{end -}}

  return Completed{d, a.____htmlArray}
}


type Completed struct {
  ____d *Data
	____htmlArray [][]byte
}

func (c Completed) Initialize____() [][]byte {
  return c.____htmlArray
}

func (c Completed) Make(r *frost.Response) {
  c.____d.Make(r)

	{{- if .HasPersistStruct}}
	c.____d.persist = nil
	{{- end}}
}

{{end}}



type AppliedGroup struct {
	____htmlArrays [][]byte
	{{- if .HasPersistStruct}}
	____persist []*persist
	{{- end}}
	____eachLen uint16
}

func ApplyAll(partials ...*Partial) (ag AppliedGroup) {
	if len(partials) == 0 {
		ag.____eachLen = 1 // Avoids division by zero

	} else {
		{{if .HasPersistStruct -}}
		ag.____persist = make([]*persist, 0, len(partials))
		{{end -}}

		for _, p := range partials {
			a := Apply(p)
			if ag.____eachLen == 0 {
				ag.____eachLen = uint16(len(a.____htmlArray))
			}

			ag.____htmlArrays = append(ag.____htmlArrays, a.____htmlArray...)

			{{- if .HasPersistStruct}}
			ag.____persist = append(ag.____persist, a.____persist)
			{{- end}}
		}
	}

	return ag
}


func (ag AppliedGroup) Complete(d ...*Data) frost.Component {
	return CompletedGroup{d, ag}
}
func (ag AppliedGroup) CompleteFunc(fn func(int) *Data) frost.Component {
	return CompletedGroupFunc{fn, ag}
}
func (ag AppliedGroup) CompleteFuncWith(
	fn func(int) (*Data, frost.Component),
) frost.Component {
	return CompletedGroupFuncWith{fn, ag}
}



type CompletedGroup struct {
  ____ds []*Data
	AppliedGroup
}
type CompletedGroupFunc struct {
  ____fn func(int) *Data
	AppliedGroup
}
type CompletedGroupFuncWith struct {
  ____fn func(int) (*Data, frost.Component)
	AppliedGroup
}

func (CompletedGroup) Initialize____() [][]byte {
  return nil
}
func (CompletedGroupFunc) Initialize____() [][]byte {
  return nil
}
func (CompletedGroupFuncWith) Initialize____() [][]byte {
  return nil
}

func (c CompletedGroup) Make(r *frost.Response) {
	data := c.____ds

	if len(data) == 0 {
		return
	}
	{{if .HasPersistStruct}}
	pers := c.____persist
	{{end}}

	r.CompleteAppliedFunc____(
		func(i int) frost.Component {
			if i < len(data) {

				{{- if .HasPersistStruct -}}
				if data[i] != nil {
					data[i].persist = pers[i]
				}
				{{end -}}

				return data[i]
			}
			return nil
		},
		c.____htmlArrays,
		c.____eachLen,
	)
}

func (c CompletedGroupFunc) Make(r *frost.Response) {
	fn := c.____fn
	if fn == nil {
		return
	}

	{{- if .HasPersistStruct}}
	pers := c.____persist
	{{- end}}

	r.CompleteAppliedFunc____(
		func(i int) frost.Component {
			{{if .HasPersistStruct -}}

			d := fn(i)
			if d != nil {
				d.persist = pers[i]
			}
			return d

			{{- else -}}

			return fn(i)

			{{- end}}
		},
		c.____htmlArrays,
		c.____eachLen,
	)
}

func (c CompletedGroupFuncWith) Make(r *frost.Response) {
	fn := c.____fn
	if fn == nil {
		return
	}

	{{- if .HasPersistStruct}}
	pers := c.____persist
	{{- end}}

	r.CompleteAppliedFuncWith____(
		func(i int) (frost.Component, frost.Component) {
			{{if .HasPersistStruct -}}

			d, j, w := fn(i)
			if d != nil {
				d.persist = pers[i]
			}
			return d, j, w

			{{- else -}}

			return fn(i)

			{{- end}}
		},
		c.____htmlArrays,
		c.____eachLen,
	)
}



{{end}}

{{end}}

`))

func (ge *Generator) createMainGoFile() error {
	// Process sitemap data
	var makeMap func(sm *types.FullSiteMapNode, comp *Component)

	makeMap = func(sm *types.FullSiteMapNode, comp *Component) {
		sm.SiteMapNode = comp.Config.SiteMapNode
		sm.Children = make([]*types.FullSiteMapNode, 0, len(comp.children))
		sm.Path = comp.ServerPath

		for _, ch := range comp.children {
			var newNode = &types.FullSiteMapNode{}

			sm.Children = append(sm.Children, newNode)

			makeMap(newNode, ch)
		}
	}

	var root = ge.getTopRoot()
	var sitemap types.FullSiteMapNode

	makeMap(&sitemap, root)

	return ge.MainFile()
}

var mainfileTmpl = template.Must(template.New("main_file").Parse(`
package main

import (
	"log"
	"runtime"
	"strings"
	{{ if .DoProf -}}
	_ "net/http/pprof"
	{{- end }}
	"fmt"
	frost "bitbucket.org/Perelandric/frost/server"
	"bitbucket.org/Perelandric/frost/server/types"

	// Page imports
	{{- range $comp := .Pages}}
	{{- $locPath := $comp.LocalPath}}
	{{- if $comp.HasInitFunc}}
	{{printf "%s %q" $comp.PageHandle $locPath}}
	{{- else}}
	_ {{printf "%q" $locPath}}
	{{- end}}
	{{- end}}

	// Frag imports
	{{- range $comp := .Frags}}
	{{- if $comp.HasInitFunc}}
	{{- $locPath := $comp.LocalPath}}
	{{printf "%s %q" $comp.PageHandle $locPath}}
	{{- end}}
	{{- end}}
)

func main() {
	log.Fatalf("Server exiting: %s\n", frost.Start())
}

{{.DoSiteMap}}

func init() {
	frost.SetResourceTags____({{printf "%#v" .CSSJsTags}})

	fmt.Println("\nFrost server framework")
	fmt.Printf("Built with Go %s\n", strings.TrimLeft(runtime.Version(), "go"))

	fmt.Println("\nAdding routes...")

	fmt.Println()

	{{- range $frag := .Frags}}
	{{- if $frag.HasInitFunc}}
	{{$frag.PageHandle}}.Init()
	{{- end}}
	{{- end}}

	{{- range $page := .Pages}}
	{{- if $page.HasInitFunc}}
	{{$page.PageHandle}}.Init()
	{{- end}}
	{{- end}}
}
`))

func (ge *Generator) MainFile() error {
	var tmpMain, err = ioutil.TempFile(ge.sourcePath(), "main_")
	if err != nil {
		return err
	}
	defer tmpMain.Close()

	var name = tmpMain.Name()
	ge.tempMainPath = name + ".go"

	if err = mainfileTmpl.Execute(tmpMain, ge); err != nil {
		return err
	}

	return os.Rename(name, ge.tempMainPath)
}

type ThisComponent struct {
	*Component
}

func (ge *Generator) DoSiteMap() string {
	return fmt.Sprintf(`

func init() {
	%s
}
`, genSiteMapNode(ThisComponent{ge.getTopRoot()}))
}

func genSiteMapNode(tcomp ThisComponent) string {
	var funcMap = template.FuncMap{
		"genNode": genSiteMapNode,
	}

	var sitemapTmpl = template.Must(template.New("sitemap").Funcs(funcMap).Parse(
		`frost.NewSiteMapNode____(
			{{printf "%t" .IsTopPage}},
			[]*frost.SiteMapNode{
				{{- range $c := .GetThisChildren}}
				{{genNode $c}},
				{{- end}}
			},
			{{printf "%q" .ServerPath}},
			{{printf "%q" .Config.DisplayName}},
			{{printf "%q" .Config.Description}},
		)`))

	var b bytes.Buffer
	if err := sitemapTmpl.Execute(&b, tcomp); err != nil {
		panic(fmt.Errorf("Internal error: %s", err))
	}

	return b.String()
}

func (tcomp ThisComponent) GetThisChildren() []ThisComponent {
	var c = tcomp.children

	var res = make([]ThisComponent, len(c))
	for i, child := range c {
		res[i] = ThisComponent{child}
	}
	return res
}
