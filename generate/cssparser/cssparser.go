package cssparser

/*
#cgo CFLAGS: -I${SRCDIR}/lib
#cgo CFLAGS: -I$PONYHOME/src/libponyrt/
#cgo LDFLAGS: ${SRCDIR}/lib/libcss_fix.a
#cgo LDFLAGS: -L$PONYHOME/build/release -lponyrt
#cgo LDFLAGS: -lpthread
#cgo LDFLAGS: -lssl
#cgo LDFLAGS: -lcrypto
#cgo LDFLAGS: -ldl
#cgo LDFLAGS: -lm

#include "stdlib.h"
#include "pony.h"
#include "lib/css_fix.h"
*/
import "C"
import (
	"bytes"
	"encoding/json"
	"regexp"
	"strings"
	"unsafe"
)

type FixResult struct {
	CSSByVersion    map[string]string
	FixesByCategory byOptCategory
}

type fixPtrResult struct {
	std unsafe.Pointer
	ie9 unsafe.Pointer
	ie8 unsafe.Pointer
	ie7 unsafe.Pointer
	ie6 unsafe.Pointer

	std_js unsafe.Pointer
	ie9_js unsafe.Pointer
	ie8_js unsafe.Pointer
	ie7_js unsafe.Pointer
	ie6_js unsafe.Pointer
}

// ProcessCSS calls into the Pony CSS processing library and generates files
// with CSS compatibility fixes added.
func ProcessCSS(source []byte, id, res_path string) *FixResult {
	source = bytes.TrimSpace(source)

	if len(source) == 0 {
		return nil
	}

	// Putting argc/argv at the package level scope causes an unrelated,
	// unexported variable in the htmlparser package to be `nil`.
	const argc = 1
	var argv = [argc]*C.char{C.CString("css_fix")}

	C.pony_init(argc, (**C.char)(&argv[0]))

	var inp = C.CString(string(source))
	var res = C.CString(res_path)
	var pfx = C.CString("cssfix-" + id + "-")

	defer func() { // Manually free the strings.
		for _, c := range [...]*C.char{inp, res, pfx} {
			C.free(unsafe.Pointer(c))
		}
	}()

	var t = C.FFIFixCSS_Alloc()

	C.pony_become(C.pony_ctx(), (*C.struct_pony_actor_t)(t))

	var ptr_result = (*fixPtrResult)(unsafe.Pointer(
		C.FFIFixCSS_box_from_string_to_strings_oooobo(
			t,
			inp, // css text
			res, // path to resources directory
			pfx, // prefix for css fix class disambigutation
			C.AmbientAuth_Alloc(),
			C.bool(true), // compress
		),
	))

	toRes := &FixResult{
		CSSByVersion:    map[string]string{},
		FixesByCategory: byOptCategory{},
	}

	toRes.addCSSByVersion(map[string]string{
		"std": C.GoString((*C.char)(ptr_result.std)),
		"ie9": C.GoString((*C.char)(ptr_result.ie9)),
		"ie8": C.GoString((*C.char)(ptr_result.ie8)),
		"ie7": C.GoString((*C.char)(ptr_result.ie7)),
		"ie6": C.GoString((*C.char)(ptr_result.ie6)),
	})

	toRes.addFixesByVersion(map[string]string{
		"std": C.GoString((*C.char)(ptr_result.std_js)),
		"ie9": C.GoString((*C.char)(ptr_result.ie9_js)),
		"ie8": C.GoString((*C.char)(ptr_result.ie8_js)),
		"ie7": C.GoString((*C.char)(ptr_result.ie7_js)),
		"ie6": C.GoString((*C.char)(ptr_result.ie6_js)),
	})

	C.pony_start(false, false, nil)

	if !toRes.hasCSS() && !toRes.hasFixes() {
		return nil
	}
	return toRes
}

// TODO: I should have only IE9 and STD included in the "modern" library.
// How to do that? Do all 5 for the time being, then figure it out

type actualData struct {
	Category string `json:"category,omitempty"`
	Cls      string `json:"cls"`
	Full     string `json:"full"`
}

type byOptCategory map[string]map[string][]*actualData

func (fr *FixResult) addCSSByVersion(other map[string]string) {
	for v, css := range other {
		css = strings.TrimSpace(css)
		if len(css) > 0 {
			fr.CSSByVersion[v] += css
		}
	}
}

func (fr *FixResult) addFixesByVersion(other map[string]string) {
	// Organize the fixes by category for each version (Std, IE9, etc)
	for vsn, jsn := range other {
		var data []*actualData
		if err := json.Unmarshal([]byte(jsn), &data); err != nil {
			panic(err)
		}

		for _, item := range data {
			byVers, ok := fr.FixesByCategory[item.Category]
			if !ok {
				byVers = map[string][]*actualData{}
				fr.FixesByCategory[item.Category] = byVers
			}

			item.Category = "" // So that it's not included in the final JSON
			byVers[vsn] = append(byVers[vsn], item)
		}
	}
}

func (fr *FixResult) hasFixes() bool {
	return len(fr.FixesByCategory) > 0
}

func (fr *FixResult) hasCSS() bool {
	return len(fr.CSSByVersion) > 0
}

func (fr *FixResult) clearLegacyFixes() {
	for c, byVsn := range fr.FixesByCategory {
		delete(byVsn, "ie8")
		delete(byVsn, "ie7")
		delete(byVsn, "ie6")

		if len(byVsn) == 0 {
			delete(fr.FixesByCategory, c)
		}
	}
}

var reUnquote = regexp.MustCompile(`"\w+":`)

func replacer(m []byte) []byte {
	m[len(m)-2] = ':'
	return bytes.ToLower(m[1 : len(m)-1])
}

func (fr *FixResult) FixesToJSON() ([]byte, []byte) {
	if !fr.hasFixes() {
		return nil, nil
	}

	legacyCSSFix, err := json.Marshal(fr.FixesByCategory)
	if err != nil {
		panic(err)
	}

	legacyCSSFix = reUnquote.ReplaceAllFunc(legacyCSSFix, replacer)

	fr.clearLegacyFixes()

	if !fr.hasFixes() {
		return legacyCSSFix, nil
	}

	modernCSSFix, err := json.Marshal(fr.FixesByCategory)
	if err != nil {
		panic(err)
	}

	modernCSSFix = reUnquote.ReplaceAllFunc(modernCSSFix, replacer)

	return legacyCSSFix, modernCSSFix
}
