package generate

import (
	"archive/tar"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"time"

	"Frost/generate/cssparser"
	"strconv"
	"strings"
	"sync"

	"bitbucket.org/Perelandric/frost/server/types"

	util "Frost/utilities"
)

// State items for Generator
type stateItem int

const (
	doDebug = stateItem(1 << iota)
	needsRebuild
	needsCSSProcess
	needsJSProcess
	doingUnusedFrags
	compressSpaceInHTML
	doProf
	runServer
)

// Generator holds the data for the target application being built.
type Generator struct {
	CSSJsTags []struct{ CSS, Js types.CSSJsTagData }

	tempMainPath string // temporary file that holds the main Go execution source

	workPath string
	persist  persistData

	buildTime time.Time

	pages []*Component

	mux sync.Mutex

	// Holds all components designated as `IsRoot`
	roots []*Component

	// Holds the prelude CSS and JS that are included for each root's CSS/JS
	preludeCSS          *cssparser.FixResult
	preludeCSSFixLegacy []byte
	preludeCSSFixModern []byte
	preludeJS           []byte

	errors []error

	// Hold a reference to each imported fragment by its import path. Ensures that
	// a frag imported from multiple components doesn't get rebuilt.
	frags map[string]*Component

	// Each component gets a unique name (index) for use in JS code.
	substituteNameIdx uint32 // `0` is reserved for global resources.
}

func (g *Generator) isError(err error) bool {
	if err != nil {
		g.mux.Lock()
		g.errors = append(g.errors, err)
		g.mux.Unlock()
		return true
	}
	return false
}

// Pages is used by the templates
func (g *Generator) Pages() []*Component {
	return g.pages
}

// Frags is used by the templates
func (g *Generator) Frags() []*Component {
	res := make([]*Component, 0, len(g.frags))
	for _, c := range g.frags {
		res = append(res, c)
	}
	return res
}

// DoProf is used by the templates
func (g *Generator) DoProf() bool {
	return g.hasState(doProf)
}

func (g *Generator) doPersistData() error {
	g.mux.Lock()
	defer g.mux.Unlock()

	g.persist.removeStale(g.buildTime)
	return g.persist.storeTo(g.persistDataPath())
}

func (g *Generator) doRestoreData(version string) {
	var persisted persistData

	// Get and unmarshal the persistData from the .frost directory. If it isn't
	// found or something else goes wrong, just continue on.
	if f, err := os.Open(g.persistDataPath()); err == nil {
		defer f.Close()

		if data, err := ioutil.ReadAll(f); err == nil {
			err = json.Unmarshal(data, &persisted)

			if err == nil && persisted.FrostVersion == version {
				goto RECOVERED
			}

			if err != nil {
				fmt.Println("persistData was corrupted")
			}
		}
	}

	// Force a rebuild
	_ = os.RemoveAll(g.persistDataPath())
	_ = os.RemoveAll(g.tarPath())
	g.setState(needsRebuild | needsCSSProcess | needsJSProcess)

RECOVERED:
	g.persist.FrostVersion = version

	if persisted.SourceMap == nil {
		g.persist.SourceMap = make(map[string]*buildData)
	} else {
		g.persist.SourceMap = persisted.SourceMap
	}

	debug := g.hasState(doDebug)
	prof := g.hasState(doProf)
	compress := g.hasState(compressSpaceInHTML)

	// Certain changes in state from the last build may force a rebuild of the
	// app, the resources, or both.
	if debug != (persisted.State&doDebug == doDebug) {
		g.setState(needsRebuild | needsCSSProcess | needsJSProcess)
	}
	if prof != (persisted.State&doProf == doProf) {
		g.setState(needsRebuild)
	}
	if compress != (persisted.State&compressSpaceInHTML == compressSpaceInHTML) {
		g.setState(needsRebuild)
	}
}

func (g *Generator) hasState(stateItem stateItem) bool {
	g.mux.Lock()
	defer g.mux.Unlock()
	return g.persist.State&stateItem != 0
}
func (g *Generator) setState(stateItem stateItem) {
	g.mux.Lock()
	defer g.mux.Unlock()
	g.persist.State |= stateItem
}

// getImport grabs the fragment component from the full collection of fragments.
// If it hasn't been created yet, it creates and initializes it. It assumes the
// `impPath` string has already been Clean()'d, and it ignores paths not rooted
// in the `frags` directory of the application.
//
func (g *Generator) getImport(impPath string) (*Component, error) {
	// We use `filepath.Clean()` probably more than needed, just to be sure that
	// we have consistency in getting and setting the fragment.
	impPath = filepath.Clean(impPath)

	g.mux.Lock()

	imp, ok := g.frags[impPath]

	if ok == false &&
		strings.HasPrefix(impPath, util.Frags+string(filepath.Separator)) {
		// Create and store the new import while we're locked.
		imp = newComponent(g, false, false, impPath, filepath.Base(impPath), nil)

		g.frags[impPath] = imp

	} else { // Ignore imports not located in `frags/` directory.
		ok = true
	}

	// Unlock before initialization because there's no mutation of other comps
	// in this phase of the build. So components can initialize in parallel.
	g.mux.Unlock()

	if ok == false {
		if err := imp.initializeFromPath(); err != nil {
			return nil, err
		}
	}

	return imp, nil
}

func (g *Generator) getTopRoot() *Component {
	for _, root := range g.roots {
		if root.IsTopPage {
			return root
		}
	}
	return nil
}

func (g *Generator) sourcePath() string {
	return filepath.Join(g.workPath, "vendor")
}
func (g *Generator) sourcePathWithName(name string) string {
	return filepath.Join(g.sourcePath(), name)
}

func (g *Generator) frostPath(name string) string {
	return filepath.Join(g.workPath, ".frost", name)
}
func (g *Generator) persistDataPath() string {
	return g.frostPath("persistData")
}
func (g *Generator) binaryPath() string {
	return g.frostPath("server")
}
func (g *Generator) generatedPath() string {
	return g.frostResPath("gen")
}
func (g *Generator) frostResPath(name string) string {
	return filepath.Join(g.frostPath("res"), name)
}
func (g *Generator) frostTempServerPath(name string) string {
	return filepath.Join(g.frostPath("temp_server"), name)
}

func (g *Generator) sourcePagesPath() string {
	return g.sourcePathWithName(util.Pages)
}
func (g *Generator) sourceFragsPath() string {
	return g.sourcePathWithName(util.Frags)
}
func (g *Generator) sourceAppPath() string {
	return g.sourcePathWithName(util.App)
}

func (g *Generator) resourcesBase() string {
	return g.workPath
}
func (g *Generator) resourcesPath() string {
	return filepath.Join(g.workPath, "res")
}

func (g *Generator) tarPath() string {
	const tarFile = "server.tar"

	if g.hasState(runServer) {
		return g.frostPath(tarFile)
	}
	return filepath.Join(g.workPath, tarFile)
}

// Create begins the processing of the comnponents in `workPath` and outputs the
// built application to `outputPath` for the given `lang`
func Create(
	workPath string,
	debug, prof, compress, doCSSJSMain, doBuild, doRun bool,
	port uint,
	version string,
) (errs []error) {

	g := &Generator{
		workPath:  workPath,
		buildTime: time.Now(),
		frags:     make(map[string]*Component),
	}

	fmt.Print("Generating ")

	if debug {
		g.setState(doDebug)
		fmt.Print("DEBUG")
	} else {
		fmt.Print("DEPLOYMENT")
	}

	fmt.Println(" files")

	if prof {
		g.setState(doProf)
	}
	if compress {
		g.setState(compressSpaceInHTML)
	}
	if doRun {
		g.setState(runServer)
	}

	g.doRestoreData(version)

	var err error

	// Make hidden `.frost` directory, if needed.
	if err = os.MkdirAll(g.frostPath(""), util.DirPerms); g.isError(err) {
		return g.errors
	}

	// If there's no current server.tar found, a build will be needed.
	if !util.FileExists(g.tarPath()) {
		g.setState(needsRebuild)
	}

	// If there's no `generated` directory, or if it's empty, CSS/JS are needed.
	exist, empty := util.DirExistsDirEmpty(g.generatedPath())
	if !exist || empty {
		g.setState(needsCSSProcess | needsJSProcess)
	}

	fmt.Println("\nProcessing components...")

	topRoot := newComponent(g, true, true, util.Pages, util.Pages, nil)

	if err = topRoot.process(); g.isError(err) {
		return g.errors
	}
	topRoot.sortAll()

	// Verify no nested tail route paths, and set indicator when a normal
	// route has a nested route that allows tails.

	for _, p := range g.pages {
		if !p.AllowTails {
			continue
		}

		// Set its parent routes as subRoutesAllowTails
		for pp := p.parent; pp != nil; pp = pp.parent {
			if pp.AllowTails {
				const msg = "The 'AllowTails' config option may not be nested.\n%s\n%s"
				g.isError(fmt.Errorf(msg, pp.ServerPath, p.ServerPath))
				return g.errors
			}
			if pp.SubRoutesAllowTails {
				break
			}
			pp.SubRoutesAllowTails = true
		}
	}

	if doCSSJSMain {
		if err = g.buildPageResources(); g.isError(err) {
			return g.errors
		}

		if g.hasState(needsRebuild) {
			if err = g.createMainGoFile(); g.isError(err) {
				return g.errors
			}
		}
	}

	if g.hasState(needsRebuild) {
		if err = g.createGoFilesForComps(); g.isError(err) {
			return g.errors
		}
	}

	fmt.Println("\nProcessing unused fragments...")

	var fragroot = newComponent(g, false, false, util.Frags, util.Frags, nil)
	g.setState(doingUnusedFrags)

	// Process any remaining fragments that were not imported
	if errs := fragroot.processRemainingFrags(true); len(errs) > 0 {
		fmt.Println("\nFound errors in unused frags...")

		for _, e := range errs {
			fmt.Println(e)
		}
	}

	fmt.Println("")

	if err = g.doPersistData(); g.isError(err) {
		return g.errors
	}
	if !doBuild {
		return nil
	}

	// Compile the application and bundle its resources
	if g.hasState(needsRebuild) {
		if err = g.buildServer(); g.isError(err) {
			return g.errors
		}
	} else {
		fmt.Println("Server already built.")
	}

	if err = g.tarServerFiles(); g.isError(err) {
		return g.errors
	}

	if g.hasState(runServer) && g.isError(g.runServer(port)) {
		return g.errors
	}

	return nil
}

func (g *Generator) buildServer() (err error) {
	fmt.Print("Building server... ")

	// Get the environment for the build command, adding the project's workpath
	env := os.Environ()
	origpath := os.Getenv("GOPATH")

	if len(origpath) == 0 {
		env = append(env, g.workPath)
	} else {
		env = append(env, g.workPath+":"+origpath)
	}

	args := []string{"build"}
	if g.hasState(doDebug) {
		args = append(args, "-tags", "debug")
	}
	args = append(args,
		//"-a", // TODO: Should run only in its own mode
		//"-v", // TODO: Should run only in its own mode; how do we capture output?
		"-gcflags", "'-l=4'",
		"-o", g.binaryPath(),
		g.tempMainPath,
	)
	defer os.Remove(g.tempMainPath)

	cmd := exec.Command("go", args...)
	cmd.Env = env

	if err = cmd.Run(); err != nil {
		return err
	}

	fmt.Println("Done")
	return nil
}

// DefaultRunPort is the default port for the 'run' command.
const DefaultRunPort = uint(8080)

func (g *Generator) runServer(port uint) (err error) {
	if port == 0 {
		port = DefaultRunPort
	}

	tempPath := g.frostTempServerPath("")
	if err = os.RemoveAll(tempPath); err != nil && !os.IsNotExist(err) {
		return err
	}
	if err = os.Mkdir(tempPath, util.DirPerms); err != nil {
		return err
	}

	if err = util.Untar(g.tarPath(), tempPath); err != nil {
		return err
	}

	if err = os.Remove(g.tarPath()); err != nil {
		return err
	}

	cmd := exec.Command(
		filepath.Join(tempPath, "server", "server"),
		"--port", strconv.FormatUint(uint64(port), 10),
		"--datapath", filepath.Join(tempPath, "server"),
	)

	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	fmt.Println()
	fmt.Println(
		"NOTE: The 'run' command always runs in DEBUG mode. Do not use for production",
	)

	fmt.Println()
	fmt.Printf("Running DEBUG server on localhost:%d\n", port)
	fmt.Println("--------------------------------------")

	w, err := g.initWatcher()
	if err != nil {
		return err
	}
	defer w.Close()

	if err = cmd.Start(); err != nil {
		return err
	}

	runChan := make(chan error)
	txtChan := make(chan string)

	go func() {
		runChan <- cmd.Wait()
	}()

	go func() {
		time.Sleep(1 * time.Second)
		fmt.Println()
		txtChan <- ""

		for {
			input := ""
			fmt.Scanln(&input)
			txtChan <- input
		}
	}()

	throttled := false

	for {
		select {
		case err = <-runChan:
			close(txtChan)
			close(runChan)
			return err

		case input := <-txtChan:
			if strings.ToLower(strings.TrimSpace(input)) == "exit" {
				fmt.Println()
				fmt.Println("Exiting server.")

				go func() {
					runChan <- cmd.Process.Kill()
				}()
				continue
			}

		case err = <-w.Errors: // Handled below

		case evt := <-w.Events:
			err, throttled = g.handleWatchEvent(evt, w)
			if throttled {
				continue
			}
		}

		if err != nil {
			if killErr := cmd.Process.Kill(); killErr != nil {
				fmt.Println(killErr)
			}
			return err
		}

		fmt.Println("\nType 'exit' to stop the server")
	}
}

// Create TAR holding all the server data
func (g *Generator) tarServerFiles() (err error) {
	fmt.Print("Creating tarball...")
	defer func() {
		if err == nil {
			fmt.Println("Done")
		}
	}()

	tarPath := g.tarPath()

	tarFile, err := os.Create(tarPath)
	if err != nil {
		return err
	}
	defer tarFile.Close()

	tarWriter := tar.NewWriter(tarFile)
	defer tarWriter.Close()

	servPth := g.binaryPath()

	info, err := os.Stat(servPth)
	if err != nil {
		return err
	}

	if err = util.WriteFileToTar(
		tarWriter,
		info,
		filepath.Dir(servPth),
		filepath.Base(servPth),
		false,
	); err != nil {
		return err
	}

	// Do NOT remove the directory ------------------------v
	if err = util.WriteDirToTar(tarWriter, g.resourcesPath(), false); err != nil {
		return err
	}

	return util.WriteDirToTar(tarWriter, g.frostResPath(""), false)
}

func addJSFrostID(node *types.GenericRoot, isPage bool, subName string) {
	var firstElem func(n types.ParentI) *types.Element

	firstElem = func(n types.ParentI) *types.Element {
		for _, node := range n.GetChildNodes() {
			switch nn := node.(type) {
			case *types.Element:
				return nn
			case types.ParentI:
				if res := firstElem(nn); res != nil {
					return res
				}
			}
		}

		return nil
	}

	if elem := firstElem(node); elem != nil {
		if isPage {
			elem.SimpleAddAttr("data-frost-page", subName)
		} else {
			elem.SimpleAddAttr("data-frost-frag", subName)
		}
	}
}

const _Indent = "  "
const cssStr = `
/** %s **/
%s
`

func (g *Generator) processPreludeCSSAndJS() (err error) {
	data, mod := g.persist.dataAndModState(
		_css, g.sourcePathWithName(filepath.Join("prelude", "prelude.css")),
	)

	if mod {
		g.setState(needsCSSProcess)
	}
	preludeCSS := bytes.TrimSpace(data)

	if len(preludeCSS) != 0 { // Parse the prelude CSS
		g.preludeCSS = cssparser.ProcessCSS(preludeCSS, "0", g.resourcesPath())
		g.preludeCSSFixLegacy, g.preludeCSSFixModern = g.preludeCSS.FixesToJSON()
	}

	data, mod = g.persist.dataAndModState(
		_js, g.sourcePathWithName(filepath.Join("prelude", "prelude.js")),
	)

	if mod {
		g.setState(needsJSProcess)
	}
	g.preludeJS = bytes.TrimSpace(data)

	if len(g.preludeJS) != 0 { // Make sure we have leading/trailing semicolons
		g.preludeJS = append(append(
			[]byte{';', '\n'}, g.preludeJS...), ';', '\n',
		)
	}
	return nil
}

func (g *Generator) buildPageResources() (err error) {
	if err = g.processPreludeCSSAndJS(); err != nil {
		return err
	}

	g.CSSJsTags = make([]struct{ CSS, Js types.CSSJsTagData }, len(g.roots))

	// Build CSS/JS tags for each root
	for i, root := range g.roots {
		if root.RootIndex != 0xFFFF { // Don't set if it's already set
			continue
		}
		root.RootIndex = uint16(i)

		name := strconv.Itoa(int(root.RootIndex))

		g.CSSJsTags[root.RootIndex] = struct{ CSS, Js types.CSSJsTagData }{
			CSS: MakeCSSTags(name),
			Js:  MakeJSTags(name),
		}
	}

	genPath := g.generatedPath()

	// Make sure the directory for the generated resources exists.
	if err = os.MkdirAll(genPath, os.ModePerm); err != nil {
		return err
	}

	mainWg := sync.WaitGroup{}

	needCSS := g.hasState(needsCSSProcess)
	needJs := g.hasState(needsJSProcess)

	if !needCSS && !needJs {
		fmt.Println("\nCSS and JS were unmodified.")

	} else {
		fmt.Println("\nBuilding CSS and JS...")

		// Remove `.css` and/or `.js` files that are going to be generated.
		f, err := os.Open(genPath)
		if err != nil {
			return err
		}
		defer f.Close()

		names, err := f.Readdirnames(-1)
		if err != nil {
			return err
		}

		for _, name := range names {
			if (needCSS && strings.HasSuffix(name, ".css")) ||
				(needJs && strings.HasSuffix(name, ".js")) {

				if err = os.Remove(filepath.Join(genPath, name)); err != nil {
					return err
				}
			}
		}

		// First set the state of each component, indicating if they have any JS
		// code, imports that have code (recursively) or children that have code
		// (recursively). When they have none, they can be elided.
		topRoot := g.getTopRoot()

		topRoot.setJSDataState(true)
		topRoot.setJSDataState(false)
		topRoot.clearJSDataChecked(true)
		topRoot.clearJSDataChecked(false)

		// Accumulate CSS and JS for each root
		for _, root := range g.roots {
			name := strconv.Itoa(int(root.RootIndex))

			mainWg.Add(2)

			go g.doCSSForRoot(root, genPath, name, &mainWg)
			go g.doJsForRoot(root, genPath, name, &mainWg)
		}
	}

	if g.hasState(needsRebuild) {
		mainWg.Add(len(g.roots))

		for _, r := range g.roots {
			go r.doInjectionsAndPartials(&mainWg)
		}
	}

	mainWg.Wait()

	return nil
}

// Used when putting the fixed CSS together into their final files
type cssFiles map[string]*os.File

func (g *Generator) doCSSForRoot(
	root *Component, genPath, name string, wg *sync.WaitGroup,
) {
	var err error
	cf := cssFiles{}
	vsns := [...]string{"std", "ie9", "ie8", "ie7", "ie6"}

	// Write prelude CSS first
	for _, v := range vsns {
		n := v
		if n == "std" {
			n = ""
		}
		cf[v], err = os.Create(filepath.Join(genPath, name+n+".css"))
		if g.isError(err) {
			return
		}
		defer cf[v].Close()

		if g.preludeCSS == nil {
			continue
		}

		css := g.preludeCSS.CSSByVersion[v]
		if len(css) == 0 {
			continue
		}

		fmt.Fprintf(cf[v], cssStr, "prelude", css)
	}

	root.accumulateCSSFrags(cf, make(map[string]bool))
	root.accumulateCSSPages(cf)

	wg.Done()
}

func (g *Generator) doJsForRoot(
	root *Component, genPath, name string, wg *sync.WaitGroup,
) {

	var cssLegacyBuf, cssModernBuf bytes.Buffer
	var thisWg sync.WaitGroup

	for _, data := range [...]struct {
		isLegacy     bool
		preludeFixes []byte
		buf          *bytes.Buffer
	}{
		{true, g.preludeCSSFixLegacy, &cssLegacyBuf},
		{false, g.preludeCSSFixModern, &cssModernBuf},
	} {
		thisWg.Add(1)
		d := data

		go func() {

			var prelude = bytes.TrimSpace(d.preludeFixes)
			if len(prelude) == 0 {
				prelude = []byte("{}")
			}

			fmt.Fprintf(
				d.buf,
				"\nconst __prelude_fixes = /**@type{!FrostCSSFixByCatByVersion}*/(%s);",
				prelude,
			)

			fmt.Fprint(d.buf, "\n\nconst __pages = /**@type{!FrostPage}*/ (")
			root.accumulateJSPages(d.buf, _Indent, d.isLegacy, false)

			d.buf.WriteString(");\n\nconst __frags = /**@type{!FrostCompObj}*/ ({")
			root.accumulateJSFrags(d.buf, make(map[string]bool), d.isLegacy, false)
			d.buf.WriteString("\n});\n")

			thisWg.Done()
		}()
	}

	thisWg.Wait()

	if err := compileJS(
		&cssLegacyBuf,
		&cssModernBuf,
		append(g.preludeJS, ';'),
		genPath,
		name,
		g.hasState(doDebug),
	); g.isError(err) {
		return
	}

	wg.Done()
}

func (g *Generator) createGoFilesForComps() error {
	for _, r := range g.pages {
		if err := r.createGoCompFile(); err != nil {
			return err
		}
	}

	for _, f := range g.frags {
		if f.IsPage { // Page fragments will have been already handled above
			continue
		}
		if err := f.createGoCompFile(); err != nil {
			return err
		}
	}

	return nil
}
