package htmlparser

import (
	"bytes"
	"encoding/json"
	"fmt"
	re "regexp"
	"strings"

	frost "bitbucket.org/Perelandric/frost/server"
	"bitbucket.org/Perelandric/frost/server/types"
)

const _invalidMarker = "Invalid marker %q in %q\n"

// Regexps
var reConsecutiveSpace = re.MustCompile(`\s+`)
var reConsecutiveOuterSpace = re.MustCompile(`(?:^\s+|\s+$)`)

type state struct {
	nodeStack        []types.ParentI
	currNode         types.ParentI
	lex              *lexer
	filePath         string
	frostCSS         []byte
	frostJs          []byte
	compress         bool
	originalCompress bool
}

func (s *state) addStack(node types.NodeI) types.ParentI {
	s.nodeStack = append(s.nodeStack, s.currNode)
	s.currNode = node.(types.ParentI)
	return node.(types.ParentI)
}

func (s *state) popStack() {
	var lastIdx = len(s.nodeStack) - 1

	if lastIdx >= 0 {
		s.currNode = s.nodeStack[lastIdx]
		s.nodeStack = s.nodeStack[0:lastIdx]

	} else {
		panic(fmt.Sprintf("Unexpected empty node stack: %s", s.filePath))
	}
}

// Parse parsed the given `data` into an AST specific to the project's internal
// needs.
func Parse(
	data string, compressSpace bool, fpth string,
) (*types.GenericRoot, []byte, []byte) {

	var s = state{
		lex: newLexer(
			strings.Replace(data, "\u0000", "", -1), // Drop NULLs
		),
		filePath:         fpth,
		frostCSS:         nil,
		frostJs:          nil,
		compress:         compressSpace,
		originalCompress: compressSpace,
	}

	var root = &types.GenericRoot{
		ParentNode: types.ParentNode{
			ChildNodes: []types.NodeI{},
		},
	}

	s.currNode = root

	var tok *token

	for {
		s.addTextNode(textUntilAny(s.lex, "<{"), s.compress, false)

		tok = nextToken(s.lex)

		switch tok.kind {
		case _tokEOF:
			return root, s.frostCSS, s.frostJs

		case _tokText, _tokOpenTagEnd, _tokOpenTagSelfClosingEnd:
			s.addTextNode(tok.String(), s.compress, false)

		case _marker, // Try a marker
			_markerPartialApplication,
			_markerStaticInjection:

			if !s.tryMarker(tok, nil, false, false) {
				fmt.Printf(_invalidMarker, tok.data, s.filePath)
			}

		case _tokHTMLComment:
			if s.compress == false {
				s.addComment(tok.data)
			}

		case _tokDoctype:
			s.addDoctype(tok.data)

		case _tokOpenTagStart: // TODO: I'm not handling namespace tags
			s.tryElement(tok.data)

		case _tokCloseTag:
			upperName := strings.ToUpper(tok.data)

			// If the closing tag didn't match anything useful, we just ignore it.
			if elem, ok := s.currNode.(*types.Element); ok {
				currName := strings.ToUpper(elem.Name)

				if currName == upperName {
					if currName == "PRE" {
						s.compress = s.originalCompress
					}
					s.popStack()

				} else {
					fmt.Printf(
						"File: %s\n  Closing %q but found %q\n",
						s.filePath, upperName, currName,
					)
				}

			} else {
				fmt.Printf(
					"File: %s\n  Closing %q, expected Element, but found %T\n",
					s.filePath, upperName, s.currNode,
				)
			}

		default:
			fmt.Println("[Unexpected token]", tok.kind.String())
		}
	}
}

func (s *state) addTextNode(str string, doCompress, onlyTrim bool) {
	if len(str) == 0 {
		return
	}

	if doCompress {
		if len(strings.TrimSpace(str)) == 0 {
			return // Strings that trim to nothing are not added.
		}

		if onlyTrim { // Make sure a single leading and trailing space is retained
			str = reConsecutiveOuterSpace.ReplaceAllLiteralString(str, " ")

		} else { // Reduce multiple, adjacent spaces to a single space.
			str = reConsecutiveSpace.ReplaceAllLiteralString(str, " ")
		}
	}

	if cn := s.currNode.GetChildNodes(); len(cn) > 0 {
		last := cn[len(cn)-1]

		if tn, ok := last.(*types.TextNode); ok {
			tn.Data += str
			return
		}
	}

	s.currNode.AddChildren(&types.TextNode{
		DataNode: types.DataNode{
			Data: str,
		},
	})
}

func (s *state) addComment(data string) types.NodeI {
	return s.currNode.AddChildren(&types.CommentNode{
		DataNode: types.DataNode{
			Data: data,
		},
	})[0]
}

func (s *state) addDoctype(data string) types.NodeI {
	return s.currNode.AddChildren(&types.Doctype{
		DataNode: types.DataNode{
			Data: data,
		},
	})[0]
}

func (s *state) addElement(name string) *types.Element {
	var el = &types.Element{
		ParentNode: types.ParentNode{
			ChildNodes: []types.NodeI{},
		},
		Name:       strings.ToUpper(name),
		Attributes: []*types.AttrNode{},
	}
	_ = s.addStack(s.currNode.AddChildren(el)[0])
	return el
}

func (s *state) addMarker(m types.MarkerData) types.ParentI {
	return s.addStack(s.currNode.AddChildren(&types.Marker{
		ParentNode: types.ParentNode{
			ChildNodes: []types.NodeI{},
		},
		DataNode: types.DataNode{
			Data: m.Comment,
		},
		Name:            m.Name,
		IsPartial:       m.IsPartial,
		StaticHTMLPath:  m.StaticHTMLPath,
		MarkersToFill:   m.MarkersToFill,
	})[0])
}

func (s *state) tryElement(tagName string) {
	newNodeUpperName := strings.ToUpper(tagName)
	el := s.addElement(newNodeUpperName)

	hasFrostAttr := false

	// Fetch attributes until we find a trailing close bracket.
	// If no bracket is found, we don't have a valid tag.

	foundEqual, foundRBracket := false, false

	for !foundRBracket {
		tok := nextNoWhitespace(s.lex)

		if tok.kind == _tokEOF {
			s.popStack() // No '>', so pop the element off the stack
			fmt.Printf(
				"Tag is missing closing bracket %q\n  %s\n", tagName, s.filePath,
			)
			return
		}

		switch tok.kind {
		case _tokOpenTagEnd, _tokOpenTagSelfClosingEnd:
			foundRBracket = true
			continue
		}

		attr := s.addAttrNode()

		foundEqual, foundRBracket = s.tryAttrName(attr, tok, true)

		if foundEqual {
			switch tok = nextNoWhitespace(s.lex); tok.kind {
			case _tokEOF:
				goto POP

			case _tokOpenTagEnd, _tokOpenTagSelfClosingEnd:
				foundRBracket = true
				goto POP

			case _tokDoubleQuote:
				attr.Quote = '"'
				tok = nextToken(s.lex)

			case _tokSingleQuote:
				attr.Quote = '\''
				tok = nextToken(s.lex)
			}

			foundRBracket = s.tryAttrValue(attr, tok, false, false)

			hasFrostAttr = hasFrostAttr ||
				(attrPartsEquals(attr, true, "data-frost") &&
					attrPartsEquals(attr, false, "true"))

			attr.SetOrElideQuote()
		}

	POP:
		s.popStack() // Pop the attr off the stack
	}

	isScript := newNodeUpperName == "SCRIPT"
	isStyle := !isScript && newNodeUpperName == "STYLE"

	if hasFrostAttr &&
		len(s.nodeStack) == 1 &&
		((isScript && s.frostJs == nil) || (isStyle && s.frostCSS == nil)) {

		// `data_frost=true` can only be at the top level of the provided HTML
		_, hasFrostAttr = s.nodeStack[len(s.nodeStack)-1].(*types.GenericRoot)

	} else {
		hasFrostAttr = false
	}

	el.RemoveDuplicateAttrNames()

	// Attempts to locate some elements to their proper semantic placement.
	s.trySemanticPlacement(newNodeUpperName)

	if types.EmptyElements[newNodeUpperName] {
		s.popStack()

	} else if isScript || isStyle {
		// If it was a script or style tag, go ahead and skip to its closing tag
		var txt string
		if isScript {
			txt = textUntilString(s.lex, "</script")
		} else {
			txt = textUntilString(s.lex, "</style")
		}

		_ = textUntilByte(s.lex, '>', false)

		if hasFrostAttr {
			if isScript {
				s.frostJs = []byte(txt)
			} else {
				s.frostCSS = []byte(txt)
			}

			s.popStack()

			// The .currNode should now be the GenericRoot, as verified earlier, so
			// remove the script or style from its ChildNodes.
			root := s.currNode.(*types.GenericRoot)
			root.ChildNodes = root.ChildNodes[0 : len(root.ChildNodes)-1]

		} else {
			s.addTextNode(strings.TrimSpace(txt), false, false)
			s.popStack()
		}

	} else if newNodeUpperName == "PRE" {
		s.compress = false
	}
}

func (s *state) addAttrNode() *types.AttrNode {
	var elem = s.currNode.(*types.Element) // Let it panic if not an Element
	var attr = &types.AttrNode{
		ParentNode: types.ParentNode{
			ChildNodes: []types.NodeI{},
		},
	}

	elem.Attributes = append(elem.Attributes, attr)
	s.addStack(attr)
	return attr
}

func (s *state) addEqualSign() {
	// This is just to verify that `currNode` is an AttrNode. Let it panic if not.
	var attr = s.currNode.(*types.AttrNode)
	attr.AddChildren(types.EqualSign{})
}

// Parsers

func (s *state) tryMarker(
	tok *token, attr *types.AttrNode, doingName, attrEsc bool,
) (result bool) {
	m := &types.MarkerData{}

	switch tok.kind {
	case _markerStaticInjection: // {{+/a/path:: {json} }} Static HTML insertion
		// Verify valid JSON
		if len(strings.TrimSpace(tok.content)) != 0 &&
			json.Compact(&bytes.Buffer{}, []byte(tok.content)) != nil {
			return false
		}

		m.StaticHTMLPath = tok.data
		m.MarkersToFill = tok.content // Hold the JSON here for now.

	case _markerPartialApplication: // Partial application
		m.IsPartial = true
		fallthrough

	default:
		m.Name = tok.data
		m.DefValue = tok.content
	}

	marker := s.addMarker(*m)

	result = true

	if len(m.DefValue) != 0 {
		if attr == nil { // Parse the `DefValue`, and absorb its result
			root, _, _ := Parse(m.DefValue, s.compress, s.filePath)
			marker.AddChildren(root.ChildNodes...)

		} else {
			origLex := s.lex
			s.lex = newLexer(m.DefValue)

			if doingName {
				foundEql, foundRBrckt := s.tryAttrName(attr, nextToken(s.lex), attrEsc)

				// An equal sign or closing bracket nested in the marker of an attribute
				// name is invalid
				result = !foundEql && !foundRBrckt

			} else if s.tryAttrValue(attr, nextToken(s.lex), true, attrEsc) {
				panic("Internal error; closing bracket should become part of the" +
					"attr value when nested in a marker's content")
			}

			s.lex = origLex
		}
	}

	s.popStack() // Pop the marker off the stack. Any content has been added.

	return result
}

func (s *state) tryAttrName(
	attr *types.AttrNode, tok *token, doEscape bool,
) (foundEqual, foundRBracket bool) {

	firstIter := true

	for ; ; tok, firstIter = nextToken(s.lex), false {
		switch tok.kind {
		case _tokEOF:
			return false, false

		case _tokOpenTagEnd, _tokOpenTagSelfClosingEnd:
			return false, true

		case _tokWhitespace:
			if peekNoWhitespace(s.lex).kind != _tokEqualAssign {
				return false, false
			}
			_ = nextToken(s.lex) // Consume the '=' token
			fallthrough

		case _tokEqualAssign:
			if !firstIter {
				s.addEqualSign()
				return true, false
			}
			s.addTextNode("=", false, false) // Is the first character in the name

		case _marker,
			_markerPartialApplication,
			_markerStaticInjection:
			if s.tryMarker(tok, attr, true, doEscape) == false {
				fmt.Printf(_invalidMarker, tok.data, s.filePath)
			}

		default:
			if doEscape {
				s.addTextNode(frost.EscapeHTML(tok.String()), false, false)
			} else {
				s.addTextNode(tok.String(), false, false)
			}
		}
	}
}

func (s *state) tryAttrValue(
	attr *types.AttrNode, tok *token, isRecurse, doEscape bool,
) (foundRBracket bool) {

	endDelim := _tokWhitespace

	switch attr.Quote {
	case '"':
		endDelim = _tokDoubleQuote
	case '\'':
		endDelim = _tokSingleQuote
	}

	for ; ; tok = nextToken(s.lex) {
		switch tok.kind {

		case _tokEOF:
			return false

		case _marker,
			_markerPartialApplication,
			_markerStaticInjection:
			if s.tryMarker(tok, attr, false, doEscape) == false {
				fmt.Printf(_invalidMarker, tok.data, s.filePath)
			}
			continue

		case endDelim:
			// If recursive (marker content), delim is part of the value
			if !isRecurse {
				return false
			}

		case _tokOpenTagEnd, _tokOpenTagSelfClosingEnd:
			if isRecurse {
				// Recursive (marker content), so > or /> is part of the value
			} else if endDelim != _tokWhitespace {
				// Not recursive and endDelim is a quotation mark, so > or /> is part of
				// the value
			} else {
				// Not recursive and endDelim is whitespace, so > or /> is the closer
				return true
			}
		}

		if doEscape {
			s.addTextNode(frost.EscapeHTML(tok.String()), false, false)
		} else {
			s.addTextNode(tok.String(), false, false)
		}
	}
}

func attrPartsEquals(attr *types.AttrNode, checkName bool, match string) bool {
	var p []string
	var hasMrkr bool

	if checkName {
		p, _, hasMrkr = attr.GetNameTextParts()
	} else {
		p, _, hasMrkr = attr.GetValueTextParts()
	}

	return len(p) == 1 && !hasMrkr && p[0] == match
}

// `newNode` is the Node being added, so `self.currNode` would be its parent
func (s *state) trySemanticPlacement(newNodeName string) {
	// Cache the new node just added...
	var newNode = s.currNode

	// ...and replace it on return, since it may get overwritten.
	defer func() {
		s.currNode = newNode
	}()

	for i := len(s.nodeStack) - 1; i >= 0; i = len(s.nodeStack) - 1 {

		var currName string

		if elem, ok := s.nodeStack[i].(*types.Element); ok {
			currName = strings.ToUpper(elem.Name)
		}

		switch newNodeName {
		case "TD", "TH":
			switch currName {
			case "TD", "TH":
				s.popStack()
			default:
				return
			}

		case "TR":
			switch currName {
			case "TR", "TD", "TH":
				s.popStack()
			default:
				return
			}

		case "TBODY", "THEAD", "TFOOT":
			switch currName {
			case "TBODY", "THEAD", "TFOOT", "COLGROUP", "TD", "TH", "TR":
				s.popStack()
			default:
				return
			}

		case "LI":
			if currName == "LI" {
				s.popStack()
			} else {
				return
			}

		case "DD", "DT":
			switch currName {
			case "DD", "DT":
				s.popStack()
			default:
				return
			}

		case "RT", "RP":
			switch currName {
			case "RT", "RP":
				s.popStack()
			default:
				return
			}

		case "OPTION":
			if currName == "OPTION" {
				s.popStack()
			} else {
				return
			}

		case "OPTGROUP":
			switch currName {
			case "OPTGROUP", "OPTION":
				s.popStack()
			default:
				return
			}

		case "BODY":
			if currName == "HEAD" {
				s.popStack()
			} else {
				return
			}

		default:
			return
		}
	}
}
