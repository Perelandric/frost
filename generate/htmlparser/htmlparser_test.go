package htmlparser

import (
	"testing"
)

func TestParse(t *testing.T) {
	for _, data := range []struct {
		html                  string
		childCount, deepCount int
	}{
		{`<div>foo</div>`,
			1, 2},
		{`<div>{{foo}}</div>`,
			1, 2},
		{`<div><table><tr><td><b>foo</b>bar</td></tr><tr><td>foo<p>hi there</p><p>hi there</p></td></tr></table></div>`,
			1, 13},
		{`<h2 class="heading"></h2>
			<h2></h2>
			<h2></h2>`,
			3, 3},
		{`<div class="foo">
				<!-- <div></div> -->
			</div>`,
			1, 1},
		{`{{^wrap:: {{foo}}
			}}`,
			1, 1},
		{`test`,
			1, 1},
		{`foo{{foo
			`, 1, 1},
	} {
		childCount(t, data.html, data.childCount)
	}
}

func childCount(t *testing.T, html string, expect int) {
	root, _, _ := Parse(html, true, "/foo/bar.html")
	if root == nil {
		t.Error("GenericRoot is nil")
	}

	if found := len(root.ChildNodes); expect != found {
		t.Errorf("Expected %d children, found %d", expect, found)
	}
}
