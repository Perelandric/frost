package htmlparser

import (
	"fmt"
	"strings"
)

// Utility funcs
func byteIsWord(b byte) bool { // alpha, digit or underscore
	return isAlpha(b) || isNumber(b) || b == '_'
}

func isSpace(c byte) bool {
	return c == ' ' || c == '\r' || c == '\n' || c == '\t'
}
func isAlpha(b byte) bool {
	return 'a' <= b|0x20 && b|0x20 <= 'z'
}
func isNumber(b byte) bool {
	return '0' <= b && b <= '9'
}

func isTagNameChar(c byte) bool {
	return isAlpha(c) || isNumber(c)
}

type tokenKind int

func (tk tokenKind) String() string {
	switch tk {
	case _marker:
		return "[TOK MARKER OPEN]"
	case _markerStaticInjection:
		return "[TOK MARKER STATIC INJECTION OPEN]"
	case _markerPartialApplication:
		return "[TOK MARKER PARTIAL APPLICATION OPEN]"

	case _tokWhitespace:
		return "[TOK WHITESPACE]"

	case _tokText:
		return "[TOK TEXT]"

	case _tokOpenTagStart:
		return "[TOK OPEN TAG START]"
	case _tokOpenTagEnd:
		return "[TOK OPEN TAG END]"
	case _tokOpenTagSelfClosingEnd:
		return "[TOK OPEN TAG SELF CLOSING END]"
	case _tokCloseTag:
		return "[TOK CLOSE TAG]"

	case _tokHTMLComment:
		return "[TOK HTML COMMENT]"
	case _tokDoctype:
		return "[TOK DOCTYPE]"

	case _tokEqualAssign:
		return "[TOK EQUAL ASSIGN]"
	case _tokSingleQuote:
		return "[TOK SINGLE QUOTE]"
	case _tokDoubleQuote:
		return "[TOK DOUBLE QUOTE]"

	default:
		panic("Internal error: Unreachable")
	}
}

const (
	_marker                   = tokenKind(1 << iota) // {{name::...}}
	_markerStaticInjection                           // {{+name::...}}
	_markerPartialApplication                        // {{^name::...}}
	_markerWrapperContent                            // {{---}}

	_tokWhitespace // \t \n \r\n ' '

	_tokText // General text accumulation

	_tokOpenTagStart          // <name
	_tokOpenTagEnd            // >
	_tokOpenTagSelfClosingEnd // />
	_tokCloseTag              // </name>

	_tokHTMLComment // <!-- ... -->
	_tokDoctype     // <!doctype ... >

	_tokEqualAssign // =
	_tokSingleQuote // '
	_tokDoubleQuote // "

	_tokEOF // For token when input is fully consumed
)

type token struct {
	line, col int
	kind      tokenKind
	data      string // Data for the token (if any), or error message if invalid
	content   string // For markers with nested content
}

func (t *token) Error() string {
	return t.data
}

func (t *token) String() string {
	switch t.kind {
	case _marker:
		if len(t.content) != 0 {
			return fmt.Sprintf("{{%s::%s}}", t.data, t.content)
		}
		return fmt.Sprintf("{{%s}}", t.data)

	case _markerStaticInjection:
		if len(t.content) != 0 {
			return fmt.Sprintf("{{+%s::%s}}", t.data, t.content)
		}
		return fmt.Sprintf("{{+%s}}", t.data)

	case _markerPartialApplication:
		if len(t.content) != 0 {
			return fmt.Sprintf("{{^%s::%s}}", t.data, t.content)
		}
		return fmt.Sprintf("{{^%s}}", t.data)

	case _markerWrapperContent:
		return "{{---}}"

	case _tokWhitespace, _tokText:
		return t.data

	case _tokOpenTagStart:
		return fmt.Sprintf("<%s", t.data)

	case _tokOpenTagEnd:
		return ">"

	case _tokOpenTagSelfClosingEnd:
		return "/>"

	case _tokCloseTag:
		return fmt.Sprintf("</%s>", t.data)

	case _tokHTMLComment:
		return fmt.Sprintf("<!--%s-->", t.data)

	case _tokDoctype:
		return fmt.Sprintf("<!DOCTYPE %s>", t.data)

	case _tokEqualAssign:
		return "="

	case _tokSingleQuote:
		return "'"

	case _tokDoubleQuote:
		return "\""

	default:
		panic("Internal error: Unreachable")
	}
}

var ( // Reusable tokens
	tokAssign = &token{
		kind: _tokEqualAssign,
	}
	tokOpenEnd = &token{
		kind: _tokOpenTagEnd,
	}
	tokOpenSelfCloserEnd = &token{
		kind: _tokOpenTagSelfClosingEnd,
	}
	tokSingleQuote = &token{
		kind: _tokSingleQuote,
	}
	tokDoubleQuote = &token{
		kind: _tokDoubleQuote,
	}
	tokEmptyHTMLComment = &token{
		kind: _tokHTMLComment,
	}
	tokEOF = &token{
		kind: _tokEOF,
	}
)

type lexer struct {
	input  string
	i      int
	tokens []*token
	peeked *token
}

// textBeforeAny consumes all text until `b` and returns it.
// If `b` is not found, the remainder of the input is consumed.
func textUntilAny(lex *lexer, chars string) (res string) {
	if idx := strings.IndexAny(lex.input[lex.i:], chars); idx == -1 {
		res = lex.input[lex.i:]
		lex.i = len(lex.input)

	} else {
		res = lex.input[lex.i : lex.i+idx]
		lex.i += idx
	}

	return res
}

// textUntilByte consumes all text through `b` and returns it. If `includeByte`
// is `true`, `b` will be included in the result. It is consumed either way.
// If `b` is not found, the entire remainder of the string is consumed.
func textUntilByte(lex *lexer, b byte, includeByte bool) string {
	start := lex.i
	ln := len(lex.input)

	for ; lex.i < ln; lex.i++ {
		if lex.input[lex.i] == b {
			lex.i++
			break
		}
	}
	if includeByte {
		return lex.input[start:lex.i]
	}
	return lex.input[start : lex.i-1]
}

// textUntilString is similar to `textUntilByte`, but looks for a string, and
// performs a case-insensitive match.
func textUntilString(lex *lexer, s string) string {
	if len(s) == 0 {
		return ""
	}
	start := lex.i
	ln := len(lex.input) - len(s)

	for ; lex.i < ln; lex.i++ {
		if strings.EqualFold(lex.input[lex.i:lex.i+len(s)], s) {
			break
		}
	}

	lex.i += len(s)
	if lex.i >= len(lex.input) {
		return lex.input[start:]
	}
	return lex.input[start : lex.i-len(s)]
}

func moveIfString(lex *lexer, s string) bool {
	if len(lex.input)-lex.i < len(s) {
		return false
	}
	if strings.EqualFold(lex.input[lex.i:lex.i+len(s)], s) {
		lex.i += len(s)
		return true
	}
	return false
}

func peekAt(lex *lexer, i int) byte {
	if i >= len(lex.input) {
		return 0
	}
	return lex.input[i]
}
func curr(lex *lexer) byte {
	if lex.i >= len(lex.input) {
		return 0
	}
	return lex.input[lex.i]
}
func currThenMove(lex *lexer) byte {
	c := curr(lex)
	lex.i++
	return c
}
func peek(lex *lexer) byte {
	if lex.i+1 >= len(lex.input) {
		return 0
	}
	return lex.input[lex.i+1]
}

func move(lex *lexer) byte {
	p := peek(lex)
	if p != 0 {
		lex.i++
	}
	return p
}

func moveIfByte(lex *lexer, b byte) bool {
	if curr(lex) == b {
		lex.i++
		return true
	}
	return false
}

// skip over `n` characters, assuming they've been verified.
func skip(lex *lexer, n int) {
	lex.i += n
}

func newLexer(input string) *lexer {
	return &lexer{
		input: input,
	}
}

func peekNoWhitespace(lex *lexer) (t *token) {
	_ = peekToken(lex)
	for lex.peeked.kind == _tokWhitespace {
		lex.peeked = nextToken(lex)
	}
	return lex.peeked
}

func nextNoWhitespace(lex *lexer) (t *token) {
	t = nextToken(lex)

	for t.kind == _tokWhitespace {
		t = nextToken(lex)
	}

	return t
}

func peekToken(lex *lexer) (t *token) {
	if lex.peeked == nil {
		lex.peeked = nextToken(lex)
	}
	return lex.peeked
}

func nextToken(lex *lexer) (t *token) {
	if lex.peeked != nil {
		t, lex.peeked = lex.peeked, nil
		return t
	}

	if lex.i >= len(lex.input) {
		return tokEOF
	}

	c := currThenMove(lex)

	switch c {
	case '{':
		if t = parseMarker(lex); t != nil {
			return t
		}

	case ' ', '\r', '\n', '\t':
		return parseWhitespace(lex)

	case '<':
		if t = parseTag(lex); t != nil {
			return t
		}

	case '>':
		return tokOpenEnd

	case '/':
		if moveIfByte(lex, '>') {
			return tokOpenSelfCloserEnd
		}

	case '=':
		return tokAssign

	case '"':
		return tokDoubleQuote

	case '\'':
		return tokSingleQuote
	}

	// Didn't get a token, so gather some text
	t = &token{
		kind: _tokText,
		data: string(c),
	}

	for {
		switch c = curr(lex); c {
		case 0, '{', '}', ' ', '\r', '\n', '\t', '<', '>', '/', '=', '\'', '"':
			return t // return the accumulated text

		default:
			t.data += string(c)
			skip(lex, 1)
		}
	}
}

// parseMarker assumes the current char is the one after the first opening `{`
func parseMarker(lex *lexer) (t *token) {
	if curr(lex) != '{' {
		return nil
	}

	startPos := lex.i
	skip(lex, 1)

	switch c := curr(lex); c {
	case '-':
		if moveIfString(lex, "---}}") {
			return &token{
				kind: _markerWrapperContent,
			}
		}

	case '+':
		skip(lex, 1)
		t = &token{kind: _markerStaticInjection}

		idx := strings.Index(lex.input[lex.i:], "}}")
		if idx == -1 {
			lex.i = startPos
			return nil
		}

		t.data = lex.input[lex.i : lex.i+idx]
		skip(lex, idx+2)

		if idx = strings.Index(t.data, "::"); idx != -1 {
			t.content = t.data[idx+2:]
			t.data = t.data[:idx]
		}
		return t

	case '^':
		skip(lex, 1)
		t = parseMarkerName(lex, &token{kind: _markerPartialApplication})

	default: // Just the name
		t = parseMarkerName(lex, &token{kind: _marker})
	}

	if t == nil {
		lex.i = startPos
		return nil
	}

	for isSpace(curr(lex)) { // Move past any whitespace
		skip(lex, 1)
	}

	if moveIfString(lex, "}}") {
		return t
	}

	if moveIfString(lex, "::") {
		nestedStart := lex.i

		for {
			switch currThenMove(lex) {
			case 0:
				lex.i = startPos // Couldn't find the "}}"
				return nil

			case '}':
				if moveIfByte(lex, '}') {
					t.content = lex.input[nestedStart : lex.i-2] // Add nested content
					return t
				}

			case '{':
				_ = parseMarker(lex) // Don't need the token returned
			}
		}
	}

	return nil
}

// parseMarkerName - The current character of `lex` is expected to be the first
// character of the name, though not yet verified.
// If there are no valid characters, `t` will become an error token.
func parseMarkerName(lex *lexer, t *token) *token {
	if !isAlpha(curr(lex)) {
		return nil
	}

	first := lex.i
	for byteIsWord(move(lex)) {
	}

	t.data = lex.input[first:lex.i]
	return t
}

// parseTag assumes that the opening `<` has been consumed so the current
// character is the one directly after it, though not yet verified.
func parseTag(lex *lexer) (t *token) {
	t = &token{}

	c := curr(lex)

	if c == '!' {
		skip(lex, 1)

		if moveIfString(lex, "doctype") {
			return &token{
				kind: _tokDoctype,
				data: textUntilByte(lex, '>', false),
			}
		}

		if moveIfString(lex, "--") {
			if moveIfByte(lex, '>') ||
				moveIfString(lex, "->") ||
				moveIfString(lex, "-->") {
				return tokEmptyHTMLComment
			}

			return &token{
				kind: _tokHTMLComment,
				data: textUntilString(lex, "-->"),
			}
		}
	}

	if c == '/' && isTagNameChar(peek(lex)) {
		skip(lex, 1)
		t.data = string(currThenMove(lex))
		t.kind = _tokCloseTag

	} else if isTagNameChar(c) {
		t.data = string(currThenMove(lex))
		t.kind = _tokOpenTagStart

	} else {
		return nil
	}

	for {
		if c = curr(lex); !isTagNameChar(c) {
			break
		}
		t.data += string(c)
		skip(lex, 1)
	}

	if t.kind == _tokCloseTag {
		// skip all invalid text between name and '>'
		_ = textUntilByte(lex, '>', false)
	}

	return t
}

// parseWhitespace assumes the first whitespace character has been consumed.
func parseWhitespace(lex *lexer) (t *token) {
	start := lex.i - 1
	ln := len(lex.input)

	for ; lex.i < ln; lex.i++ {
		switch c := lex.input[lex.i]; c {
		case ' ', '\r', '\n', '\t':
			continue
		}
		break
	}

	return &token{
		kind: _tokWhitespace,
		data: lex.input[start:lex.i],
	}
}
