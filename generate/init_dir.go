package generate

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	util "Frost/utilities"
)

// Init creates the files for a new application, page or frag.
func Init(workDir, pth string) error {
	fullPath := strings.TrimRight(
		filepath.Join(workDir, pth), string(filepath.Separator),
	)
	fmt.Printf("Initializing: %s\n", fullPath)

	// Convert the path to an array of parts
	temp := fullPath
	pathParts := []string{}
	projPath := ""
	end := ""

	for len(temp) > 0 && temp != string(filepath.Separator) {
		frostPath := filepath.Join(temp, ".frost")
		if len(projPath) == 0 &&
			util.VerifyDir(frostPath, false) == nil {

			if util.VerifyDir(filepath.Join(temp, "vendor"), false) == nil {
				const msg = "Found a '.frost' directory at %q, but it does not appear to be a valid Frost project directory. Please delete or rename this directory and try again"
				return fmt.Errorf(msg, frostPath)
			}

			projPath = temp
			break
		}

		temp, end = filepath.Split(temp)
		temp = strings.TrimRight(temp, string(filepath.Separator))
		pathParts = append([]string{end}, pathParts...)
	}

	if len(projPath) == 0 {
		return createApp(fullPath)
	}

	const wrongDir = "Components must be within vendor/pages or vendor/frags. Found: %s"

	if len(pathParts) <= 1 || pathParts[0] != "vendor" {
		return fmt.Errorf(wrongDir, filepath.Join(pathParts...))
	}

	switch pathParts[1] {
	case "pages":
		return makeComponent(fullPath, contentForPage, true)

	case "frags":
		if len(pathParts) == 2 { // Fragments must be one more level deep
			return fmt.Errorf(wrongDir, "")
		}
		return makeComponent(fullPath, contentForFrag, false)

	default:
		return fmt.Errorf(wrongDir, "")
	}
}

func createApp(dir string) error {
	if err := util.VerifyDir(dir, true); err != nil {
		return err
	}

	fmt.Println("Initializing application structure...")

	// Create the basic file structure
	for _, err := range []error{
		createDir(dir, "res"),

		createDir(dir, "vendor", "app"),
		createDir(dir, "vendor", "pages"),
		createDir(dir, "vendor", "frags"),
		createDir(dir, "vendor", "prelude"),

		// Initialize a page and fragment.
		makeComponent(filepath.Join(dir, "vendor", "pages"), contentForPage, true),
		makeComponent(filepath.Join(dir, "vendor", "frags", "page_wrap"), contentForFrag, false),
	} {
		if err != nil {
			return err
		}
	}

	return nil
}

func createDir(path ...string) error {
	dir := filepath.Join(path...)
	fmt.Printf("creating: %s\n", dir)

	return os.MkdirAll(dir, util.DirPerms)
}
func createFile(fullPath, name, ext string) (*os.File, error) {
	fmt.Printf("creating: %s\n", filepath.Join(fullPath, name+"."+ext))

	return os.Create(filepath.Join(fullPath, name+"."+ext))
}

func makeComponent(fullPath string, contentArr [3]string, isPage bool) error {
	if util.FileExists(fullPath) {
		return fmt.Errorf("A file (non-directory) exists at: %s", fullPath)
	}

	exists, empty := util.DirExistsDirEmpty(fullPath)
	if exists && !empty {
		return fmt.Errorf("Directory isn't empty: %s", fullPath)
	}

	if err := createDir(fullPath); err != nil {
		return err
	}

	name := filepath.Base(fullPath)

	for i, ext := range []string{"go", "html"} {
		newFile, err := createFile(fullPath, name, ext)
		if err != nil {
			return err
		}

		if ext == "go" {
			_, err = fmt.Fprintf(newFile, contentArr[i], name)
		} else {
			_, err = newFile.WriteString(contentArr[i])
		}
		if err != nil {
			return err
		}
	}

	if isPage {
		// Write the default config file
		newFile, err := createFile(fullPath, "config", "json")
		if err != nil {
			return err
		}
		_, err = newFile.WriteString(contentArr[2])
		return err
	}
	return nil
}

// Describes the default content for the files in a "page" component.
var contentForPage = [3]string{
	`package %s

import (
	frost "bitbucket.org/Perelandric/frost/server"
)

func handleRequest(r *frost.Response) frost.Component {
	return page_wrap.Data{Content: Data{}}
}

// Data contains the data needed for this component
type Data struct {
}

// Make implements the frost.Component interface
func (d Data) Make(r *frost.Response) {
	// Add some content
	r.Add(marker.my_placeholder, "some dynamic content")
}

`,
	`
<div>
	<p>The page <span class="y">%q</span> has been initialized.</p>
	<p>Define <span class="y">{{my_placeholder}}</span> in the ".go" file.</p>
</div>


<script data-frost=true>
	console.log("Success!");
</script>


<style data-frost=true>
div p .y {
	font-weight: bold;
}
</style>

`,

	`
{
	"isRoot": false,
	"order": -1,
	"displayName": "",
	"description": ""
}
`,
}

// Describes the default content for the files in a "frag" component.
var contentForFrag = [3]string{
	`package %s

import (
	frost "bitbucket.org/Perelandric/frost/server"
)

// Data contains the data needed for this component
type Data struct {
	Content frost.Component
}

// Make implements the frost.Component interface
func (d Data) Make(r *frost.Response) {
	r.AddComponent(marker.my_placeholder, d.Content)
}

`,
	`
<div>
	{{my_placeholder}}
</div>


<script data-frost=true>
	console.log("Success!");
</script>


<style data-frost=true>
div {
	font-weight: bold;
}
</style>

`,

	``,
}
