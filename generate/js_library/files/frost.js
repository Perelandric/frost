
// Function() is needed so that closure compiler doesn't remove the @cc_on
// The wrapping IIFE is to work around github #2214
const __ie_version = LEGACY_LIB ? new Function(
  "return /*@cc_on {1.0:3, 3.0:4, 5.0:5, 5.1:5, 5.5:5.5, 5.6:6, " +
  "5.7: window.XMLHttpRequest ? 7 : 6, 5.8:8, 9:9, 10:10" +
  "}[@_jscript_version] @*/;")() : Infinity

const __fix_vers = __ie_version < 10 ? ("ie" + __ie_version) : "std"

/**
 * @typedef {Object<string,FrostComp>}
 */
var FrostCompObj

/**
 * @constructor
 */
function FrostCSSFix() {
  this.full = ""
  this.cls = ""
}

/**
 * @constructor
 * @param {!FrostCSSFixByCatByVersion} fixes
 */
function FrostCSSFixByCat(fixes) {
  this.unoptimized = fixes.unoptimized && fixes.unoptimized[__fix_vers]
  this.firstchild = fixes.firstchild && fixes.firstchild[__fix_vers]
  this.lastchild = fixes.lastchild && fixes.lastchild[__fix_vers]
  this.onlychild = fixes.onlychild && fixes.onlychild[__fix_vers]
  this.empty = fixes.empty && fixes.empty[__fix_vers]
}

/**
 * @constructor
 */
function FrostCSSFixByVersion() {
  this.std = /**@type{Array<FrostCSSFixByCat>}*/(null)
  this.ie9 = /**@type{Array<FrostCSSFixByCat>}*/(null)
  this.ie8 = /**@type{Array<FrostCSSFixByCat>}*/(null)
  this.ie7 = /**@type{Array<FrostCSSFixByCat>}*/(null)
  this.ie6 = /**@type{Array<FrostCSSFixByCat>}*/(null)
}

/**
 * This is never actually invoked. It is only used as a cast for the
 * generated object literals that have data in this format.
 *
 * @constructor
 */
function FrostCSSFixByCatByVersion() {
  this.unoptimized = /**@type{FrostCSSFixByVersion}*/(null)
  this.firstchild = /**@type{FrostCSSFixByVersion}*/(null)
  this.lastchild = /**@type{FrostCSSFixByVersion}*/(null)
  this.onlychild = /**@type{FrostCSSFixByVersion}*/(null)
  this.empty = /**@type{FrostCSSFixByVersion}*/(null)
}

/**
 * @struct
 */
class FrostComp {
  /**
   * @param {!number} id
   * @param {Array<!number>} imported
   * @param {function():*} readyCode
   * @param {function():!FrostCSSFixByCatByVersion} fixCode
   */
  constructor(id, imported, readyCode, fixCode) {
    this.frost_id = id
    this.imported = imported
    this.readyCode = readyCode
    this.fixCode = fixCode

    this.fixes = /**@type{FrostCSSFixByCat}*/(null)
    this.frags = /**@type{!Array<!FrostComp>}*/([])
    this.isReady = false
  }
}

/**
 * @struct
 */
class FrostPage extends FrostComp {
  /**
   * @param {!number} id
   * @param {Array<!number>} imported
   * @param {function():*} readyCode
   * @param {function():!FrostCSSFixByCatByVersion} fixCode
   * @param {FrostCompObj} childComps
   */
  constructor(id, imported, readyCode, fixCode, childComps) {
    super(id, imported, readyCode, fixCode)
    this.childComps = childComps
  }
}


/**
 * __setup_fixes is only invoked from generated JS code. Result is assigned to
 * the `const __fixes`.
 *
 * @param {!FrostCSSFixByCatByVersion} __prelude_fixes
 * @return {!FrostCSSFixByCat}
 */
function __setup_fixes(__prelude_fixes) {
  // Consolidate all the fixes for the current page and its accumulated
  // frags into a single set, by category
  return __page
    .frags
    .concat(__page) // Prevent mutating .frags
    .reduce(
    /**
     * @param {!FrostCSSFixByCat} by_cat
     * @param {!FrostComp} comp
     * @return {!FrostCSSFixByCat}
     */
    function (by_cat, comp) {
      Object.keys(comp.fixes || {}).forEach(cat => {
        by_cat[cat] = (by_cat[cat] || []).concat(comp.fixes[cat] || [])
      })
      return by_cat
    }, new FrostCSSFixByCat(__prelude_fixes))
}

/**
 * __setup_page is only invoked from generated JS code. Result is assigned to
 * the `const __page`.
 *
 * @return {!FrostPage}
 */
function __setup_page() {
  let page = __pages || new FrostPage(Infinity, null, function () {}, null)

  let childComps = page.childComps || /**@type{FrostCompObj}*/({})

  location.pathname.split("/").every(function(part) {
    if (!part) {
      return true // Skip adjacent directory separators
    }
    if (!childComps.hasOwnProperty(part)) {
      return false // Stop when we can go no deeper via the path
    }

    page = childComps[part] || new FrostPage(Infinity, null, function() {}, null)
    childComps = page.childComps
    return !!childComps // Stop if no childComps
  })

  if (page.fixCode) {
    page.fixes = new FrostCSSFixByCat(page.fixCode)
    page.fixCode = null
  }

  const temp = []

  // Gather imports of the object and their imports, recursively.
  page.frags = (page.imported || temp).reduce(function fn(arr, imp) {
    const frag = __frags[imp]

    if (frag && frag.fixCode) {
      frag.fixes = new FrostCSSFixByCat(frag.fixCode)
      frag.fixCode = null
    }

    return frag ? arr.concat(frag, (frag.imported || temp).reduce(fn, [])) : arr
  }, [])

  return page
}

const __cls_re = /(?:^|\s+)(?:cssfix-\d+-\d+-\d+)(?=\s+|$)/g

/**
 * @struct
 */
const Frost = {
  /**
   * @param {Event} event
   */
  loader: function (event) {
    if (!LEGACY_LIB || document.removeEventListener) {
      document.removeEventListener("DOMContentLoaded", Frost.loader, false);
    }

    Frost.updateFromRoot(document)

    for (const comp of __page.frags.concat(__page)) {
      if (!comp || comp.isReady) {
        continue
      }
      comp.isReady = true

      if (!comp.readyCode || !comp.frost_id || comp.frost_id === Infinity) {
        continue
      }

      const kind = (comp instanceof FrostPage) ? "page" : "frag"

      const sel = "[data-frost-" + kind + "='" + comp.frost_id + "']"

      for (const el of DOM.qa(sel)) {
        if (el["__frost_ready"]) {
          continue
        }
        el["__frost_ready"] = true

        comp.readyCode.call(el)
      }
    }
  },

  updateElement: function(el) {
    return __fixes && __updateElement(el, __fixes)
  },

  updateFromRoot: function(root) {
    if (!root) {
      root = document
    }
    if (!__fixes || !root.getElementsByTagName) {
      return
    }

    const fix_data = __fixes

    __updateElement(root, fix_data)

    for (const elem of root.getElementsByTagName("*")) {
    	__updateElement(elem, fix_data)
    }
  }
}


/**
 * @param {!Element} el
 * @param {!Array<!FrostCSSFix>} fixes
 */
function __do_sels(el, fixes) {
  for (const fix of fixes) {
    if (DOM.matches(el, fix.full)) {
      el.className += " " + fix.cls
    }
  }
}

/**
 * @param {Node} el
 * @param {!FrostCSSFixByCat} fix_data
 */
function __updateElement(el, fix_data) {
  if (el && el.nodeType === 1) {
    if (el.className) { // Strip away any previously added `cssfix-` classes
      el.className = el.className.replace(__cls_re, "").trim()
    }

    var prev = el
    ,   next = el

    // "optimized" selectors can be quickly ruled out for an element
    if (fix_data.firstchild || fix_data.onlychild) {
      prev = previousElementSibling(el)
    }
    if (fix_data.lastchild || fix_data.onlychild) {
      next = nextElementSibling(el)
    }

    if (!prev && fix_data.firstchild) {
      __do_sels(el, fix_data.firstchild)
    }
    if (!next && fix_data.lastchild) {
      __do_sels(el, fix_data.lastchild)
    }
    if (!prev && !next && fix_data.onlychild) {
      __do_sels(el, fix_data.onlychild)
    }

    if (fix_data.empty && !el.childNodes.length) {
      __do_sels(el, fix_data.empty)
    }

    if (fix_data.unoptimized) {
      __do_sels(el, fix_data.unoptimized)
    }
  }
}





/*


I think a better approach may be to allow the developer to define a `markers:`
property in their JS files that points to an object. The object would be given
properties representing each defined marker. The value of each marker property
would either be a literal value or a function that would be invoked to fill in
the marker.

There would need to be a way to load other fragments that are available to that
component though. I think that would be easy enough.

So this way, the developer gets to define everything, and the subsystem to manage
this would likely be very small.



// https://jsfiddle.net/rty2n3av/18/

function Marker(key) {
	// This needs to remain `defineProperties` instead of `defineProperty` so that
  // Closure Compiler will consistenly munge the `key$`
	Object.defineProperties(this, {
  	key$: {
      value: key
    }
  })
}

Marker.prototype = Object.create(null, {
	constructor: {value: Marker},
  key$: {value: ""},
  toString: {
  	value: function() {
    	return this.key$
    }
  },
  add: {
    value: function(r, data) {
      r[this.key$].push((data instanceof Result) ? data : (data + ""))
    }
  },
  skip: {
    value: function(r) {
      r[this.key$].push(__skip)
    }
  }
})

function Result(wrapCloseStart, dataArray) {
	Object.defineProperties(this, {
  	wrapCloseStart$: {value: wrapCloseStart},
    isWrapper$: {value: wrapCloseStart > -1},
    pre$: {value: "", writable: true},
    post$: {value: "", writable: true}
  })

  dataArray.forEach(function(obj) {
    Object.defineProperty(this, obj.key$, {
      value: []
    })
  }, this)
}

Result.prototype = Object.create(null, {
	constructor: {value: Result},
  wrapCloseStart$: {value: -1},
  isWrapper$: {value: false},
  pre$: {value: "", writable: true},
  post$: {value: "", writable: true}
})

var __skip = {}

function __finalize(r, dataArray, i, len) {
  var markerInsertions,
  	j = 0,
    markerHasContent = false

  function append(s) {
    if (!r.isWrapper$ || i < r.wrapCloseStart$) {
      r.pre$ += s
    } else {
      r.post$ += s
    }
  }

  // Walk through static HTML fragments and their corresponding markers.
  for (; i < len; i+=1) {
    if (!dataArray[i].key$) {
      append(dataArray[i].html$)
      continue
    }

    // Array of dynamic insertions for this key.
    markerInsertions = r[dataArray[i].key$]

    for (j = 0; j < markerInsertions.length; j+=1) {
      if (markerInsertions[j] === __skip) {
        i = dataArray[i].end$
        break
      }

      // If a wrapper component was added, wrap any content of the current marker.
      if (markerInsertions[j] instanceof Result) {
        markerHasContent = dataArray[i].start$ !== dataArray[i].end$

        append(markerInsertions[j].pre$) // Insert the opening of the wrapper component.

        // Only recurse when there was something to wrap.
        if (markerHasContent) {
          append(dataArray[i].html$)
            // Recurse only the content of this marker.
          __finalize(r, dataArray, i + 1, dataArray[i].end$ + 1)
        }

        append(markerInsertions[j].post$) // Insert the closing of the wrapper component.

        if (markerHasContent) {
          // Any marker content was added above, so move the outer loop ahead.
          i = dataArray[i].end$
          break // Anything else in `markerInsertions` is ignored since we've moved on.
        }

      } else {
        append(markerInsertions[j])
      }
    }

    append(dataArray[i].html$)
  }
  return r
}

function setup(make_fn, wrapCloseStart, dataArray) {
  // Take the markers, make a new object inheriting from `__funcs_proto`,
  // add each marker to that object, and pass it to the closure that
  // returns the original `make()` function.
  var markersObj = dataArray.reduce(function(res, obj) {
    return Object.defineProperty(res, obj.key$, {
      value: new Marker(obj.key$)
    })
  }, {})

  return function create(data) {
    var newr = new Result(wrapCloseStart, dataArray)

    make_fn.call(markersObj, newr, data)

    return __finalize(newr, dataArray, 0, dataArray.length)
  }
}
*/



/*
// RESULT
// <div>my data</div>more my data static string<span>some tester string</span>more text

var pages = {
  "/foo/bar": {
    // handlers
  }
}

var frags = {
  "some": {
    "other": {
    	make: function make(r, data) {
        this.tester.add(r, "some tester string")
      }
    },
    "nested": {
      "frag": {
        // handlers
        click: function(event) {

        },
        make: function make(r, data) {
          this.foobar.add(r, data.whatever)
          this.raboof.add(r, data.more_whatever)
          this.raboof.add(r, " static string")
          this.raboof.add(r, frags.some.other.create("whatever"))
        }
      }
    }
  }
}

frags.some.other.create = setup(
  frags.some.other.make,
  -1,
  [{
    key$: "",
    start$: 0,
    end$: 0,
    html$: "<span>"
  }, {
    key$: "tester",
    start$: 1,
    end$: 1,
    html$: "</span>"
  }]
)
delete frags.some.other.make

frags.some.nested.frag.create = setup(
  frags.some.nested.frag.make,
  -1,
  [{
    key$: "",
    start$: 0,
    end$: 0,
    html$: "<div>"
  }, {
    key$: "foobar",
    start$: 1,
    end$: 1,
    html$: "</div>"
  }, {
    key$: "raboof",
    start$: 2,
    end$: 2,
    html$: "more text"
  }]
)
delete frags.some.nested.frag.make

var o = frags.some.nested.frag.create({
  whatever: "my data",
  more_whatever: "more my data"
})
console.log(o.pre$ + o.post$)
*/
