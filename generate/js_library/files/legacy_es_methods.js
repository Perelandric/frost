
/*
    json2.js
    2015-05-03

    Public Domain.

    This is a reference implementation. You are free to copy, modify, or
    redistribute.
*/

if (typeof JSON !== 'object') {
  var JSON = {}

  ;(function () {
    "use strict"

    var rx_one = /^[\],:{}\s]*$/
    ,   rx_two = /\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g
    ,   rx_three = /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g
    ,   rx_four = /(?:^|:|,)(?:\s*\[)+/g
    ,   rx_escapable = /[\\\"\u0000-\u001f\u007f-\u009f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g
    ,   rx_dangerous = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g

    function f(n) {
      return n < 10 ? '0' + n : n
    }

    function this_value() {
      return this.valueOf() /*jshint ignore:line*/
    }

    if (typeof Date.prototype.toJSON !== 'function') {

      Date.prototype.toJSON = function () {

        return isFinite(this.valueOf()) ? this.getUTCFullYear() + '-' +
                    f(this.getUTCMonth() + 1) + '-' +
                    f(this.getUTCDate()) + 'T' +
                    f(this.getUTCHours()) + ':' +
                    f(this.getUTCMinutes()) + ':' +
                    f(this.getUTCSeconds()) + 'Z' :
                  null
      }

      Boolean.prototype.toJSON = this_value
      Number.prototype.toJSON = this_value
      String.prototype.toJSON = this_value
    }

    var gap
    ,   indent
    ,   meta
    ,   rep


    function quote(string) {
      rx_escapable.lastIndex = 0
      return rx_escapable.test(string) ?
          '"' + string.replace(rx_escapable, function (a) {
              var c = meta[a]
              return typeof c === 'string' ?
                  c :
                  '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4)
          }) + '"' :
          '"' + string + '"'
    }


    function str(key, holder) {

      var i          // The loop counter.
      ,   k          // The member key.
      ,   v          // The member value.
      ,   length
      ,   mind = gap
      ,   partial
      ,   value = holder[key]

      if (value && typeof value === 'object' &&
                   typeof value.toJSON === 'function') {
        value = value.toJSON(key)
      }

      if (typeof rep === 'function') {
        value = rep.call(holder, key, value)
      }

      switch (typeof value) {
      case 'string':
        return quote(value)

      case 'number':
        return isFinite(value) ? String(value) : 'null'

      case 'boolean':
      case 'null':
        return String(value)
      case 'object':
        if (!value) {
            return 'null'
        }

        gap += indent
        partial = []

        if (Object.prototype.toString.apply(value) === '[object Array]') {
          length = value.length
          for (i = 0; i < length; i += 1) {
            partial[i] = str(i, value) || 'null'
          }

          v = partial.length === 0 ? '[]' :
              gap ? '[\n' + gap + partial.join(',\n' + gap) + '\n' + mind + ']'
                  : '[' + partial.join(',') + ']'
          gap = mind
          return v
        }

        if (rep && typeof rep === 'object') {
          length = rep.length
          for (i = 0; i < length; i += 1) {
            if (typeof rep[i] === 'string') {
              k = rep[i]
              v = str(k, value)
              if (v) {
                partial.push(quote(k) + (
                  gap ? ': ' : ':'
                ) + v)
              }
            }
          }
        } else {
          for (k in value) {
            if (Object.prototype.hasOwnProperty.call(value, k)) {
              v = str(k, value)
              if (v) {
                partial.push(quote(k) + (gap ? ': ' : ':') + v)
              }
            }
          }
        }
        v = partial.length === 0 ? '{}' :
          gap ? '{\n' + gap + partial.join(',\n' + gap) + '\n' + mind + '}' :
                '{' + partial.join(',') + '}'
        gap = mind
        return v
      }
    }

    if (typeof JSON.stringify !== 'function') {
      meta = {    // table of character substitutions
          '\b': '\\b',
          '\t': '\\t',
          '\n': '\\n',
          '\f': '\\f',
          '\r': '\\r',
          '"': '\\"',
          '\\': '\\\\'
      }

      JSON.stringify = function (value, replacer, space) {
        var i
        gap = ''
        indent = ''

        if (typeof space === 'number') {
          for (i = 0; i < space; i += 1) {
              indent += ' '
          }
        } else if (typeof space === 'string') {
          indent = space
        }

        rep = replacer
        if (replacer && typeof replacer !== 'function' &&
                (typeof replacer !== 'object' ||
                typeof replacer.length !== 'number')) {
            throw new Error('JSON.stringify')
        }
        return str('', {'': value})
      }
    }

    if (typeof JSON.parse !== 'function') {
      JSON.parse = function (text, reviver) {
        var j

        function walk(holder, key) {
          var k, v, value = holder[key]
          if (value && typeof value === 'object') {
            for (k in value) {
              if (Object.prototype.hasOwnProperty.call(value, k)) {
                v = walk(value, k)
                if (v !== undefined) {
                  value[k] = v
                } else {
                  delete value[k]
                }
              }
            }
          }
          return reviver.call(holder, key, value)
        }

        text = String(text)
        rx_dangerous.lastIndex = 0

        if (rx_dangerous.test(text)) {
          text = text.replace(rx_dangerous, function (a) {
            return '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4)
          })
        }

        if (rx_one.test(text.replace(rx_two, '@')
                            .replace(rx_three, ']')
                            .replace(rx_four, ''))) {
          j = eval('(' + text + ')') /*jshint ignore:line*/

          return typeof reviver === 'function' ? walk({'': j}, '') : j
        }
        throw new SyntaxError('JSON.parse')
      }
    }
  }())
}


const __objectToString = Object.prototype.toString
const __hasOwnProperty = Object.prototype.hasOwnProperty

window.setTimeout = window.setTimeout
window.setInterval = window.setInterval
void function (__orig_timeout, __orig_interval) {
  // Check to see if the fixes were actually needed, and remove them if not
  __orig_timeout(function(x) {
    if (arguments.length === 2 && x === "_") { // Fixes were not needed
      delete window.setTimeout
      delete window.setInterval
    }
  }, 15, "_")

  function doFix(__orig, args) {
    var fn = args[0]
    var ms = +args[1] || 0

    if (args.length <= 2 || typeof fn !== "function") {
      return __orig(fn, ms)
    }

    var extraArgs = Array.from(args).slice(2)
    return __orig(function () {
      return fn.apply(this, extraArgs)
    }, ms)
  }

  window.setTimeout = function () {
    return doFix(__orig_timeout, arguments)
  }

  window.setInterval = function () {
    return doFix(__orig_interval, arguments)
  }
}(setTimeout, setInterval)

/*********************

  ECMAScript 3

*********************/

// Patches for specific ES3 issues


// Fix Array#splice to give the correct result when 1 arg is passed
if ([1,2,3].splice(1).length !== 2) {
  ;(function(__orig_splice__) {
    Array.prototype.splice = function() {
      // If only one arg, add second arg large enough to remove remaining members
      if (arguments.length < 2) {
        arguments[1] = Infinity
        arguments.length = 2
      }
      return __orig_splice__.apply(this, arguments)
    }
  })(Array.prototype.splice)
}


// Fix Array#slice to handle more than just Array and Arguments.
// Do NOT use `Function#apply` here, since the `Function#apply` fix calls it.
try {
  Array.prototype.slice.call(document.forms)

  if ([1,2,3].slice(1, undefined).length !== 2) {
    throw "" // Not handling the second argument correctly.
  }

} catch(e) {
  ;(function(__orig_slice__, undefined) {
    Array.prototype.slice = function(start, end) {
      // Make sure `start` and `end` are handled correctly
      start = /**@type {number}*/ (start) >>> 0

      const len = this.length >>> 0

      if (start < 0) {
        start = Math.max(len + start, 0)
      } else {
        start = Math.min(len, start)
      }

      if (end === undefined) {
        end = len
      } else {
        end = /**@type {number}*/ (end) >>> 0
      }

      if (end < 0) {
        end = Math.max(len + end, 0)
      } else {
        end = Math.min(len, end)
      }

      try { // Again, be sure to use `.call()` instead of `.apply()` here since
            // the `.apply()` fix calls `Array#slice`
        return __orig_slice__.call(this, start, end)

      } catch(e) {
        const result = []
        let n = 0

        for (; start < end; start++, n++) {
          if (start in this) {
            result[n] = this[start]
          }
        }
        return result
      }
    }
  })(Array.prototype.slice, void 0)
}


// `parseInt()` should not interpret leading zero as octal
if (parseInt("010") === 8) {
  window.parseInt = function(__old_parseInt) {
    return function(n, rad) {
      const args = arguments

      if (args.length === 1 &&
        __objectToString.call(n) === "[object String]") {

        let nn = n.trim()
        ,   i = 0

  			for (; nn.charAt(i) === "0"; i++) {
  			}

        if (0 < i && i < nn.length) { // At least one, but not all, zeros.
          nn = nn.slice(i)

    			if ("0" <= nn.charAt(0) && nn.charAt(0) <= "7") {
    				args[0] = nn
    			}
        }
      }
      return __old_parseInt.apply(window, args)
    }
  }(parseInt)
}


// FF < 8 would treat `undefined` or no argument as the string "undefined"
if ("".search(/**@type{string}*/(undefined))!==0) {
  const __ff_original_string_search = String.prototype.search

  String.prototype.search = function(re) {
    if (re === undefined) {
      return 0
    }
    return __ff_original_string_search.apply(this, arguments)
  }
}


// From https://github.com/termi/ES5-DOM-SHIM/
if (!Date.prototype.toISOString ||
  !new Date(-62198755200000).toISOString().includes("-000001") ||
  new Date(-1).toISOString() !== "1969-12-31T23:59:59.999Z") {

  Date.prototype.toISOString = function() {
    let result
    ,   year
    ,   month

    if (!isFinite(this)) {
      throw new RangeError(
        "Date.prototype.toISOString called on non-finite value.")
    }
    year = this.getUTCFullYear()
    month = this.getUTCMonth()

    // see https://github.com/kriskowal/es5-shim/issues/111
    year += ~~(month / 12)
    month = (month % 12 + 12) % 12

    // the date time string format is specified in 15.9.1.15.
    result = [month + 1, this.getUTCDate(), this.getUTCHours(),
                this.getUTCMinutes(), this.getUTCSeconds()
            ].map(function(val) {
                return val < 10 ? "0" + val : "" + val
            })

    year = (year < 0 ? '-' : (year > 9999 ? '+' : '')) +
          ('00000' + Math.abs(year)).slice(0 <= year && year <= 9999 ? -4 : -6)

    // pad milliseconds to have three digits.
    return year + "-" + result.slice(0, 2).join("-") + "T" +
            result.slice(2).join(":") + "." +
            ("000" + this.getUTCMilliseconds()).slice(-3) + "Z"
  }
}



/*********************

  ECMAScript 5

*********************/


/*
  These patches are compatible with IE6+
*/


/******************

    Function fixes

******************/

// For `.apply()` implementations that only accept Array and Arguments as the
// arg collection
try {
  Array.apply(Array, {length:0})

} catch(e) {
  ;(function(__apply__) {
    // ES3 only allowed Arrays and Arguments as the 2nd arg to .apply
    Function.prototype.apply = function(thisArg, args) {
      switch (__objectToString.call(args).slice(8, -1)) {
        case "Array": case "Arguments":
          return __apply__.call(this, thisArg, args)

        default: // IE8 and lower gets a patch for this----------vvv
          return __apply__.call(this, thisArg, Array.prototype.slice.call(args))
      }
    }
  })(Function.apply)
}


/**************

    Date

**************/

if (!Date.now) {
  Date.now = function() {
    return (new Date).getTime()
  }
}

/***************

    Object

***************/

if (!Object.keys) {
  let hasDontEnumBug = true
  ,   key = ""

  const dontEnums =
      [   "toString"
      ,   "toLocaleString"
      ,   "valueOf"
      ,   "hasOwnProperty"
      ,   "isPrototypeOf"
      ,   "propertyIsEnumerable"
      ,   "constructor"
      ]
  ,   len = dontEnums.length

  for (key in {toString:0}) {
    hasDontEnumBug = false
    break
  }

  Object.keys = function(o) {
    const keys = []
    let key
    ,   i

    for (key in o) {
      if (__hasOwnProperty.call(o, key)) {
        keys.push(key)
      }
    }

    if (hasDontEnumBug) {
      for (i = 0; i < len; i++) {
        if (__hasOwnProperty.call(o, dontEnums[i])) {
          keys.push(dontEnums[i])
        }
      }
    }
    return keys
  }
}


/************************

    Array fixes

************************/

if (!Array.isArray) {
  Array.isArray = function(arr) {
    if (arr && typeof arr === "object" && ("length" in arr)) {
      if (__objectToString.call(arr) === "[object Array]") {
        return true
      }

      const str = arr.constructor+""
      return str.startsWith("function Array()") && str.includes("[native code]")
    }
    return false
  }
}

if (!Array.prototype.forEach) {
  Array.prototype.forEach = function(fn, ctx) {
    for (let i = 0, len = this.length >>> 0; i < len; i++) {
      if (i in this) {
        fn.call(ctx, this[i], i, this)
      }
    }
  }
}

if (!Array.prototype.every) {
  Array.prototype.every = function(fn, ctx) {
    for (let i = 0, len = this.length >>> 0; i < len; i++) {
      if ((i in this) && !fn.call(ctx, this[i], i, this)) {
        return false
      }
    }
    return true
  }
}

if (!Array.prototype.filter) {
 Array.prototype.filter = function(fn, ctx) {
    const result = []
    for (let i = 0, len = this.length >>> 0; i < len; i++) {
      if ((i in this) && fn.call(ctx, this[i], i, this)) {
        result.push(this[i])
      }
    }
    return result
  }
}

if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function(val, start) {
    for (let i = Math.max(+start || 0, 0), len = this.length >>> 0; i < len; i++) {
      if ((i in this) && val === this[i]) {
        return i
      }
    }
    return -1
  }
}

if (!Array.prototype.lastIndexOf) {
  Array.prototype.lastIndexOf = function(val, start) {
    for (let i = Math.min(+start + 1 || this.length >>> 0, this.length >>> 0); i--;) {
      if ((i in this) && val === this[i]) {
        return i
      }
    }
    return -1
  }
}

if (Array.prototype.map) {
  Array.prototype.map = function(fn, ctx) {
    const result = []
    for (let i = 0, len = this.length >>> 0; i < len; i++) {
      if (i in this) {
        result.push(fn.call(ctx, this[i], i, this))
      }
    }
    return result
  }
}

if (!Array.prototype.some) {
  Array.prototype.some = function(fn, ctx) {
    for (let i = 0, len = this.length >>> 0; i < len; i++) {
      if ((i in this) && fn.call(ctx, this[i], i, this)) {
        return true
      }
    }
    return false
  }
}

if (!Array.prototype.reduce) {
  Array.prototype.reduce = function(fn, acc) {
    const len = this.length >>> 0
    let i = 0

    if (arguments.length < 2) {
      while (i < len && !(i in this)) {
        i++
      }
      acc = this[i++]
    }
    for (; i < len; i++) {
      if (i in this) {
        acc = fn(acc, this[i], i, this)
      }
    }
    return acc
  }
}

if (!Array.prototype.reduceRight) {
  Array.prototype.reduceRight = function(fn, acc) {
    let i = (this.length >>> 0) - 1

    if (arguments.length < 2) {
      while (i > -1 && !(i in this)) {
        i--
      }
      acc = this[i--]
    }

    for (; i > -1; i--) {
      if (i in this) {
        acc = fn(acc, this[i], i, this)
      }
    }
    return acc
  }
}

/******************

    Function

******************/
/* Based on MDN patch for Function.prototype.bind */
if (!Function.prototype.bind) {
  Function.prototype.bind = function (boundThis, ...args) {
    const origFunc = this

    function boundFunc() {
      return origFunc.call(
        this instanceof boundFunc ? this : boundThis, ...args, ...arguments
      )
    }

    if (this.prototype) {
      boundFunc.prototype = Object.create(this.prototype)
    }
    return boundFunc
  }
}


const trim_white = "\x09\x0A\x0B\x0C\x0D\x20\xA0\x85\u1680\u180E\u2000\u2001" +
  "\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u202F\u205F\u3000" +
  "\u2028\u2029\uFEFF\u200B\uFEFF"
const trim_white_group = "[" + trim_white + "]"
const trim_left_re = new RegExp("^" + trim_white_group + trim_white_group + "*")
const trim_right_re = new RegExp(trim_white_group + trim_white_group + "*$")

if (!String.prototype.trim || trim_white.trim()) {
  String.prototype.trim = function() {
    return (this + "").replace(trim_left_re, "")
                      .replace(trim_right_re, "")
  }
}




/*********************

  ECMAScript 6

*********************/

/*
  These patches are compatible with IE6+ if ES5 methods are patched
*/

// const __arrayIndexOf = Array.prototype.indexOf
// const __arraySlice = Array.prototype.slice
// const __stringIndexOf = String.prototype.indexOf

// if (!Object.assign) {
//   /**
//    * @param {*} target
//    * @param {...Object} args
//    * @return {!Object}
//    */
//   Object.assign = function (target, ...args) {
//     if (target == null) {
//       throw new TypeError()
//     }

//     if (!args.length) {
//       return target
//     }

//     const to = /**@type {!Object}*/(Object(target))

//     for (let i = 0; i < args.length; i++) {
//       const source = args[i]

//       for (const key in source) {
//         if (__hasOwnProperty.call(source, key)) {
//           to[key] = source[key]
//         }
//       }
//     }
//     return to
//   }
// }

// if (!Object.is) {
//   /**
//    * @param {*} a First argument for comparison
//    * @param {*} b Second argument for comparison
//    * @return {boolean}
//    */
//   Object.is = function(a, b) { // Equal if...
//     if (a === 0 && b === 0) {
//       return 1 / a === 1 / b // ...both zeros are positive or negative
//     }
//     // ...both values are strictly equal or both are NaN
//     return a === b || a !== a && b !== b
//   }
// }

// if (!Number.isNaN) {
//   /**
//    * @param {*} v The value to test
//    * @return {boolean}
//    */
//   Number.isNaN = function(v) {
//     return v !== v
//   }
// }

// if (!Array.of) {
//   /**
//    * @return {Array.<*>}
//    */
//   Array.of = function() {
//     let a

//     if (typeof this === "function") {
//       try {
//         a = new this(len)
//       } catch(e) {
//         a = new Array(len)
//       }
//     } else {
//       a = new Array(len)
//     }

//     const len = arguments.length
//     for (let k = 0; k < len; k+=1) {
//       a[k] = arguments[k]
//     }
//     a.length = len
//     return a
//   }
// }

// if (!Array.from) {
//   /**
//    * @param {!Object} o Object to be converted to an Array
//    * @param {Function=} cb Optional "map" callback
//    * @param {*=} ctx Optional thisArg for the callback
//    * @return {Array.<*>}
//    */
//   Array.from = function(o, cb, ctx) {
//     const len = o.length >>> 0
//     ,     hasCb = typeof cb === "function"
//     ,     target = typeof this === "function" ? new this(len) : new Array(len)

//     for (let i = 0; i < len; i++) {
//       target[i] = hasCb ? cb.call(ctx, o[i], i) : o[i]
//     }
//     target.length = len
//     return target
//   }
// }

// if (!Array.prototype.find) {
//   /**
//    * @param {function(?*=, number=, Array.<*>=): *} callback
//    * @param {*=} ctx
//    * @return {*}
//    */
//   Array.prototype.find = function(callback, ctx) {
//     const o = Object(this)
//     ,     len = o.length >>> 0
//     ,     ctx = arguments[1]

//     for (let i = 0; i < len; i+=1) {
//       if (callback.call(ctx, o[i], i, o)) {
//         return this[i]
//       }
//     }
//     return void 0
//   }
// }

// if (!Array.prototype.findIndex) {
//   /**
//    * @param {function(?*=, number=, Array.<*>=): *} callback
//    * @param {*=} ctx
//    * @return {number}
//    */
//   Array.prototype.findIndex = function(callback, ctx) {
//     const o = Object(this)
//     ,     len = o.length >>> 0
//     ,     ctx = arguments[1]

//     for (let i = 0; i < len; i+=1) {
//       if (callback.call(ctx, o[i], i, o)) {
//         return i
//       }
//     }
//     return -1
//   }
// }

// if (!Array.prototype.fill) {
//   /**
//    * @param {*} val Value to be placed into Array
//    * @param {number=} start
//    * @param {number=} end
//    * @return {Array.<*>}
//    */
//   Array.prototype.fill = function(val/*, start, end*/) {
//     const o = Object(this)
//     ,     len = o.length>>>0

//     let start = parseInt(arguments[1], 10)
//     start = isNaN(start) ? 0 :
//             start < 0    ? Math.max(len + start, 0) :
//                            Math.min(len, start)

//     let end = parseInt(arguments[2], 10)
//     end = isNaN(end) ? len :
//           end < 0    ? Math.max(len + end, 0) :
//                        Math.min(len, end)

//     for (; start < end; start+=1) {
//       o[start] = val
//     }
//     return this
//   }
// }

// // MDN polyfill
// if (!Array.prototype.copyWithin) {
//   Array.prototype.copyWithin = function(target, start/*, end*/) {
//     const O = this

//     // Steps 3-5.
//     const len = O.length >>> 0

//     // Steps 6-8.
//     const relativeTarget = target >> 0

//     let to = relativeTarget < 0 ?
//       Math.max(len + relativeTarget, 0) :
//       Math.min(relativeTarget, len)

//     // Steps 9-11.
//     const relativeStart = start >> 0

//     let from = relativeStart < 0 ?
//       Math.max(len + relativeStart, 0) :
//       Math.min(relativeStart, len)

//     // Steps 12-14.
//     const end = arguments[2]
//     const relativeEnd = end === undefined ? len : end >> 0

//     const _final = relativeEnd < 0 ?
//       Math.max(len + relativeEnd, 0) :
//       Math.min(relativeEnd, len)

//     // Step 15.
//     let count = Math.min(_final - from, len - to)

//     // Steps 16-17.
//     let direction = 1

//     if (from < to && to < (from + count)) {
//       direction = -1
//       from += count - 1
//       to += count - 1
//     }

//     // Step 18.
//     while (count > 0) {
//       if (from in O) {
//         O[to] = O[from]
//       } else {
//         delete O[to]
//       }

//       from += direction
//       to += direction
//       count--
//     }

//     // Step 19.
//     return O
//   }
// }


// if (!String.prototype.repeat) {
//   /**
//    * @param {number} n
//    * @return {string}
//    */
//   String.prototype.repeat = function(n) {
//     const s = (this + "")
//     let half = ""

//     n = parseInt(n, 10) || 0
//     if (n < 0 || n === Infinity) {
//       throw new RangeError
//     }
//     return  !n ? "" :
//             n % 2 ? (s + s.repeat(n-1)) :
//                     (half = s.repeat(n/2)) + half
//   }
// }

// if (!String.prototype.codePointAt) {
//   (function() {
//     'use strict'; // needed to support `apply`/`call` with `undefined`/`null`
//     var codePointAt = function(position) {
//       if (this == null) {
//         throw TypeError();
//       }
//       var string = String(this);
//       var size = string.length;
//       // `ToInteger`
//       var index = position ? Number(position) : 0;
//       if (index != index) { // better `isNaN`
//         index = 0;
//       }
//       // Account for out-of-bounds indices:
//       if (index < 0 || index >= size) {
//         return undefined;
//       }
//       // Get the first code unit
//       var first = string.charCodeAt(index);
//       var second;
//       if ( // check if it’s the start of a surrogate pair
//         first >= 0xD800 && first <= 0xDBFF && // high surrogate
//         size > index + 1 // there is a next code unit
//       ) {
//         second = string.charCodeAt(index + 1);
//         if (second >= 0xDC00 && second <= 0xDFFF) { // low surrogate
//           // http://mathiasbynens.be/notes/javascript-encoding#surrogate-formulae
//           return (first - 0xD800) * 0x400 + second - 0xDC00 + 0x10000;
//         }
//       }
//       return first;
//     };
//     if (Object.defineProperty) {
//       Object.defineProperty(String.prototype, 'codePointAt', {
//         'value': codePointAt,
//         'configurable': true,
//         'writable': true
//       });
//     } else {
//       String.prototype.codePointAt = codePointAt;
//     }
//   }());
// }

// if (!String.fromCodePoint) {
//   (function() {
//     var defineProperty = (function() {
//       // IE 8 only supports `Object.defineProperty` on DOM elements
//       try {
//         var object = {};
//         var $defineProperty = Object.defineProperty;
//         var result = $defineProperty(object, object, object) && $defineProperty;
//       } catch(error) {}
//       return result;
//     }());
//     var stringFromCharCode = String.fromCharCode;
//     var floor = Math.floor;
//     var fromCodePoint = function() {
//       var MAX_SIZE = 0x4000;
//       var codeUnits = [];
//       var highSurrogate;
//       var lowSurrogate;
//       var index = -1;
//       var length = arguments.length;
//       if (!length) {
//         return '';
//       }
//       var result = '';
//       while (++index < length) {
//         var codePoint = Number(arguments[index]);
//         if (
//           !isFinite(codePoint) ||       // `NaN`, `+Infinity`, or `-Infinity`
//           codePoint < 0 ||              // not a valid Unicode code point
//           codePoint > 0x10FFFF ||       // not a valid Unicode code point
//           floor(codePoint) != codePoint // not an integer
//         ) {
//           throw RangeError('Invalid code point: ' + codePoint);
//         }
//         if (codePoint <= 0xFFFF) { // BMP code point
//           codeUnits.push(codePoint);
//         } else { // Astral code point; split in surrogate halves
//           // http://mathiasbynens.be/notes/javascript-encoding#surrogate-formulae
//           codePoint -= 0x10000;
//           highSurrogate = (codePoint >> 10) + 0xD800;
//           lowSurrogate = (codePoint % 0x400) + 0xDC00;
//           codeUnits.push(highSurrogate, lowSurrogate);
//         }
//         if (index + 1 == length || codeUnits.length > MAX_SIZE) {
//           result += stringFromCharCode.apply(null, codeUnits);
//           codeUnits.length = 0;
//         }
//       }
//       return result;
//     };
//     if (defineProperty) {
//       defineProperty(String, 'fromCodePoint', {
//         'value': fromCodePoint,
//         'configurable': true,
//         'writable': true
//       });
//     } else {
//       String.fromCodePoint = fromCodePoint;
//     }
//   }());
// }

// if (!String.prototype.startsWith) {
//   /**
//    * @param {string} pattern
//    * @param {number=} start
//    * @return {boolean}
//    */
//   String.prototype.startsWith = function(pattern/*, start*/) {
//     if (__objectToString.call(pattern) === "[object RegExp]") {
//       throw new TypeError
//     }
//     pattern += ""
//     const source = this + ""
//     let start = arguments[1]

//     if (start < 0 || +start !== +start) {
//       start = 0
//     }
//     return source.length - start >= pattern.length &&
//             source.slice(+start, +start + pattern.length) === pattern
//   }
// }

// if (!String.prototype.endsWith) {
//   /**
//    * @param {string} pattern
//    * @param {number=} end
//    * @return {boolean}
//    */
//   String.prototype.endsWith = function(pattern/*, end*/) {
//     if (__objectToString.call(pattern) === "[object RegExp]") {
//       throw new TypeError
//     }
//     pattern += ""
//     const source = this + ""
//     let end = arguments[1]

//     if (end === undefined || end > source.length) {
//       end = source.length

//     } else if (end < 0 || +end !== +end) {
//       end = 0
//     }

//     return end >= pattern.length &&
//             source.slice(end - pattern.length, end) === pattern
//   }
// }

// if (!String.prototype.includes) {
//   /**
//    * @param {string} pattern
//    * @param {number=} start
//    * @return {boolean}
//    */
//   String.prototype.includes = function(pattern, start) {
//     if (__objectToString.call(pattern) === "[object RegExp]") {
//       throw new TypeError
//     }
//     return __stringIndexOf.call(this, pattern, start) !== -1
//   }
// }



/*********************

  ECMAScript 7

*********************/

/*
  These patches are compatible with IE6+ if ES5 & ES6 methods are patched
*/


// if (!Array.prototype.includes) {
//   /**
//    * @param {*} target Value to be found in Array
//    * @param {number=} start
//    * @return {boolean}
//    */
//   Array.prototype.includes = function(target, start) {
//     return target === target ? // Not searching for NaN
//       __arrayIndexOf.call(this, target, start) !== -1 :
//       __arraySlice.call(this, start).findIndex(Number.isNaN) !== -1
//   }
// }
