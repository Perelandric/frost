


;(
/**
 * @param {!Object} window
 * @param {!Object} DOM
 * @param {undefined} undefined
 */
function(window, DOM, undefined) {
  const document = window.document
  ,     __hasOwnProperty = Object.prototype.hasOwnProperty
  ,     __unshift = Array.prototype.unshift


  // Function() is needed so that closure compiler doesn't remove the @cc_on
  // The wrapping IIFE is to work around github #2214
  LEGACY_LIB && (function () {
    return new Function(
      "/*@cc_on " +
      "var __vsn = {1.0:3, 3.0:4, 5.0:5, 5.1:5, 5.5:5.5, 5.6:6," +
      "  5.7: window.XMLHttpRequest ? 7 : 6, 5.8:8, 9:9, 10:10" +
      "}[@_jscript_version] || Infinity;" +

      'if (__vsn < 12) {' +
      '  if (__vsn <= 6) { // Stop background image flicker' +
      '    try {' +
      '      document.execCommand("BackgroundImageCache", false, true);' +
      '    } catch(e){}' +
      '  }' +

      '  if (window.attachEvent) {' +
      '    window.attachEvent("onmouseover", __redraw_helper);' +
      '    window.attachEvent("onmouseout", __redraw_helper);' +
      '    window.attachEvent("onmouseup", __redraw_delayed);' +

      '  } else if (window.addEventListener) {' +
      '    window.addEventListener("mouseover", __redraw_helper, true);' +
      '    window.addEventListener("mouseout", __redraw_helper, true);' +
      '    window.addEventListener("mouseup", __redraw_delayed, true);' +
      '  }' +
      '}' +

      'function __redraw_helper(event) {' +
      '  document.body.className = document.body.className' +
      '}' +
      'function __redraw_delayed() {' +
      '  setTimeout(__redraw_helper, 30)' +
      '}' +
      "@*/;"
    )()
  })()


  // Fix .insertCell and .insertRow where having no argument could fail
  let __tr = document.createElement("tr")

  // Firefox < 20 and some older IE/Webkit
  try {
    __tr.insertCell() // <-Need a cell in place to test the last pos insert
    __tr.insertCell().appendChild(document.createTextNode("1"))
    __tr.insertCell(-1).appendChild(document.createTextNode("2"))

    // Make sure no args and -1 is adding it to the end
    if (__tr.cells[1].firstChild.data !== "1" ||
        __tr.cells[2].firstChild.data !== "2") {
      throw "" // needs the fix
    }

  } catch(e) {
    const __insertRowFix = function(idx) {
      let par = this

      if (!arguments.length || idx == -1) {
        if (this.nodeName.toUpperCase() === "TABLE" && this.tBodies.length) {
          par = this.tBodies[this.tBodies.length-1]
        }
        idx = par.rows.length
      }
      return par.insertBefore(document.createElement("tr"), par.rows[+idx])
    }

    const __insertCellFix = function(idx) {
      if (!arguments.length || idx == -1) {
        idx = this.cells.length
      }
      return this.insertBefore(document.createElement("td"), this.cells[+idx])
    }

    HTMLTableElement.prototype.insertRow =
    HTMLTableSectionElement.prototype.insertRow = __insertRowFix

    HTMLTableRowElement.prototype.insertCell = __insertCellFix
  }

  __tr = null



  /***************************

      parentElement fix

  ***************************/

  const __parentElementFix = {get: function() {
    const p = this.parentNode
    return p && p.nodeType === 1 ? p : null
  }}

  // <= FF8 doesn't have `.parentElement` at all.
  if (!("parentElement" in document.documentElement)) {
    Object.defineProperty(Element.prototype, "parentElement", __parentElementFix)
  }

  // Opera and IE9+ only have `.parentElement` on actual Elements.
  // I need to test actual nodes since in Chrome, "parentElement" doesn't show
  // up on the prototypes.
  if (!("parentElement" in document.createTextNode(""))) {
    Object.defineProperty(Text.prototype, "parentElement", __parentElementFix)
  }

  if (!("parentElement" in document)) {
    Object.defineProperty(Document.prototype, "parentElement", __parentElementFix)
  }




  /***************************

      ClassList fix

  ***************************/

  // IE < 10
  // FF < 3.6
  // Ch < 8
  // OP < 11.5
  // Sa < 5.1
  if (!window.DOMTokenList) {
    const __classlist_re__ = /\s+/
    /**
     * @constructor
     * @param {*} elem
     */
    const ClassList = function(elem) {
      this._elem = elem
    }

    ClassList.prototype.__parse = function() {
      return (this._elem.className = this._elem.className.trim())
        .split(__classlist_re__)
    }

    ClassList.prototype.item = function (i) {
      return this.__parse()[i] || null
    }

    ClassList.prototype.contains = function (clss) {
      clss += ""
      if (clss === "" || __classlist_re__.test(clss)) {
        throw "Invalid argument"
      }
      return DOM.hasClass(this._elem, clss)
    }

    /**
     * @param {...string} args
     */
    ClassList.prototype.add = function (...args) {
      DOM.addClass(this._elem, ...args)
    }
    /**
     * @param {...string} args
     */
    ClassList.prototype.remove = function (...args) {
      DOM.removeClass(this._elem, ...args)
    }

    /**
     * @param {...string} clss
     * @param {boolean=} doAdd
     */
    ClassList.prototype.toggle = function (clss, doAdd) {
      return DOM.toggleClass(this._elem, ...arguments)
    }

    ClassList.prototype.toString = function () {
      return (this._elem.className = this.__parse().join(" "))
    }

    Object.defineProperty(ClassList.prototype, "length", {
      get: function() {
        return this.__parse().length
      }
    })

    Object.defineProperty(Element.prototype, "classList", {
      get: function () {return new ClassList(this)}
    })

  } else { // This is for native impls.
    // IE 10 - latest (Fixed in Edge)
    // FF 3.6 - 25 (Fixed in 26)
    // Sa 5.1 - 5.x (Fixed in 6)
    // Op 11.5 - 12.x (Fixed in 15)
    // Ch 8 - 23 (Fixed in 24)

    const __cl__ = document.createElement("p").classList

    // variadic test
    if (__cl__.add("a", "b"), !__cl__.contains("b")) {
      /**
       * @this {DOMTokenList}
       * @param {string} token
       * @param {...string} args
       */
      window.DOMTokenList.prototype.add = function(token, ...args) {
        DOM.addClass(this, ...args)
      }

      /**
       * @this {DOMTokenList}
       * @param {string} token
       * @param {...string} args
       */
      window.DOMTokenList.prototype.remove = function(token, args) {
        DOM.removeClass(this, ...args)
      }
    }

    // switch arg test
    if (__cl__.toggle("a", true), !__cl__.contains("a")) {
      /**
       * @this {DOMTokenList}
       * @param {string} token The CSS class to toggle from this element.
       * @param {boolean=} doAdd
       * @return {boolean} False if the token was removed; True otherwise.
       */
      window.DOMTokenList.prototype.toggle = function (token, doAdd) {
        return DOM.toggleClass(this, ...arguments)
      }
    }
  }



  if (!Element.prototype.matches) {
    Element.prototype.matches =
      Element.prototype.webkitMatchesSelector ||
      Element.prototype.mozMatchesSelector ||
      Element.prototype.msMatchesSelector ||
      Element.prototype.oMatchesSelector ||
      function (sel) {
        return Query.matches(this, sel)
      }
  }



  if (!Element.prototype.remove) {
    /**
     * @this {Node}
     */
    Text.prototype.remove =
    Element.prototype.remove = function() {
      this.parentNode.removeChild(this)
    }
  }



  const __innerHTML_fix_elements = (function() {
    const table = document.createElement("table")
    ,     select = document.createElement("select")
    ,     col = document.createElement("col")
    ,     colgroup = document.createElement("colgroup")
    ,     style = document.createElement("style")
    //,   frameset = document.createElement("frameset")
    //,   title = document.createElement("title")

    try {
      table.innerHTML = "<tbody><tr><td></td></tr></tbody>"
      DOM.empty(table)

      return null

    } catch (e) {
      DOM.empty(table)

      return {
        "TABLE": { depth: 0, root: table, preHTML: "", postHTML: "" },
        "TBODY": { depth: 1, root: table, preHTML: "<tbody>", postHTML: "</tbody>" },
        "THEAD": { depth: 1, root: table, preHTML: "<thead>", postHTML: "</thead>" },
        "TFOOT": { depth: 1, root: table, preHTML: "<tfoot>", postHTML: "</tfoot>" },
        "TR": { depth: 2, root: table, preHTML: "<tbody><tr>", postHTML: "</tr></tbody>" },
        "SELECT": { depth: 0, root: select, preHTML: "", postHTML: "" },
        "OPTGROUP": { depth: 1, root: select, preHTML: "<optgroup>", postHTML: "</optgroup>" },
        "COL": { depth: 0, root: col, preHTML: "", postHTML: "" },
        "COLGROUP": { depth: 0, root: colgroup, preHTML: "", postHTML: "" },
        //    "FRAMESET":{depth:0, root:frameset, preHTML:"", postHTML:""},
        //    "TITLE": {depth:0, root:title, preHTML:"", postHTML:""},
        "STYLE": { depth: 0, root: style, preHTML: "", postHTML: "" }
      }
    }
  })()

  let __original_innerHTML_set = null

  if (__innerHTML_fix_elements) {
    __original_innerHTML_set = Object.getOwnPropertyDescriptor(
      Element.prototype, "innerHTML"
    ).set

    Element.prototype.insertAdjacentHTML = __insertAdjacentHTML_fix

    Object.defineProperty(Element.prototype, "innerHTML", {
      set: __innerHTML_set_fix
    })

    Object.defineProperty(Element.prototype, "outerHTML", {
      set: __outerHTML_set_fix
    })
  }

  if (!Element.prototype.insertAdjacentHTML) {
    Element.prototype.insertAdjacentHTML = __insertAdjacentHTML_fix
  }

  if (!("outerHTML" in document.documentElement)) {
    Object.defineProperty(Element.prototype, "outerHTML", {
      get: __outerHTML_get_fix,
      set: __outerHTML_set_fix
    })
  }

  function __innerHTML_set_fix(html) {
    // Manually remove the childNodes so their content doesn't get cleared.
    DOM.empty(this)

    if (!html.length) { // Nothing more to do
      return html
    }

    const data = __innerHTML_fix_elements &&
                 __innerHTML_fix_elements[this.nodeName.toUpperCase()]

    if (!data) {
      return __original_innerHTML_set ?
              __original_innerHTML_set.call(this, html) :
              (this.innerHTML = html)
    }

    let root = data.root

    // Make sure it's empty, just to be safe
    DOM.empty(root)

    let depth = data.depth
    const wrappedHtml = data.preHTML + html + data.postHTML

    if (__original_innerHTML_set) {
      __original_innerHTML_set.call(root, wrappedHtml)
    } else {
      root.innerHTML = wrappedHtml
    }

    // Traverse to the proper level.
    while (root && depth--) {
      root = firstElementChild(root)
    }

    // Append the new content.
    while (root.firstChild) {
      this.appendChild(root.firstChild)
    }

    root = null

    return html
  }


  /**
   * @param {!Element} ctx
   * @param {!string} pos
   * @return {!Array<!Element>}
   */
  function _do_adjacent(ctx, pos) {
    switch (pos.toLowerCase()) {
    case "beforebegin": return [ctx.parentNode, ctx]
    case "afterend":    return [ctx.parentNode, ctx.nextSibling]
    case "afterbegin":  return [ctx, ctx.firstChild]
    case "beforeend":   return [ctx, null]
    default:
      throw new SyntaxError("Invalid argument")
    }
  }

  if (!Element.prototype.insertAdjacentElement) {
    Element.prototype.insertAdjacentElement = function (pos, el) {
      const [par, targ] = _do_adjacent(this, pos)
      return (par && par.insertBefore) ? par.insertBefore(el, targ) : null
    }
  }


  function __insertAdjacentHTML_fix(pos, html) {
    const [par, targ] = _do_adjacent(this, pos)
    if (!par || !par.insertBefore) {
      return null
    }

    const tn = par.nodeType !== 1 || par.nodeName === "HTML" ?
      "body" :
      par.nodeName

    const temp = (this.ownerDocument || document).createElement(tn)
    __innerHTML_set_fix.call(temp, html)

    while (temp.firstChild) {
      par.insertBefore(temp.firstChild, targ)
    }
  }



  // Firefox <= 10
  function __outerHTML_get_fix() {
    const tag = this.tagName
    ,     result = ("<" + tag) + Array.from(this.attributes, function(attr) {
      return " " + attr.name + '="' + attr.value + '"'
    }).join("") + ">"

    if (__hasOwnProperty.call(_void_elements, tag.toUpperCase())) {
      return result
    }

    return result + this.innerHTML + "</" + tag + ">"
  }

  function __outerHTML_set_fix(newHTML) {
    __insertAdjacentHTML_fix.call(this, "beforebegin", newHTML)
    this.parentNode.removeChild(this)
  }




  // Firefox < 9
  /**
   * @param {Node} node
   * @return {boolean}
   */
  const __contains_fix = function(node) {
    do {
      if (this === node) {
        return true
      }
    } while ((node = /**@type {Document|Element}*/ (node.parentNode)))
    return false
  }

  if (!Text.prototype.contains) {
    /**
     * @param {Document|Element} node
     */
    Text.prototype.contains = function(node) {
      return __contains_fix.call(/**@type {Text}*/ (this), node)
    }
  }

  if (!document.documentElement.contains) {
    /**
     * @param {Document|Element} node
     */
    Element.prototype.contains = function(node) {
      return __contains_fix.call(/**@type {Element}*/ (this), node)
    }
  }

  if (!document.contains) {
    /**
     * @param {Node} node
     */
    Document.prototype.contains = function(node) {
      return __contains_fix.call(/**@type {Document}*/ (this), node)
    }
  }


// Event listener fixes for various browsers (not including IE8 and lower)

  /************************
      mouseenter support
  ************************/
  // Ch < 30
  // FF < 10
  // Op < 17
  // Sa < 6.1 (6.0?)
  function _need_mouseenter() {
    // More mouseenter info: http://stackoverflow.com/questions/18730135
    // http://jsbin.com/forehobefa/2/edit
    return !("onmouseenter" in document.documentElement)
  }



  /************************
      focusin/out support
  ************************/
  // TODO: I think this needs to be adjusted so that if a focus/blur happens *during*
  //        a focusin/out event, it does not trigger another focusin/out (or someting
  //        like that). There's also something about it not triggering when the
  //        window/document gets a focus/blur event.

  // Ch < 14 (maybe lower)
  // FF All
  // Sa < 6.1 (6.0?)
  function _need_focusinout() {
    if ("onfocusin" in document) {
      return false
    }

    // Chrome supports `focusin/out`, but it doesn't show up on elements
    let result = true

    const _i = document.createElement("input")
    _i.type = "hidden"

    document.documentElement.appendChild(_i)

    function _focusin_test_handler() {
      result = false
    }

    document.addEventListener('focusin', _focusin_test_handler, false)

    _i.focus()
    document.removeEventListener("focusin", _focusin_test_handler, false)

    document.documentElement.removeChild(_i)

    return result
  }


  /*****************************************

      Listener fixes for legacy W3 browsers

  ******************************************/

/*
  if (!window.CustomEvent && document.createEvent) {
    /**
     * @type {function (new:CustomEvent, string, CustomEventInit=): ?}
     /
    window.CustomEvent = function CustomEvent ( event, params ) {
      params = params || { bubbles: false, cancelable: false, detail: undefined }

      const evt = document.createEvent('CustomEvent')
      evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail)
      return evt
    }
    CustomEvent.prototype = window.Event.prototype
  }
  */


  const _need_mouseenter_fix = _need_mouseenter()
  ,     _need_focusinout_fix = _need_focusinout()



 /**********************************
     event.target gives text node
 **********************************/
  // Chrome < 28
  // Safari -- browserhacks.com test for older Safari -v
  if (_need_mouseenter_fix || /constructor/i.test(window.HTMLElement)) {
    if (Object.getOwnPropertyDescriptor) {
      const __target_desc = Object.getOwnPropertyDescriptor(Event.prototype, "target")

      if (__target_desc && __target_desc.get) {
        const event_get = __target_desc.get
        //,   event_set = __target_desc.set

        Object.defineProperty(Event.prototype, "target", {
          get: function() {
            const targ = event_get.call(this)
            return targ && targ.nodeType === 3 ? targ.parentNode : targ
          }/*,
          set: function(val) {
            event_set.call(this, val)
          }
          */
        })
      }
    }
  }

  const MOUSEENTER_FIX = 1
  const FOCUSINOUT_FIX = 2

  function get_fix_for(type) {
    switch (type) {
    case "mouseenter": case "mouseleave":
      return MOUSEENTER_FIX
    case "focusin": case "focusout":
      return FOCUSINOUT_FIX
    }
    return 0
  }

  function type_list_str(type, captures) {
    return "__" + type + "_cap:" + !!captures + "_list__"
  }

  const _addEventListener = document.documentElement.addEventListener
  ,     _removeEventListener = document.documentElement.removeEventListener

  Window.prototype.addEventListener = function(type, handler, captures) {
    _legacy_event_listener_fix.call(
      /**@type {Window}*/ (this), type, handler, captures)
  }
  Document.prototype.addEventListener =
  Element.prototype.addEventListener = function(type, handler, captures) {
    _legacy_event_listener_fix.call(
      /**@type {Node}*/ (this), type, handler, captures)
  }


  /**
   * @this {!Window|!Node}
   * @param {string} type
   * @param {(EventListener|function (!Event): (boolean|undefined)|null)} handler
   * @param {object|boolean|undefined} captures
   */
  function _legacy_event_listener_fix(type, handler, captures) {
    // Until the "options" argument is patched, just grab the captures
    if (captures && typeof captures === "object") {
      captures = !!captures.capture
    } else {
      captures = !!captures
    }

    // FF < 9
    if (handler == null) {
      return
    }

    if (typeof handler !== "function" && typeof handler !== "object") {
      throw "Invalid argument"
    }

    const fix_kind = get_fix_for(type)
    let simple_event_type_substitute = ""

    if (!fix_kind && !simple_event_type_substitute) {
      return _addEventListener.call(
        /**@type {Element}*/ (this), type, handler, captures
      )
    }

    const type_list = type_list_str(type, captures)
    ,     fixes = this[type_list] || (this[type_list] = [])

    // Make sure we're not binding the same handler/event/capture twice.
    for (const fix of fixes) {
      if (fix.orig_handler === handler) {
        return
      }
    }

    // The event handler
    const fix =
      simple_event_type_substitute ?
        new SimpleSubstituteFix(type, handler, simple_event_type_substitute, captures) :

      fix_kind === MOUSEENTER_FIX ?
        new MouseEnterLeaveFix(type, handler, captures) :

      fix_kind === FOCUSINOUT_FIX ?
        new FocusInOutFix(type, handler, captures) :

        null // <-- null should never happen

    if (!fix) {
      throw "Internal error"
    }
    fixes.push(fix)

    return _addEventListener.call(
      /**@type {Element}*/ (this),
      fix.bound_type,
      fix,
      fix.bound_captures
    )
  }



  Window.prototype.removeEventListener = function(type, handler, captures) {
    _legacy_event_remover_fix.call(
      /**@type {Window}*/ (this), type, handler, captures)
  }
  Document.prototype.removeEventListener =
  Element.prototype.removeEventListener =  function(type, handler, captures) {
    _legacy_event_remover_fix.call(
      /**@type {Element}*/ (this), type, handler, captures)
  }

  /**
   * @this {!Window|!Node}
   * @param {string} type
   * @param {(EventListener|function (!Event): (boolean|undefined)|null)} handler
   * @param {object|boolean|undefined} captures
   */
  function _legacy_event_remover_fix(type, handler, captures) {
    // Until the "options" argument is patched, just grab the captures
    if (captures && typeof captures === "object") {
      captures = !!captures.capture
    } else {
      captures = !!captures
    }

    const fixes = this[type_list_str(type, captures)]
    if (!fixes) {
      return _removeEventListener.call(
        /**@type {Element}*/ (this), type, handler, captures
      )
    }

    fixes.some((fix, i, orig) => {
      if (fix.orig_handler !== handler) {
        return
      }
      _removeEventListener.call(
        /**@type {!Element}*/ (this), fix.bound_type, fix, !!fix.bound_captures
      )

      orig.splice(i, 1)

      return true
    })
  }

  function BoundHandlerFix(type, bound_type, orig, captures, bound_captures) {
    this.type = type
    this.bound_type = bound_type

    this.orig_handler = orig
    this.is_func = typeof orig === "function"

    this.captures = captures
    this.bound_captures = bound_captures
  }

  BoundHandlerFix.prototype.invoke = function(event, data) {
    if (data) {
      this.setEventData(event, data)
    }

    if (this.is_func) {
      this.orig_handler.call(event.currentTarget, event)

    } else {
      this.orig_handler.handleEvent(event)
    }
  }

  BoundHandlerFix.prototype.setEventData = function(event, data) {
    for (const p in data) {
      delete event[p]

      Object.defineProperty(event, p, {
        value: data[p],
        enumerable: false,
        configurable: true,
        writable: true
      })
    }
  }


  /**********************
    simple subsititute
  **********************/

  function SimpleSubstituteFix(type, handler, bound_type, captures) {
    BoundHandlerFix.call(this, type, bound_type, handler, captures, captures)
  }
  SimpleSubstituteFix.prototype = Object.create(BoundHandlerFix.prototype)

  SimpleSubstituteFix.prototype.handleEvent = function(event) {
    this.invoke(event, {type:this.type})
  }


  /**********************
    mouseenter/leave fix
  **********************/

  function MouseEnterLeaveFix(type, handler, captures) {
    BoundHandlerFix.call(
      this,
      type,
      type === "mouseenter" ? "mouseover" : "mouseout",
      handler,
      captures,
      captures
    )
    this.handler = handler
  }
  MouseEnterLeaveFix.prototype = Object.create(BoundHandlerFix.prototype)

  MouseEnterLeaveFix.prototype.handleEvent = function(event) {
    const cur_targ = event.currentTarget
    ,     rel_targ = event.relatedTarget

    if (rel_targ === cur_targ.parentNode || !cur_targ.contains(rel_targ)) {
      this.invoke(event, {type:this.type, target:cur_targ})
    }
  }


  /**********************
    mouseenter/leave fix
  **********************/

  function FocusInOutFix(type, handler, captures) {
    BoundHandlerFix.call(
      this,
      type,
      type === "focusin" ? "focus" : "blur",
      handler,
      captures,
      true
    )
  }
  FocusInOutFix.prototype = Object.create(BoundHandlerFix.prototype)

  FocusInOutFix.prototype.handleEvent = function(event) {
    // The first element that captured the focus/blur handles the phases.
    // Nested elements that bound the same event are handled here.
    if (event.__focusinout_is_handled) {
      return
    }

    event.__focusinout_is_handled = true

    this.setEventData(event, {type:this.type}) // Set the new event type first

    // Traverse from the event.target all the way up, collecting event data
    let cur_targ = event.target
    const orig_targ = event.currentTarget
    ,     handlers = []

    do { // Add handlers in the proper captures/bubbles order.
      const h_cap = cur_targ[type_list_str(this.type, true)]
      ,     h_bub = cur_targ[type_list_str(this.type, false)]

      if (h_cap) { // unshift capturing handlers
        handlers.unshift(...h_cap)
      }
      if (h_bub) { // push bubbling handlers
        handlers.push(...h_bub)
      }
    } while ((cur_targ = cur_targ.parentNode))

    // Handlers should be in proper order, so invoke them.
    handlers.forEach(function(data) {
      data.fix.invoke(event, {currentTarget:data.cur_targ})
    })

    // Reset the event data
    this.setEventData(event, {type:this.bound_type, currentTarget:orig_targ})
  }

})(this, DOM, void 0);









/******************************

  addEventListener options argument

  This is the start of a patch. I won't bother integrating this until most
  modern browsers support this behavior.

******************************/

/*
!function(__a, __i) {
  if (__a.click) {

    // TODO: Chrome added "once" support a couple versions before "passive"
    // support, so we actually need to check for "passive" instead.

    __a.addEventListener("click", function() { __i++ }, {once: true})
    __a.click() // should fire only once
    __a.click()

    if (__i === 1) { // fired only once, so the object is supported
      return
    }

    const __orig_aEL = Element.prototype.addEventListener

    const ListenerCtor = function(listener, options) {
      this.listener = listener
      this.isFunc = typeof listener === "function"

      if (!isFunc && (!listener || typeof listener !== "object")) {
        throw new TypeError("Argument 2 of addEventListener is not an object.")
      }

      this.once = !!options.once
      this.passive = !!options.passive
      this.capture = !!options.capture
    }

    ListenerCtor.prototype.handleEvent = function(event) {
      if (this.passive) { // block calls to preventDefault
        Object.defineProperty(event, "preventDefault", {
          value: this.preventDefault
        })
      }

      if (this.once) { // Remove the listener on the first event occurrence
        event.currentTarget.removeEventListener(event.type, this, this.capture)
      }

      const listener = this.listener

      if (this.isFunc) {
        listener(this.event.currentTarget, ...arguments)

      } else if (listener && typeof listener.handleEvent === "function") {
        listener.handleEvent(this.event.currentTarget, ...arguments)

      } else {
        throw new TypeError(
          "TypeError: Property 'handleEvent' is not callable."
        )
      }
    }

    // Just stored here. Not actually called on the object
    ListenerCtor.prototype.preventDefault = function() {
      if (window.console) {
        console.log(
          "Ignoring ‘preventDefault()’ call on event of type ‘click’ from a listener registered as ‘passive’."
        )
      }
    }

    const __aEL_fix = {
      value: function(type, listener, ops) {
        if (!ops || typeof ops !== "object" || (!ops.once && !ops.passive)) {
          __orig_aEL.call(this, type, listener, !!ops.capture)

        } else {
          __orig_aEL.call(
            this, type, new ListenerCtor(listener, ops), !!ops.capture
          )
        }
      }
    }

    Object.defineProperty(Window.prototype, "addEventListener", __aEL_fix)
    Object.defineProperty(Document.prototype, "addEventListener", __aEL_fix)
    Object.defineProperty(Element.prototype, "addEventListener", __aEL_fix)
  }

  __a = null
}(document.createElement("a"), 0)
*/
