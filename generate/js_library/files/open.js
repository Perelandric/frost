
/** @define {boolean} */
const DEBUG_MODE = true

/** @define {boolean} */
const LEGACY_LIB = true

// Some features are not supported in versions LOWER than:
//  Safari 6
//  Firefox 8
//  IE 9
//  Chrome 10
//  Opera 12.10
//
// In a new Function so that JS processors don't do any mangling
const __unsupported = LEGACY_LIB && new Function(
  'try{ return new Function("' +
  // Freeze, zero-width char idents and trailing commas?
  'const o = Object.freeze({ _\\u200c\\u200d: 1, });' +

  // Check keyworkd props and whether it's actually frozen.
  'o.if = true;' +
  'return !!o.if' +
  '")() }catch(e){return true}'
)()
