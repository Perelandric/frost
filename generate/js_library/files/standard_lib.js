

/*
An error is intentionally thrown in the middle of this file in browsers
that fall below the official level of support. This prevents methods
from having the appearance of support in very old browsers.
*/

const __slice = Array.prototype.slice


/**************************

  Edge 14 still needs this
  so the fix is here.

**************************/

const __childElementCountFix = {
  /**
   * @this {!Node}
   * @return {!number} The number of child Elements found under `this`.
   */
  get: function () {
    let n = 0
    for (const ch of this.childNodes) {
      if (ch.nodeType === 1) {
        n += 1
      }
    }
    return n
  }
}

if (!("childElementCount" in document)) {
  Object.defineProperty(Document.prototype, "childElementCount", __childElementCountFix)
}

if (window.DocumentFragment &&
    !("childElementCount" in document.createDocumentFragment())) {
  Object.defineProperty(DocumentFragment.prototype, "childElementCount", __childElementCountFix)
}

// IE8
if (!("childElementCount" in document.documentElement)) {
  Object.defineProperty(Element.prototype, "childElementCount", __childElementCountFix)
}





/**
 * @param {!string} classes The full string of classes from an element
 * @param {!string} clss The individual class being checked
 * @param {!number} idx The index of the starting point within `classes`
 * @return {!number} The next starting index where a match was found, or -1
 */
function _class_index_from(classes, clss, idx) {
  while ((idx = classes.indexOf(clss, idx)) !== -1) {
    if (classes.charAt(idx + clss.length).trim()) { // No space After?
      // The next possible match starts at two past the now known non-space.
      idx += clss.length + 2

    } else if (classes.charAt(idx-1).trim()) { // No space before?
      // The next possible match starts at one past the known space after.
      idx += clss.length + 1

    } else {
      return idx
    }
  }

  return -1
}


/**
 * @param {Element} el
 * @param {!string} clss
 * @return {boolean}
 */
function _add_class(el, clss) {
  if (!el.className) {
    el.className = clss
    return true
  }
  if (_class_index_from(el.className, clss) === -1) {
    el.className += " " + clss
    return true
  }
  return false
}

/**
 * Remove the given `clss` from `el`. If `idx` >= `0`, the first class has
 * already been found.
 *
 * @param {Element} el
 * @param {!string} clss
 * @param {!number} idx
 * @return {boolean}
 */
function _remove_class_from_idx(el, clss, idx) {
  let classes = el.className
  if (!classes) {
    return false
  }

  if (idx !== idx) { // NaN means the first idx has not yet been searched
    idx = _class_index_from(classes, clss, 0)
  }

  let removed = false
  while (idx !== -1) {
    removed = true
    classes = (idx ? classes.slice(0, idx) : "") +
                      classes.slice(idx + clss.length + 1)
    idx = _class_index_from(classes, clss, idx)
  }

  if (removed) {
    el.className = classes.trim()
  }

  return removed
}


/**
 * @dict
 */
const _fix_props = {
  "class":"className"
, "innerText":"textContent"
, "for":"htmlFor"
}


const DOM = {
  /**
   * @param {!Element|Document|string} el
   * @param {string=} sel
   * @return {Element}
   */
  q: function(el, sel) {
    if (typeof el === "string") {
      sel = el
      el = document
    }

    if (LEGACY_LIB) {
      try {
        return el.querySelector(sel)
      } catch(e) {
        return window.Query.one(el, sel)
      }

    } else {
      return el.querySelector(sel)
    }
  },

  /**
   * @param {!Element|Document|string} el
   * @param {string=} sel
   * @return {!Array<!Element>}
   */
  qa: function(el, sel) {
    if (typeof el === "string") {
      sel = el
      el = document
    }

    if (LEGACY_LIB) {
      try {
        return el.querySelectorAll(sel)
      } catch(e) {
        return window.Query.all(el, sel)
      }

    } else {
      return el.querySelectorAll(sel)
    }
  },


  /**
   * @param {Element} el
   * @param {!string} clss
   * @return {boolean}
   */
  hasClass: function(el, clss) {
    return !!el && _class_index_from(el.className, clss, 0) !== -1
  },


  /**
   * @param {Element} el
   * @param {...!string} toAdd
   * @return {boolean}
   */
  addClass: function(el, ...toAdd) {
    if (!el) {
      return false
    }

    el.className = el.className.trim()

    let added = false

    for (const clss of toAdd) {
      added = _add_class(el, clss) || added
    }

    return added
  },


  /**
   * @param {Element} el
   * @param {...!string} toRemove
   * @return {boolean}
   */
  removeClass: function (el, ...toRemove) {
    if (!el) {
      return false
    }

    if (!(el.className = el.className.trim())) {
      return false
    }

    let removed = false

    for (const clss of toRemove) {
      removed = _remove_class_from_idx(el, clss, NaN) || removed

      if (!el.className) {
        break
      }
    }

    return removed
  },


  /**
   * @param {Element} el
   * @param {!string} clss
   * @param {boolean=} swtch
   * @return {boolean}
   */
  toggleClass: function(el, clss, swtch) {
    if (!el) {
      return false
    }
    let idx = NaN // Start with NaN

    el.className = el.className.trim()

    swtch = arguments.length > 2 ?
      !!swtch :
      (idx = _class_index_from(el.className, clss, 0)) === -1

    if (swtch) { // add the class
      if (!(idx >= 0)) { // Checks for negative or NaN
        _add_class(el, clss)
      } else {
        // already has the class
      }

    } else if (idx !== -1) {
      _remove_class_from_idx(el, clss, idx) // `idx` to be removed (or NaN)

    } else {
      // Nothing to remove
    }

    return swtch
  },


  /**
   * @param {!Element} el
   * @return {!Element}
   */
  empty: function(el) {
    while (el.firstChild) {
      el.removeChild(el.firstChild)
    }
    return el
  },


  /**
   * @param {!Element} el
   * @param {Object.<string>} props
   * @return {!Element}
   */
  setProps: function(el, props) {
    for (const p in props) {
      const val = props[p]
      ,     lower_p = p.toLowerCase()

      if (lower_p === "style") {
        if (typeof val === "object") {
          for (const s in val) {
            el.style[s] = val[s]
          }
        } else {
          el.style.cssText = "" + val
        }

      } else if (lower_p.startsWith("data-")) {
        el.setAttribute(lower_p, val)

      } else {
        el[_fix_props[lower_p] || p] = val
      }
    }
    return el
  },

  /**
   * Creates a copy of the collection and reverses the order of elements in
   * the collection as well as the DOM.
   *
   * The order of the collection may be out of sync with the DOM if there
   * were any impossible swaps, like between an ancestor and descendant.
   *
   * The copy of the collection is returned.
   *
   * @param {!Array<!Element>} coll
   * @return {!Array<!Element>}
   */
  reverse: function(coll) {
    coll = Array.from(coll)

    if (coll.length <= 1) {
      return coll
    }

    let i = 0
    ,   j = coll.length - 1

    const swap = DOM.swap

    while (i < j) {
      const temp = coll[i]

      swap((coll[i++] = coll[j]), (coll[j--] = temp))
    }

    return coll
  },

  /**
   * Creates a copy of the collection and shuffles the order of elements in
   * the collection as well as the DOM.
   *
   * The order of the collection may be out of sync with the DOM if there
   * were any impossible swaps, like between an ancestor and descendant.
   *
   * The copy of the collection is returned.
   *
   * @param {!Array<!Element>} coll
   * @return {!Array<!Element>}
   */
  shuffle: function(coll) {
    coll = Array.from(coll)

    if (coll.length > 1) {
      const swap = DOM.swap

      let i = coll.length - 1
      ,   r = 0

      for (; i >= 1; i = i - 1) {
        r = Math.floor(Math.random() * i)
        const temp = coll[r]

        if (swap((coll[r] = coll[i]), (coll[i] = temp)) === false) {
          // The swap was abandoned.
        }
      }
    }

    return coll
  },


  /**
   * Swap positions of the two elements provided. If one is a descendant of the
   * other, the swap is impossible and is abandoned.
   *
   * Returns `true` if the swap was successful or if the two arguments were the
   * same element, else `false`.
   *
   * @param {!Element} node_a
   * @param {!Element} node_b
   * @return {boolean}
   */
  swap: function(node_a, node_b) {
    if (node_a === node_b) {
      return true
    }

    if (node_b.nextSibling === node_a) {
      node_b.parentNode.insertBefore(node_a, node_b)

    } else {
      const next = node_b.nextSibling
      ,     par = node_b.parentNode

  		try {
        node_a.parentNode.insertBefore(node_b, node_a)
      } catch(e) {
      	return false
      }

      try {
        par.insertBefore(node_a, next)
      } catch(e) {
      	// restore `node_b` to its original location
        par.insertBefore(node_b, next)
        return false
      }
    }

    return true
  },


  /**
   * Takes the list of elements and sorts both the DOM and the list according
   * to the callback. If no callback is provided, they are sorted in document
   * order.
   *
   * The DOM is not updated until after the list is sorted.
   *
   * A list may end out ouf of sync with the DOM if the sort requires an
   * impossible swap of two elements, such as an ancestor and descendant
   * swapping positions.
   *
   * The list is always shallow copied before sorting. The copy is returned.
   *
   * @param {!(NodeList|HTMLCollection|Array<!Element>)} coll
   * @param {function(!Element, !Element):number} cb
   * @return {!Array<!Element>}
   */
  sort: function(coll, cb) {
    if (__unsupported) {
      return
    }
    coll = Array.from(coll)

    if (coll.length < 2) {
      return coll
    }

    if (typeof cb !== "function") {
      cb = sorter // Sort to document order.
    }

    const copy = coll.slice()
    ,     swap = DOM.swap
    coll.sort(cb)

    for (let i = 0; i < coll.length; i++) {
      const swp_idx = coll[i].__sort_idx__

      if (swp_idx !== i) {
        const tmp = copy[swp_idx]
        swap(copy[swp_idx] = copy[i], copy[i] = tmp)
        copy[swp_idx].__sort_idx__ = swp_idx
      }
      delete copy[i].__sort_idx__
    }

    return coll
  },


  /**
   * Returns a collection representing closest possible cousins.
   * If an element contains another, they'll be merged.
   * If an element contains all others, all will be merged into that one.
   *
   * @param {!(NodeList|HTMLCollection|Array<!Element>)} coll
   * @return {!Array<!Element>}
   */
  alignedAncestors: function(coll) {
    let shallowest = Infinity

    return Array.from(coll, node => {
      let dist = 0
      while ((node = node.parentNode)) {
        dist += 1
      }

      if (dist < shallowest) {
        shallowest = dist
      }

      return dist

    }).map((dist, i) => {
      let node = coll[i]
      for (; dist > shallowest; --dist) {
        node = node.parentNode
      }
      return node
    }).filter((node, i, arr) => arr.indexOf(node) === i)
  },



  /**
   * @param {!Element} el
   * @return {!Array<!Element>}
   */
  siblings: function(el, filter) {
    const res = []
    ,     orig = el
    ,     par = parElement(el)

    if (par) {
      el = firstElementChild(par)

      while (el) {
        if (el !== orig) {
          res.push(el)
        }
        el = nextElementSibling(el)
      }
    }

    return res
  },

  /**
   * @param {!Element} el
   * @param {number} from
   * @param {number} to
   * @return {!Array<!Element>}
   */
  siblingsRange: function(el, from, to, filter) {
    if (to < from) {
      return []
    }

    const res = []
    ,     orig = el
    ,     origFrom = from

    let i = 0
    while (--i >= from && (el = previousElementSibling(el))) {
      if (i <= to) {
        res.push(el)
      }
    }
    res.reverse()

    from = origFrom
    el = orig
    i = 0

    while (++i <= to && (el = nextElementSibling(el))) {
      if (i >= from) {
        res.push(el)
      }
    }

    return res
  },


  /**
   * @param {!Element} el
   * @return {!Array.<!Element>}
   */
  prevSibs: function(el) {
    const res = []
    while ((el = previousElementSibling(el))) {
      res.push(el)
    }
    return res
  },

  /**
   * @param {!Element} el
   * @param {number} at
   * @return {!Array.<!Element>}
   */
  prevSibAt: function(el, at) {
    if (at <= 0) {
      return null
    }
    while (at-- && (el = previousElementSibling(el))) {
    }
    return el
  },


  /**
   * @param {!Element} el
   * @return {!Array.<!Element>}
   */
  nextSibs: function(el) {
    const res = []
    while ((el = nextElementSibling(el))) {
      res.push(el)
    }
    return res
  },

  /**
   * @param {!Element} el
   * @param {number} at
   * @return {!Array.<!Element>}
   */
  nextSibAt: function(el, at) {
    if (at <= 0) {
      return null
    }
    while (at-- && (el = nextElementSibling(el))) {
    }
    return el
  },

  /**
   * @param {!Element} el
   * @return {!Array.<!Element>}
   */
  ancestors: function(el) {
    const res = []
    while ((el = parElement(el))) {
      res.push(el)
    }
    return res
  },

  /**
   * @param {!Element} el
   * @param {number} at
   * @return {Element}
   */
  ancestorAt: function(el, at) {
    if (at <= 0) {
      return null
    }

    while (at-- && (el = parElement(el))) {
    }
    return el
  },

  /**
   * @param {!Element} el
   * @param {number} at
   * @return {!Array.<!Element>}
   */
  descendantsAt: function(el, at) {
    if (at <= 0) {
      return []
    }

    let res = Array.from(children(el))

    while (--at > 0 && res.length) {
      res = res.reduce((r, _el) => r.concat(...children(_el)), [])
    }

    return res
  },

  /**
   * @param {!Element} el
   * @param {number} from
   * @param {number} to
   * @return {!Array.<!Element>}
   */
  descendantsRange: function(el, from, to) {
    if (from < 0) {
      from = 1
    }
    if (to < from) {
      return []
    }

    const res = DOM.descendantsAt(el, from)

    let prevLen = 0
    ,   currLen = res.length

    while (++from <= to && prevLen !== currLen) {
      res.slice(prevLen).forEach(_el => res.push(...children(_el)))

      prevLen = currLen
      currLen = res.length
    }

    return res
  },

  /**
   * @param {!Element} el
   * @param {number} n
   * @return {!Array.<!Element>}
   */
  cousins: function (el, n, filter) {
    if (!el || !(el = parElement(el)) || n <= 0) {
      return []
    }

    const par = el // This one is for the descendants filter
    let nn = n

    while (nn && parElement(el)) {
      el = parElement(el)
      nn -= 1
    }

    // Descend down to the level of the original's parent, excluding that parent
    let res = DOM.descendantsAt(el, n-nn, d => d !== par)

    // Descend down one more level to the starting level to get the final result.
    res = res.reduce((r, _el) => r.concat(...children(_el)), [])

    return res
  }
}







  // /**
  //  * @param {!(Node|NodeList|HTMLCollection|Array<!Element>)} roots
  //  * @return {!string}
  //  */
  // serialize: function(roots) {
  //   return "[" + DOM.mapAll(roots, DOM.serializeNode).join(",") + "]"
  // },
  // /**
  //  * @param {!Node} n
  //  * @return {!string}
  //  */
  // serializeNode: function(n) {
  //   switch (n.nodeType) {
  //   case 1:
  //     let items = '["' + n.nodeName.toLowerCase() + '"'
  //     const len = n.attributes.length
  //     for (let i = 0; i < len; i+=1) {
  //       items += ","
  //       if (i === 0) {
  //         items += "{"
  //       }
  //       const attr = /**@type {!Attr}*/ (n.attributes[i])
  //       // This favors the property value over the attribute value
  //       items +=
  //         '"' + attr.name + '":"' +
  //         (attr.name in n ? /**@type {string}*/ (n[attr.name]) : attr.value) +
  //         '"'

  //       if (i === len - 1) {
  //         items += "}"
  //       }
  //     }

  //     if (n.childNodes.length) {
  //       items += "," + Array.from(n.childNodes, DOM.serializeNode).join(",")
  //     }

  //     return items + "]"

  //   case 3:
  //     return '"' + /**@type {Text}*/ (n).data + '"'

  //   case 8:
  //     return '["#comment","' + /**@type {Comment}*/ (n).data + '"]'

  //   case 9:
  //     return '["#document",' +
  //       Array.from(n.childNodes, DOM.serializeNode).join(",") + ']'

  //   case 10:
  //     return '["!doctype ' + /**@type {DocumentType}*/ (n).name +
  //                     (n.publicId ? (' PUBLIC \\"' + n.publicId + '\\"') : "") +
  //                     (n.systemId ? (' \\"' + n.systemId + '\\"') : "") +
  //             '"]'

  //   case 11:
  //     return '["#document-fragment",' +
  //       Array.from(n.childNodes, DOM.serializeNode).join(",") + ']'
  //   }
  //   return ""
  // },


//function Person(item, i) {
//    if (item.foo || item.bar) {
//        return create(["div",
//           ["p", item.foo],
//            ["p", item.bar]
//        ])
//    }
//}
//
//function peeps(items) {
//    return create(["ul",
//        items.map(Person)
//    ])
//}

//function Entry(item) {
//  return create(
//    ["div", {className: "entry"},
//      ["h1", item.title],
//      ["h2", "By ", item.author.name],
//
//      ["div", {className: "body"},
//        item.body
//      ]
//    ])
//}
//
//let context = {
//    title: "My First Blog Post!",
//    author: {
//        id: 47,
//        name: "Yehuda Katz"
//    },
//    body: "My first post. Wheeeee!"
//};
//
//let e = Entry(context)

  // /**
  //  *  Several changes were made, so be sure to test this.
  //  *  Mimick HandleBarsJS.com's "select parent context" features.
  //  *
  //  *
  //  * Can recieve an Array of DOM nodes as well. If the first member of the Array
  //  * is a DOM node, then the entire array is processed as being added to the
  //  * `parent` if any, or simply processed and returned if no parent.
  //  *
  //  * The `null` and `undefined` values are never processed. This allows for holes
  //  * in the Array that don't end up as text nodes.
  //  *
  //  * @param {(string|Array)} newnode
  //  * @param {(Element|DocumentFragment)=} parent
  //  * @return {!(Node|Array<!Node>)}
  //  */
  // create: function(newnode, parent) {
  //   // If a node wasn't passed as `parent`, use `this` (if it's a node).
  //   if (!parent || !parent.nodeType || !parent.insertBefore) {
  //     // Use `this`. If it's not an element, it'll be caught before appending.
  //     parent = document.createDocumentFragment()
  //   }
  //   return internal_create_nodes.call(parent, newnode)


  //   /**
  //    * @this {!(Element|DocumentFragment)}
  //    * @param {string|Array.<*>} newnode
  //    * @return {!Node|!Array<!Node>}
  //    */
  //   function internal_create_nodes(newnode) {
  //     let created_node = null

  //     if (newnode && +newnode.nodeType === +newnode.nodeType) {
  //       created_node = newnode

  //     } else if (Util.isArrayLike(newnode) && newnode.length) {
  //       if (typeof newnode[0] !== "string") {
  //         // I still need to slice it for NodeList reindexing protection.
  //         return Array.from(newnode, internal_create_nodes, this)
  //       }

  //       let can_have_children = false

  //       const node_type = newnode[0]

  //       switch (node_type) {
  //       case "#text":
  //         created_node = document.createTextNode(__slice.call(newnode, 1).join(""))
  //         break

  //       case "#comment":
  //         created_node = document.createComment(__slice.call(newnode, 1).join(""))
  //         break

  //       case "#document":
  //       case "#document-fragment":
  //         created_node = document.createDocumentFragment()
  //         can_have_children = true
  //         break

  //       default:
  //         if (!/^[a-zA-Z][a-zA-Z0-9]*$/.test(node_type)) { // Check for invalid name
  //           return Array.from(newnode, internal_create_nodes, this)
  //         }

  //         created_node = document.createElement(node_type)
  //         can_have_children = !(node_type.toUpperCase() in _void_elements)

  //         let i = 1
  //         const props = /**@type {!Object}*/ (newnode[i])

  //         if (props && typeof props === "object" && !Util.isArrayLike(props)) {
  //           /**@type {!Element}*/ (created_node)._setProps(props)
  //           i+=1
  //         }

  //         if (can_have_children) {
  //           __slice.call(newnode, i).forEach(internal_create_nodes, created_node)
  //         }
  //       }

  //     } else {
  //       created_node = document.createTextNode(newnode == null ? "" : ("" + newnode))
  //     }

  //     return this.insertBefore(created_node, null)
  //   }
  // },




  // /**
  //  * @param {!Element} el
  //  * @param {!Array<string>} styles
  //  * @return {!Object<string>}
  //  */
  // getStyles: function(el, styles) {
  //   if (__unsupported) {
  //     return {}
  //   }

  //   /**
  //    * @dict
  //    */
  //   const result = {}
  //   let cmp = null

  //   for (const s of styles) {
  //     result[s] = styles[s] ||
  //       (cmp || (cmp = window.getComputedStyle(el))).getPropertyValues(s)
  //   }

  //   return result
  // },



  // /**
  //  * @param {!Element} el
  //  * @param {!Object} styles
  //  * @return {!Element}
  //  */
  // setStyles: function(el, styles) {
  //   if (__unsupported) {
  //     return el
  //   }

  //   for (const s in styles) {
  //     el.style[s] = styles[s] === null ?
  //       "" :// el.style.removeProperty(s)
  //       styles[s]
  //   }

  //   return el
  // },
