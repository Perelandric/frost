
/*
Everything in this file is meant to be IE6 callable
*/

const Util = {
  /**
   * Unsupported browsers can call this with the guarantee that the code will
   * not be invoked in supported browsers.
   *
   * @param {Function} handler The function to be invoked.
   * @param {Element} thisArg  The `this` value of the invoked function.
   * @param {...*} args        Any function arguments.
   * @return {*}
   */
  legacyCall: function(handler, thisArg, args) {
    if (__unsupported) {
      const __call = Function.call

      return __call.apply(__call, arguments)
    }
  },

  /**
   * Unsupported browsers can call this with the guarantee that the code will
   * not be invoked in supported browsers.
   * This is used in event handlers, primarily bound as attributes.
   *
   * @param {Function} handler The function to be invoked.
   * @param {Element} elem    The bound element.
   * @param {Event} event     The event object.
   * @param {...*} args     Any remaining args.
   * @return {*}
   */
  legacyEventCall: function(handler, elem, event, args) {
    if (__unsupported) {
      if (event) {
        arguments[2] = window.Util.legacyRepairEvent(event, elem)
      }

      const __call = Function.call

      return __call.apply(__call, arguments)
    }

    // Try to unbind the handler in supported browsers.
    if (elem && event && typeof elem["on" + event.type] === "function") {
      elem["on" + event.type] = null
    }
  },

  /**
   * This is a no-op in supported browsers.
   *
   * Repairs the event object (to a limited extent), and returns it.
   *
   * @param {Event} event     The event object.
   * @param {Event} elem      The bound element.
   * @return {Event}
   */
  legacyRepairEvent: function(event, elem) {
    if (__unsupported) {
      if (!(event = event || window.event)) {
        return event
      }

      if (!event.currentTarget) {
        event.currentTarget = elem
      }
      if (!event.target) {
        event.target = event.srcElement
      }
      if (!event.preventDefault) {
        event.preventDefault = __preventDefault
      }
      if (!event.stopPropagation) {
        event.stopPropagation = __stopPropagation
      }
    }

    return event
  },

  /**
   * Use this to bind events with an attempt to bind to unsupported browsers as well.
   * If the handler is an object implementing the EventListener interface, it is
   * expected that the object has a method with the same name as the event type. This
   * method will be bound in unsupported browsers.
   *
   * @param {!Element|Window|Document} el
   * @param {!string} evt_type
   * @param {!Function|EventListener} handler
   * @param {boolean} captures
   */
  compatAddListener: function(el, evt_type, handler, captures) {
    if (__unsupported && !el.addEventListener) {
      window.Util.compatHandlerSwap(el, evt_type, handler)

    } else {
      el.addEventListener(evt_type, handler, captures)
    }
  },

  /**
   * Use this to bind events with an attempt to bind to unsupported browsers as
   * well.
   * If the handler is an object implementing the EventListener interface, it
   * is expected that the object has a method with the same name as the event
   * type. This method will be bound in unsupported browsers.
   *
   * @param {!Element|Window|Document} el
   * @param {!string} evt_type
   * @param {!Function|EventListener} handler
   * @param {boolean} captures
   */
  compatRemoveListener: function(el, evt_type, handler, captures) {
    if (__unsupported && !el.removeEventListener) {
      const is_bound = typeof handler !== "function" &&
                      typeof handler.handleEvent === "function" &&
                      typeof handler[evt_type] === "function"

      if (!evt_type.startsWith("on")) {
        evt_type = "on" + evt_type
      }

      if (!el[evt_type]) {
        return
      }

      if (el[evt_type] === handler) {
        el[evt_type] = null
      } else if (el[evt_type].__unbinder) {
        el[evt_type] = el[evt_type].__rebind_without(el, handler, is_bound)
      }

    } else {
      el.removeEventListener(evt_type, handler, captures)
    }
  },

  // /**
  //  * This is a no-op in supported browsers.
  //  *
  //  * Binds the provided handler to the on[evt_type] of the given element, with
  //  * the guarantee that any previously bound handler will be invoked first.
  //  * It also repairs the event object.
  //  *
  //  * @param {Element} el       The event object.
  //  * @param {!string} evt_type The type of event to be bound.
  //  * @param {!Function|EventListener} fn The event handler function.
  //  */
  // compatHandlerSwap: function(el, evt_type, fn) {
  //   if (__unsupported) {
  //     let orig = null

  //     if (typeof fn !== "function" &&
  //         typeof fn.handleEvent === "function" &&
  //         typeof fn[evt_type] === "function") {
  //       orig = fn
  //       fn = fn[evt_type].bind(fn)
  //     }

  //     if (!evt_type.startsWith("on")) {
  //       evt_type = "on" + evt_type
  //     }

  //     let prev_handler = el[evt_type]

  //     // If `undefined` (not `null`), the event type must not be supported.
  //     if (prev_handler === undefined) {
  //       return
  //     }

  //     el[evt_type] = function(event) {
  //       event = Util.legacyRepairEvent(event, el)

  //       if (prev_handler) {
  //         Util.legacyEventCall(prev_handler, this, event)
  //       }

  //       Util.legacyEventCall(fn, this, event)
  //     }
  //     /**
  //      * Attempts to remove the handler that closes over the original from the chain
  //      * of closured handlers.
  //      *
  //      * @this {!Object}
  //      */
  //     el[evt_type].__rebind_without = function (el, removeFn, is_bound) {
  //       if (is_bound ? orig === removeFn : fn === removeFn) {
  //         const temp = prev_handler
  //         fn = orig = prev_handler = this.__rebind_without = null
  //         return temp
  //       }

  //       prev_handler = this.__rebind_without(el, removeFn, is_bound)
  //       return this
  //     }

  //     el = null
  //   }
  // },


  deselect: function(el) {
    if (document.selection) {
      document.selection.empty()
    }

    window.getSelection().removeAllRanges()

    el = el || document.activeElement

    if (el && el.nodeType === 1 && ("value" in el)) {
      const v = el.value
      el.value = ""
      el.value = v
    }
  },


  /**
   * JSON.parse substitute that returns `undefined` instead of throwing an error.
   * The error or an optional message is logged using `console.log`.
   *
   * @param {string} json
   * @param {string=} msg
   * @return {Object|undefined}
   */
  parseJSON: function(json, msg) {
    try { return JSON.parse(json) }
    catch(e) { log(e || msg) }
  },


  /**
   * Cycle through an Array in a sequential yet async manner. The `callback` is
   * invoked on the current item in the Array, and is passed...
   * - a "next" callback that is invoked when ready to do the next Array item
   * - the current item from the Array
   * - the current index of the Array
   * - the original array itself
   * The "next" callback can optionally be passed an index to deviate from the
   * otherwise sequential ordering of the original Array
   *
   * asyncLoop(["foo", "bar", "baz"], function(next, item, i, arr) {
   *   setTimeout(function() {
   *     console.log(item)
   *     next((i+1) % arr.length) // Make it cycle continuously
   *   }, 1000)
   * })
   *
   * @param {Array<*>} array
   * @param {function(function(number=), *, number, Array<*>)} callback
   * @param {*} thisArg
   */
  asyncLoop: function(array, callback, thisArg) {
    let i = 0
    /**
     * @param {number=} number
     */
    function next(number) {
      i = typeof number == "number" ? number : i+1

      if (0 <= i && i < array.length) {
        callback.call(thisArg, next, array[i], i, array)
      }
    }
    next(0)
  },

  /**
   * Check if an object is an array-like object. Criteria are that it has a `length`
   * propery that is a primitive number that is greater than -1, and isn't a DOM node
   * like `form` or `select` unless flagged as allowed.
   *
   * @param {*} obj The object to be tested.
   * @param {boolean=} allow_node_coll Allow nodes that are an array-like collection.
   * @return {boolean} The boolean true if the object is array-like, otherwise false.
   */
  isArrayLike: function(obj, allow_node_coll) {
    // truthy, object
    return !!obj && typeof obj === "object" &&
    // positive primitive number
    obj.length === (obj.length>>>0) &&
    // non-DOM node (unless allowed)
     (!!allow_node_coll || !obj.nodeType || !(obj+"").startsWith("[object HTML"))
  },

  /**
   * @param {string} url
   * @return {!Object}
   */
  parseURL: function(url) {
    __anchor.href = url
    return {
      hash:	 __anchor.hash,
      host:	 __anchor.host,
      hostname:__anchor.hostname,
  //	href:	 __anchor.href,
      pathname:__anchor.pathname,
      port:	 __anchor.port.slice(1),
      protocol:__anchor.protocol,
      search:	 __anchor.search
    }
  },


  /**
   * @return {!Cookies}
   */
  cookies: function() {
    return _this_cookies
  },


  /**
   * Check if cookies are enabled in the browser.
   *
   * @return {boolean}
   */
  cookiesEnabled: function() {
    return (("cookie" in document) &&
      ((document.cookie && document.cookie.length) ||
      (document.cookie = "test").indexOf.call(document.cookie, "test") > -1))
  },


  /****************************

      Storage helpers

  ****************************/

  /**
   * Retrieve browser compatible data storage
   *
   * @param {string} name
   * @return {Object}
   */
  getStorage: function(name) {
    return Util.parseJSON(
      window.localStorage   ? window.localStorage[name] :
      window.globalStorage  ? window.globalStorage[name] :
      Util.cookiesEnabled() ? Util.cookies().get(name, "/") :
      null
    ) || null
  },

  /**
   * Browser compatible data storage
   *
   * @param {string} name
   * @param {*} data
   * @param {boolean} cookieFallback
   */
  setStorage: function(name, data, cookieFallback) {
    try {
      data = JSON.stringify(data)

      if (window.localStorage) {
        window.localStorage[name] = data

      } else if (window.globalStorage) {
        window.globalStorage[name] = data

      } else if (cookieFallback && Util.cookiesEnabled()) {
        Util.cookies().set(name, data, 365, "/")
      }
    } catch (e) {
      log(e)
    }
  },

  /**
   * Clear browser compatible data storage
   *
   * @param {string} name
   */
  clearStorage: function(name) {
    if (window.localStorage) {
      window.localStorage[name] = ""
    }
    if (window.globalStorage) {
      window.globalStorage[name] = ""
    }
    if (Util.cookiesEnabled()) {
      Util.cookies().expire(name, "/")
    }
  },


  /****************************

    Form persistence

  ****************************/


  /**
   * Persist form values to storage or cookies.
   *
   * @param {Element} form
   * @param {string=} name
   */
  memorizeForm: function(form, name) {
    name = checkFormAndName(form, "memorize", name)
    if (!name) {
      return
    }

    const elems = getPersistentElems(form.elements, false)
    ,   o = {}

    for (const elem of elems) {
      memorize(o, elem)
    }

    Util.setStorage(name, o, true)
  },

  /**
   * Recover persisted form values.
   *
   * @param {Element} form
   * @param {string=} name
   */
  recallForm: function(form, name) {
    name = checkFormAndName(form, "recall", name)
    if (!name) {
      return
    }
    const data = Util.getStorage(name)

    if (!data) {
      return
    }

    for (const elem of getPersistentElems(form.elements, true)) {
      recall(data, elem)
    }
  },


  /**
   * Clear persisted form values.
   *
   * @param {Element} form
   * @param {string=} name
   */
  forgetForm: function(form, name) {
    name = checkFormAndName(form, "forget", name)
    if (!name) {
      return
    }
    Util.clearStorage(name)
  },


  /**
   * @param {!Element|!Array<!Element>} el_or_coll
   * @param {function(!Element, ...*):*} fn
   * @param {...*} args
   */
  forAll: function (el_or_coll, fn, ...args) {
    return (
      el_or_coll.nodeType ?
        [el_or_coll] :
        (el_or_coll instanceof Array ? el_or_coll : Array.from(el_or_coll))
    ).forEach(el => fn(el, ...args))
  },

  /**
   * @param {!Element|!Array<!Element>} el_or_coll
   * @param {function(!Element, ...*):*} fn
   * @param {...*} args
   * @return {!Array<*>}
   */
  mapAll: function (el_or_coll, fn, ...args) {
    return (
      el_or_coll.nodeType ?
        [el_or_coll] :
        (el_or_coll instanceof Array ? el_or_coll : Array.from(el_or_coll))
    ).map(el => fn(el, ...args))
  }
}



/*

 Internal functions and data

*/


/**
 * @struct
 */
const _void_elements = {
  "AREA":1,"BASE":1,"BR":1,"COL":1,"HR":1,"IMG":1,
  "INPUT":1,"LINK":1,"META":1,"PARAM":1,

  "COMMAND":1,"KEYGEN":1,"SOURCE":1 // HTML5
}

const __anchor = /**@type {!HTMLAnchorElement}*/(document.createElement("a"))

const _this_cookies = new Cookies()

function log() {
  if (window.console && console.log) {
    Function.apply.call(console.log, console, arguments)
  }
}

/**
 * @param {!Node} node
 * @return {Element}
 */
function parElement(node) {
  if (__unsupported) {
    while ((node = node.parentNode) && node.nodeType !== Node.ELEMENT_NODE) {}
    return node
  } else {
    return node.parentElement
  }
}
/**
 * @param {!Node} node
 * @return {Element}
 */
function previousElementSibling(node) {
  if (__unsupported) {
    while ((node = node.previousSibling) && node.nodeType !== Node.ELEMENT_NODE) {}
    return node
  } else {
    return node.previousElementSibling
  }
}
/**
 * @param {!Node} node
 * @return {Element}
 */
function nextElementSibling(node) {
  if (__unsupported) {
    while ((node = node.nextSibling) && node.nodeType !== Node.ELEMENT_NODE) {}
    return node
  } else {
    return node.nextElementSibling
  }
}
/**
 * @param {!Node} node
 * @return {Element}
 */
function firstElementChild(node) {
  if (__unsupported) {
    if ((node = node.firstChild) && node.nodeType !== Node.ELEMENT_NODE) {
      return nextElementSibling(node)
    }
    return node
  } else {
    return node.firstElementChild
  }
}
/**
 * @param {!Node} node
 * @return {Element}
 */
function lastElementChild(node) {
  if (__unsupported) {
    if ((node = node.lastChild) && node.nodeType !== Node.ELEMENT_NODE) {
      return previousElementSibling(node)
    }
    return node
  } else {
    return node.lastElementChild
  }
}
/**
 * @param {!Node} node
 * @return {!Array<!Element>}
 */
function children(node) {
  if (__unsupported) {
    let hasNull = false

    const res = Array.from(node.children || node.childNodes, ch => {
      if (ch.nodeType === Node.ELEMENT_NODE) {
        return ch
      }
      hasNull = true
      return null
    })
    return hasNull ? res.filter(Boolean) : res

  } else {
    return node.children
  }
}

/**
 * @this {!Event}
 */
function __preventDefault() {
  if (__unsupported) {
    this.returnValue = false
  }
}

/**
 * @this {!Event}
 */
function __stopPropagation() {
  if (__unsupported) {
    this.cancelBubble = true
  }
}

/****************

    Cookies

****************/


/**
 * @private
 * @constructor
 */
function Cookies() {
  /**
   * @dict
   */
  this._cache = {}
}

/**
 * Initialize the cache
 */
Cookies.prototype._initialize = function() {
  if (this._cache) {
    return
  }

  this._cache = {}

  _updateCookies(this)
}

/**
 * Get the cookie value for the provided name.
 *
 * @this {!Cookies}
 * @param {string} name     The name of the cookie.
 * @param {string=} path
 * @return {string}
 */
Cookies.prototype.get = function(name, path) {
  const full_name = name + "|" + (path || window.location.pathname)

  this._initialize()

  /**
   * @type {string}
   */
  let val = /**@type {string}*/ (this._cache[full_name])

  if (val) {
    return val
  }

  val = /**@type {string}*/ (this._cache[name])

  if (val) {
    delete this._cache[name]
    this._cache[full_name] = val
    return val
  }

  _updateCookies(this) // One last try to see if cookies were somehow added

  val = /**@type {string}*/ (this._cache[name])

  if (val) {
    delete this._cache[name]
    this._cache[full_name] = val
    return val
  }

  return ""
}

/**
 * Set the cookie value for the provided name.
 *
 * @this {!Cookies}
 * @param {string} name     The name of the cookie.
 * @param {string} val      The value for the cookie.
 * @param {number} exp_days The number of days in which the cookie should expire.
 * @param {string=} path    The subpath context of the cookie. Default is the current page.
 */
Cookies.prototype.set = function(name, val, exp_days, path) {
  const full_name = name + "|" + (path || window.location.pathname)

  this._initialize()

  this._cache[full_name] = val

  exp_days = Math.max(0, +exp_days || 0)

  document.cookie =
    name + "=" + encodeURIComponent(val) +
    "; expires=" + new Date(Date.now() + (exp_days*24*60*60*1000)).toGMTString() +
    "; path=" + (path || window.location.pathname) +
    ";"
}

/**
 * Immediately expire the cookie for the provided name and path.
 *
 * @this {!Cookies}
 * @param {string} name The name of the cookie.
 * @param {string=} path Subpath context of the cookie. Default is current page.
 */
Cookies.prototype.expire = function(name, path) {
    const full_name = name + "|" + (path || window.location.pathname)

    this._initialize()

    if (full_name in this._cache) {
      delete this._cache[full_name]
    }
    document.cookie = name + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" +
                    "; path=" + (path || window.location.pathname) +
                    ";"
}

/**
 * Update the cache
 */
function _updateCookies(cookies) {
  for (const cookie of document.cookie.split(";")) {
    const parts = cookie.split("=")
    cookies._cache[parts[0].trim()] = decodeURIComponent(parts[1] || "").trim()
  }
}


  /**
   * @param {!Node} a
   * @param {!Node} b
   * @return {number}
   */
function sorter(a, b) {
  if (__unsupported) {
    return 0
  }
  const pos = a.compareDocumentPosition(b)

  return !pos ? 0 : pos & 4 ? -1 : // v---push items in different docs the end
          pos & 2 ?  1 : (document.contains(/**@type {!Element}*/ (a)) ? -1 : 1)
}

/**
 * Recover element value from storage
 * @private
 *
 * @param {Object} o
 * @param {Element} elem
 */
function recall(o, elem) {
  const vals = o[elem.name]

  if (!vals) {
    return
  }

  switch (elem.nodeName.toUpperCase()) {
  case "SELECT":
    if (elem.selectedIndex != -1) { // Don't overwrite
      return
    }

    OUTER:
    for (const opt of elem.options) {
      for (const val of vals) {
        if (val == opt.value || (!opt.value && val == opt.text)) {
          opt.selected = true

          if (!elem.multiple) {
            break OUTER
          }
        }
      }
    }
    return

  case "INPUT":
    if (elem.type === "radio" || elem.type === "checkbox") {
      if (elem.checked) { // Don't overwrite
        return
      }

      for (const val of vals) {
        if (val == elem.value) {
          elem.checked = true
          break
        }
      }
      return
    }
  }

  if (elem.value) { // Don't overwrite
    return
  }

  elem.value = vals[0] || ""
}

/**
 * Remove element value from storage
 *
 * @private
 *
 * @param {Element} elem
 */
function forget(elem) {
  switch (elem.nodeName.toUpperCase()) {
  case "SELECT":
    if (elem.multiple) {
      for (const opt of elem.options) {
        opt.selected = false
      }

    } else {
      elem.selectedIndex = 0
    }
    return

  case "INPUT":
    if (elem.type === "radio" || elem.type === "checkbox") {
      // All checkboxes are cleared and radios have only the first one set.
      // (If there's only one radio, it is cleared.)
      elem.checked = elem.type === "radio" &&
                      elem.form.elements[elem.name][0] === elem
      return
    }
  }

  elem.value = ""
}

/**
 * Persist element value to storage
 * @private
 *
 * @param {Object} o
 * @param {Element} elem
 */
function memorize(o, elem) {
  let vals

  switch (elem.nodeName.toUpperCase()) {
  case "SELECT":
    if (elem.multiple) {
      vals = []

      for (const opt of elem.options) {
        if (opt.selected && (opt.value || opt.text)) {
          vals.push(opt.value || opt.text)
        }
      }

    } else if (elem.selectedIndex >= 0) {
      const opt = elem.options[elem.selectedIndex]
      vals = opt.value || opt.text ? [opt.value || opt.text] : []
    }
    break

  case "INPUT":
    if (elem.type === "radio" || elem.type === "checkbox") {
      if (elem.checked) {
        vals = [elem.value]
      }
    } else {
      vals = [elem.value]
    }
    break

  default:
    vals = [elem.value]
  }

  if (vals && vals.length) {
    if (o[elem.name]) {
      o[elem.name] = o[elem.name].concat(vals)
    } else {
      o[elem.name] = vals
    }
  }
}


/**
 * Gather persistent form elements. Supports IE6+
 * @private
 *
 * @param {NodeList} elems
 * @param {boolean} isRecover
 * @return {Array<Element>}
 */
function getPersistentElems(elems, isRecover) {
  const result = []
  ,     dnp = "data-nopersist"

  for (const elem of elems) {
    if (!elem.name) {
      continue // Skip elements that have no name
    }

    const nodeName = elem.nodeName.toUpperCase()
    ,     type = elem.type

    if (nodeName == "BUTTON" || (nodeName == "INPUT" && type == "submit")) {
      continue // Skip buttons
    }

    if (elem.attributes[dnp]) {
      continue // Skip elements that have instruction to not persist
    }

    if (!isRecover && nodeName == "INPUT" &&
      (type == "radio" || type == "checkbox") && !elem.checked) {
      continue // Skip radios and checkboxes that aren't checked
    }

    result.push(elem)
  }

  return result
}

/**
 * Make sure we have a proper form element and name for persist/recover ops.
 * This method does support IE6/7.
 *
 * @private
 * @param {Element} form
 * @param {string} action
 * @param {string=} name
 * @return {string}
 */
function checkFormAndName(form, action, name) {
  if (!form || form.nodeName.toUpperCase() != "FORM") {
    log("Expected form element. Found " + ((form && form.nodeName) || form))
    return ""
  }

  // Don't run if data-nopersist attribute is included.
  if (form.attributes["data-nopersist"]) {
    return ""
  }

  // Don't run memorize/recall/forget if data-no[action] attribute is included.
  if (form.attributes["data-no" + action]) {
    return ""
  }

  if (!name) {
    name = form.name
    if (!name) {
      log("Cannot " + action + " a form without a name")
      return ""
    }
  }
  return name
}
