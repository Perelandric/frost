package generate

import (
	"crypto/sha256"
	"encoding/json"
	"io/ioutil"
	"os"
	"sync"
	"time"
)

type buildItem struct {
	Time time.Time
	Hash [sha256.Size]byte
}

func (b *buildItem) Equal(other buildItem) bool {
	return b.Time.Equal(other.Time) && b.Hash == other.Hash
}

type buildData struct {
	CSS    buildItem
	Js     buildItem
	Config buildItem
	HTML   buildItem
	Code   buildItem
}

func (b *buildData) Equal(other *buildData) bool {
	return b.CSS.Equal(other.CSS) &&
		b.Js.Equal(other.Js) &&
		b.Config.Equal(other.Config) &&
		b.HTML.Equal(other.HTML) &&
		b.Code.Equal(other.Code)
}

type persistData struct {
	FrostVersion string
	State        stateItem
	SourceMap    map[string]*buildData
	mux          sync.RWMutex
}

func (p *persistData) sourceMapGet(pth string) (*buildData, bool) {
	p.mux.Lock()
	data, ok := p.SourceMap[pth]
	defer p.mux.Unlock()
	return data, ok
}
func (p *persistData) sourceMapSet(pth string, data *buildData) {
	p.mux.Lock()
	p.SourceMap[pth] = data
	p.mux.Unlock()
}
func (p *persistData) sourceMapRemove(pth string) {
	p.mux.Lock()
	delete(p.SourceMap, pth)
	p.mux.Unlock()
}

func (p *persistData) removeStale(time time.Time) {
	// Any component that is in the map but has no files that were given the
	// current build time must have been removed, so delete them from the map.
	for key, data := range p.SourceMap {
		p.mux.Lock()
		if data == nil ||
			data.CSS.Time.Before(time) &&
				data.Js.Time.Before(time) &&
				data.Config.Time.Before(time) &&
				data.HTML.Time.Before(time) &&
				data.Code.Time.Before(time) {
			delete(p.SourceMap, key) // Component must have been removed
		}
		p.mux.Unlock()
	}
}

func (p *persistData) storeTo(pth string) (err error) {
	p.mux.RLock()
	data, err := json.Marshal(p)
	p.mux.RUnlock()
	if err != nil {
		return err
	}

	newf, err := os.Create(pth)
	if err != nil {
		return err
	}
	defer newf.Close()

	_, err = newf.Write(data)
	return err
}

type persistKind int

const (
	_css = persistKind(1 << iota)
	_js
	_html
	_config
	_code
)

func (b *buildData) getKind(kind persistKind) *buildItem {
	switch kind {
	case _css:
		return &b.CSS
	case _js:
		return &b.Js
	case _html:
		return &b.HTML
	case _config:
		return &b.Config
	case _code:
		return &b.Code
	default:
		panic("Internal error; Unreachable")
	}
}

func (p *persistData) dataAndModState(
	kind persistKind,pth string,
) (data []byte, wasChanged bool) {

	bData, sMapOK := p.sourceMapGet(pth)
	data, err := ioutil.ReadFile(pth)

	if os.IsNotExist(err) || len(data) == 0 {
		if !sMapOK {
			return nil, false // Never existed
		}
		p.sourceMapRemove(pth) // File was removed, so update source map
		return nil, true
	}

	newHash := sha256.Sum256(data)
	if !sMapOK {
		bData = &buildData{}
	}

	p.mux.Lock()
	defer p.mux.Unlock()

	item := bData.getKind(kind)

	if item.Hash != newHash {
		item.Hash = newHash
		return data, true
	}
	return data, false
}
