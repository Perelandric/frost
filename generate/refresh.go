package generate

import (
	"Frost/generate/htmlparser"
	util "Frost/utilities"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"github.com/fsnotify/fsnotify"
)

type fileKind int

const (
	_dirFile = fileKind(1 << iota)
	_cssFile
	_jsFile
	_htmlFile
)

var watching = make(map[string]string)

func getFileKind(name string) fileKind {
	switch filepath.Ext(name) {
	case "":
		return _dirFile
	case ".css":
		return _cssFile
	case ".js":
		return _jsFile
	case ".html":
		return _htmlFile
	default:
		return 0
	}
}

func (g *Generator) initWatcher() (w *fsnotify.Watcher, err error) {
	defer func() {
		if err != nil && w != nil {
			w.Close()
		}
	}()

	if w, err = fsnotify.NewWatcher(); err != nil {
		return nil, err
	}
	if err = addToWatcher(g.sourcePathWithName("prelude"), w); err != nil {
		return nil, err
	}
	if err = addToWatcher(g.sourcePathWithName("frags"), w); err != nil {
		return nil, err
	}
	if err = addToWatcher(g.sourcePathWithName("pages"), w); err != nil {
		return nil, err
	}
	return w, nil
}

func addToWatcher(base string, w *fsnotify.Watcher) error {
	vendorBase, _ := filepath.Split(base)

	return filepath.Walk(base, func(p string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() {
			return nil
		}
		local := p[len(vendorBase):]
		return watchDir(p, local, w)
	})
}

func watchDir(full, local string, w *fsnotify.Watcher) error {
	if _, ok := watching[full]; ok {
		return nil
	}

	watching[full] = local
	return w.Add(full)
}

func stopWatchingDir(p string, w *fsnotify.Watcher) (err error) {

	// TODO: Need to walk the path to also remove subdirectories

	if _, ok := watching[p]; ok {
		delete(watching, p)
		return w.Remove(p)
	}
	return nil
}

var throttle = map[string]bool{}
var tMux sync.Mutex

func (g *Generator) handleWatchEvent(
	evt fsnotify.Event, w *fsnotify.Watcher,
) (err error, throttled bool) {
	tMux.Lock()

	if throttle[evt.Name] {
		tMux.Unlock()
		return nil, true
	}

	throttle[evt.Name] = true
	tMux.Unlock()

	go func(name string) {
		time.Sleep(50 * time.Millisecond)

		tMux.Lock()
		delete(throttle, name)
		tMux.Unlock()
	}(evt.Name)

	return g.handleFileEvent(evt, w), false
}

func (g *Generator) handleFileEvent(
	evt fsnotify.Event, w *fsnotify.Watcher,
) error {
	kind := getFileKind(evt.Name)
	if kind == 0 {
		return nil
	}

	dir, last := filepath.Split(evt.Name)
	dir = filepath.Clean(dir) // Gets rid of trailing separator.

	localDir, _ := watching[dir]

	local := filepath.Join(localDir, last)

	switch evt.Op {
	case fsnotify.Create:
		return handleFileCreate(evt.Name, local, kind, w, g)

	case fsnotify.Write:
		return handleFileUpdate(evt.Name, local, kind, w, g)

	case fsnotify.Rename:
		// A separate 'Create' event should come through for the new name
		fallthrough

	case fsnotify.Remove:
		return handleFileRemove(evt.Name, kind, w)

	case fsnotify.Chmod:
		// Nothing to handle here
	}
	return nil
}

func handleFileCreate(
	full, local string, kind fileKind, w *fsnotify.Watcher, g *Generator,
) (err error) {
	/*
		switch kind {
		case 0:
			panic("Internal error")
		case _dirFile:
			return watchDir(full, local, w)

		default:
			ok := false
			localDir, fileName := filepath.Split(local)
			fileNameNoExt := fileName[0 : len(fileName)-len(filepath.Ext(fileName))]
			var parComp *Component

			isFrag := strings.HasPrefix(localDir, "frags"+string(os.PathSeparator))
			if isFrag {
				if _, ok = g.frags[localDir]; ok {
					return nil // A frag must have already been processed
				}
				parComp = g.frags[filepath.Base(localDir)]
			}

			isPage := !isFrag &&
				strings.HasPrefix(localDir, "frags"+string(os.PathSeparator))
			if isPage {
				for _, p := range g.pages {
					if p.LocalPath() == localDir {
						return nil // A page must have already been processed
					}
				}

				parDir := filepath.Base(localDir)
				for _, p := range g.pages {
					if p.LocalPath() == parDir {
						parComp = p
					}
				}
			}

			if parComp != nil {
				c := newComponent(g, isPage, false, localDir, fileNameNoExt, parComp)
				if err = c.initializeFromPath(); err != nil {
					return err
				}
			}
			return handleFileUpdate(full, local, kind, w, g)
		}
	*/
	return nil
}

// getCompnentForPath assumes `fPath` will be a local path pointing to a file
// directly inside a component's directory
func getComponentForFile(g *Generator, local string) *Component {
	localDir := filepath.Dir(local)

	// frag
	if strings.HasPrefix(localDir, "frags") {
		return g.frags[localDir]
	}

	// page
	if strings.HasPrefix(localDir, "pages") {
		for _, c := range g.pages {
			if c.LocalPath() == localDir {
				return c
			}
		}
		return nil
	}

	return nil
}

func handleFileUpdate(
	full, local string, kind fileKind, w *fsnotify.Watcher, g *Generator,
) (err error) {
	switch kind {
	case 0, _dirFile:
		panic("Internal error")

	default:
		if strings.HasPrefix(local, "prelude"+string(os.PathSeparator)) {
			if err = g.processPreludeCSSAndJS(); err != nil {
				return err
			}
			return g.buildPageResources()
		}

		c := getComponentForFile(g, local)
		if c == nil {
			return nil
		}

		if kind == _htmlFile {

			// Not sure why this pause is needed.
			time.Sleep(50 * time.Millisecond)

			data, err := ioutil.ReadFile(full)
			if err != nil {
				return err
			}

			// TODO: Right now we're ignoring changes except those to the data-frost
			//   CSS and JS.
			//   Changes to HTML that do not change any Markers should be handled too.

			_, css, js := htmlparser.Parse(
				string(data),
				c.g.hasState(compressSpaceInHTML),
				c.localPathWithName(c.Name),
			)

			c.processCSS(css)
			c.processJS(js)

			if err = g.buildPageResources(); err != nil {
				return err
			}

			tempServGenPath := g.frostTempServerPath(
				filepath.Join("server", "res", "gen"),
			)
			if err = os.RemoveAll(tempServGenPath); err != nil {
				return err
			}
			if err = os.Mkdir(tempServGenPath, util.DirPerms); err != nil {
				return err
			}
			return util.DeepCopy(g.generatedPath(), tempServGenPath, false)
		}

		return nil
	}
}

func handleFileRemove(full string, kind fileKind, w *fsnotify.Watcher) error {
	switch kind {
	case 0:
		panic("Internal error")
	case _dirFile:
		return stopWatchingDir(full, w)
	default:
		// TODO: Remove content held in memory
		return nil
	}
}
