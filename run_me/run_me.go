package main

/*
This file needs to be run before building the main application. It imports
files used to build...
	...the eventual front end server
	...the eventual back end server (for each target language)
	...the JS libraries

It also builds the linked libraries for...
	...the CSS parser
*/

import (
	"Frost/utilities"
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"sync"
	"time"
)

func main() {
	var workingDir, err = os.Getwd()
	if err != nil {
		panic(err)
	}

	var name = "frontend_created_by_run_me_to_hold_files"
	var leadingDir = filepath.Join(workingDir, "generate", "targets", name)
	var outFile = name + ".go"

	file, err := os.Create(filepath.Join(leadingDir, outFile))
	if err != nil {
		panic(err)
	}
	defer file.Close()

	fmt.Fprintf(file, "package %s\n\n", name)

	var wg sync.WaitGroup

	wg.Add(2)

	go buildCSSParser(workingDir, &wg)
	go importJSLibraries(workingDir, file, &wg)

	wg.Wait()

	installFrost(workingDir)

	fmt.Println("Done")
}

func printer(s string) {
	fmt.Printf("Generating %q\n", s)
}

func importFile(filePth, vname string, file *os.File) {
	var data, err = ioutil.ReadFile(filePth)
	if err != nil {
		panic(err)
	}
	_, err = fmt.Fprintf(file, "var %s = %#v\n\n", vname, data)
	if err != nil {
		panic(err)
	}
}

// Copy the JS library into the Frost application
func importJSLibraries(
	workingDir string, file *os.File, wg *sync.WaitGroup,
) {
	defer wg.Done()
	fmt.Println("Generating JS library files")

	leadingDir := filepath.Join(workingDir, "generate", "js_library")
	leadingFilesDir := filepath.Join(leadingDir, "files")

	content, err := ioutil.ReadDir(leadingFilesDir)
	if err != nil {
		panic(err)
	}

	for _, info := range content {
		name := info.Name()

		if info.IsDir() == false && strings.HasSuffix(name, ".js") {
			importFile(
				filepath.Join(leadingFilesDir, name),
				"JS_"+strings.Title(name[0:len(name)-3]),
				file)
		}
	}

	// Get the selector-engine-legacy file
	importFile(
		filepath.Join(
			leadingFilesDir, "node_modules", "selector-engine", "lib",
			"selector-engine-no-wrap.js",
		),
		"JS_Selector_engine",
		file,
	)

	// Get loader script for JavaScript libraries.
	importFile(
		filepath.Join(leadingFilesDir, "build", "loader.js"),
		"JSLoader",
		file,
	)

	// Get the output wrapper used by closure compiler
	importFile(
		filepath.Join(leadingFilesDir, "build", "wrapper.txt"),
		"JS_Wrapper",
		file,
	)
}

// Build the linked CSS parser
func buildCSSParser(workingDir string, wg *sync.WaitGroup) {
	defer wg.Done()

	targetBuildDir := filepath.Join(workingDir, "generate", "cssparser", "lib")
	cssParserPath := filepath.Join(
		"/", "home", "mainuser", "Documents", "projects", "pony", "css_fix",
	)

	{
		exists, empty := utilities.DirExistsDirEmpty(targetBuildDir)

		if !exists || empty {
			if !exists {
				if err := os.MkdirAll(targetBuildDir, os.ModeDir); err != nil {
					panic(err)
				}
			}
			goto BUILD
		}
	}

	{
		// Compare the CSS parser's last build time...
		cssParserBuiltPath := filepath.Join(targetBuildDir, "css_fix.h")
		cssParserBuiltStat, err := os.Stat(cssParserBuiltPath)
		if err != nil {
			goto BUILD
		}

		// ...to the last pony commit
		ponyCommit := getLastCommitTime(os.ExpandEnv("$PONYHOME"))
		if ponyCommit.IsZero() {
			goto BUILD
		}
		if cssParserBuiltStat.ModTime().Before(ponyCommit) {
			goto BUILD
		}

		// ...to the parser' last commit
		parserCommit := getLastCommitTime(cssParserPath)
		if parserCommit.IsZero() {
			goto BUILD
		} else if cssParserBuiltStat.ModTime().Before(parserCommit) {
			goto BUILD
		}
	}

	fmt.Println("CSS processor already built")
	return

BUILD:
	fmt.Println("Building CSS processor")

	err := utilities.Empty(targetBuildDir)
	if err != nil {
		panic("Error emptying css lib build dir:" + err.Error())
	}

	var bld = exec.Command(
		"ponyc",
		"--library",
		//"--debug",
		"--output", targetBuildDir,
		cssParserPath,
	)

	bld.Stderr = os.Stderr

	if err = bld.Run(); err != nil {
		fmt.Println(err)
	}
}

func getLastCommitTime(pth string) time.Time {
	out, err := exec.Command(
		"git",
		"-C", pth,
		"log",
		"-1",
		"--format=%cd",
	).Output()
	if err != nil {
		fmt.Printf("Error getting log from git for %q: %s\n", pth, err)
		return time.Time{}
	}

	t, err := time.Parse(
		"Mon Jan 2 15:04:05 2006 -0700", string(bytes.TrimSpace(out)),
	)
	if err != nil {
		fmt.Printf("Error parsing date from git for %q: %s\n", pth, err)
		return time.Time{}
	}
	return t
}

// Check to see if a directory holds at least one `.go` file
func atLeastOneGoFile(pth string) bool {
	f, err := os.Open(pth)
	if err != nil {
		return false
	}
	defer f.Close()

	if n, err := f.Readdirnames(0); err != nil {
		fmt.Println(err)

	} else {
		for _, name := range n {
			if strings.HasSuffix(name, ".go") {
				return true
			}
		}
	}
	return false
}

// After everything else has been built and imported so install Frost.
func installFrost(workingDir string) {
	fmt.Println("Installing Frost")

	f, err := os.Create(filepath.Join(workingDir, "version.go"))
	if err != nil {
		panic(err)
	}
	defer f.Close()
	_, err = f.WriteString(fmt.Sprintf(`
package main

const version = %q

`, time.Now().Format(time.UnixDate)))

	if err != nil {
		panic(err)
	}

	var bld = exec.Command("go", "install")
	bld.Stderr = os.Stdout
	if err := bld.Run(); err != nil {
		fmt.Printf("Error running 'go install': %s\n", err)
	}
}
