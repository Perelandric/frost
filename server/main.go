package frost

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/Perelandric/frost/server/types"
)

const (
	ReadTimeout  = 10 * time.Second
	WriteTimeout = 10 * time.Second
)

type CompRequest struct {
	types.BasicCompRequest

	handler     func(*Response) Component
	siteMapNode *SiteMapNode
}

type server struct {
	routes           map[string]routeFunc
	dataPath         string
	port             int
	compressionLevel int
	isStarted        bool
}

type routeFunc func(http.ResponseWriter, *http.Request, string)

var mainServer = server{
	routes:   make(map[string]routeFunc),
	dataPath: "",
}

var resourceTags []struct {
	CSS, Js types.CSSJsTagData
}

func SetResourceTags____(tags []struct {
	CSS, Js types.CSSJsTagData
}) {
	resourceTags = tags
}

// Gets populated by the generated main file
var compRequests = map[string]*CompRequest{}
var rootCR *CompRequest

func Start() error {
	return mainServer.start()
}

func (s *server) start() error {
	if s.isStarted {
		return nil
	}
	s.isStarted = true

	rootCR = compRequests["/"]

	rootPage.setup()

	const defaultPort = 80
	const defaultCompression = -2

	var err error

	flag.IntVar(
		&s.compressionLevel,
		"compression",
		defaultCompression,
		"Compression level for served pages.",
	)
	flag.IntVar(
		&s.port,
		"port",
		defaultPort,
		"Port for front end server.",
	)
	flag.StringVar(
		&s.dataPath,
		"datapath",
		".",
		"Path to application data.",
	)

	flag.Parse()

	const invalid = "%d is an invalid %s. Using %d instead.\n"

	if s.port <= 0 {
		fmt.Printf(invalid, s.port, "port", defaultPort)
		s.port = defaultPort
	}

	if s.compressionLevel < -2 {
		fmt.Printf(invalid, s.compressionLevel, "compression", -2)
		s.compressionLevel = -2

	} else if s.compressionLevel > 9 {
		fmt.Printf(invalid, s.compressionLevel, "compression", 9)
		s.compressionLevel = 9
	}

	s.dataPath, err = filepath.Abs(s.dataPath)
	if err != nil {
		fmt.Println(err)
		flag.PrintDefaults()
		return err
	}

	fmt.Println("Working directory:", s.dataPath)

	// Add routes to mandatory root-based resources
	s.routes["/sitemap.xml"] =
		staticFile(filepath.Join(s.dataPath, "sitemap.xml")).serve
	s.routes["/favicon.ico"] =
		staticFile(filepath.Join(s.dataPath, "favicon.ico")).serve
	s.routes["/robots.txt"] =
		staticFile(filepath.Join(s.dataPath, "robots.txt")).serve

	addr := ":" + strconv.Itoa(s.port)

	if err != nil {
		log.Fatal(err)
		return err
	}

	fmt.Printf("\nStarting server on %s\n", addr)

	return http.ListenAndServe(addr, s)
}

// SetupPageHandler____ is invoked on initialization of each component that is
// a page
func SetupPageHandler____(
	handler func(*Response) Component,
	compReqData types.BasicCompRequest,
) {
	c := CompRequest{
		BasicCompRequest: compReqData,
		handler:          handler,
	}

	compRequests[c.ServerPath] = &c
	compRequests[toggleSlash(c.ServerPath)] = &c

	// Even if tails are allowed, we set the routes map as an optimization.
	mainServer.routes[c.ServerPath] = c.serve
	mainServer.routes[toggleSlash(c.ServerPath)] = c.serve
}

/*
toggleSlash puts a trailing slash on the path if there is none and removes it
if there is. It is meant to be used only on "directory" paths as it will give
no regard to the presence of a file extension.
*/
func toggleSlash(pth string) string {
	if len(pth) != 0 && pth[len(pth)-1] == '/' {
		return pth[0 : len(pth)-1]
	}
	return pth + "/"
}

func (s *server) ServeHTTP(r http.ResponseWriter, req *http.Request) {
	fullPth := req.URL.Path

	/*
		TODO: I think I should have a separate server for resources
	*/
	// Check for static resource request
	if len(fullPth) >= 5 &&
		fullPth[1] == 'r' &&
		fullPth[2] == 'e' &&
		fullPth[3] == 's' &&
		fullPth[4] == '/' {
		s.serveFile(r, req)
		return
	}

	if len(fullPth) == 0 {
		fullPth = "/"
	}

	if rfunc, ok := s.routes[fullPth]; ok {
		rfunc(r, req, "")
		return
	}

	// Check if the root or any of its sub-routes allow a tail that would
	// cause the URL to not be found in routes

	cr := rootCR
	temp, idx := 0, 1

	for {
		if cr.AllowTails {
			cr.serve(r, req, fullPth[idx:])
			return
		}

		if !cr.SubRoutesAllowTails {
			http.NotFound(r, req)
			return
		}

		temp = idx
		if idx = strings.IndexByte(fullPth[idx:], '/'); idx == -1 {
			http.NotFound(r, req)
			return
		}

		idx = temp + idx + 1

		if cr = compRequests[fullPth[0:idx]]; cr == nil {
			http.NotFound(r, req)
			return
		}
	}
}
