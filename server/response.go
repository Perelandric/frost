package frost

import (
	util "Frost/utilities"
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
)

const (
	sent = 1 << iota // Used to prevent writing after redirect, 503, etc.
	acceptsGzip
	sentCSS
	sentJS
)

// Response represents the HTTP response being built for the current request.
type Response struct {
	gzip gzip.Writer

	/*
		Unable to write directly to the ResponseWriter (or via gzip) because
		that cause WriteHeader to take place with StatusOK, which means we can
		no longer redirect.
		So instead we must write to a bytes.Buffer.
	*/
	buf bytes.Buffer

	writer io.Writer

	htmlArray   [][]byte
	siteMapNode *SiteMapNode // for the requested page

	cookiesToSend []string

	responseClear
}

// These are separated out simply to quickly clear them
type responseClear struct {
	request   *http.Request
	response  http.ResponseWriter
	tail      string // The "tail" part of a "tail route"
	rootIndex uint16
	idx       uint16
	state     uint16
}

func newResponse() *Response {
	gz, _ := gzip.NewWriterLevel(nil, mainServer.compressionLevel)

	return &Response{
		gzip: *gz,
		buf:  *bytes.NewBuffer(make([]byte, 0, _bufMaxSize)),
	}
}

/******************

	Various methods

******************/

// Tail returns the trailing part of the URL for routes that set the
// `AllowTails` option.
func (r *Response) Tail() string {
	return r.tail
}

// PageNode returns the `SiteMapNode` for the requested route.
func (r *Response) PageNode() *SiteMapNode {
	return r.siteMapNode
}

// Send503 sends a `StatusServiceUnavailable` response.
func (r *Response) Send503(err error) {
	log.Println(err)

	if !wasSent(r) {
		r.state |= sent
		r.response.WriteHeader(http.StatusServiceUnavailable)
	}
}

// Redirect sends a redirect with the given response `code` to the given `url`.
func (r *Response) Redirect(code int, url string) {
	r.doRedirect(code, url)
}

// TemporaryRedirect sends a `StatusTemporaryRedirect` (307) response.
func (r *Response) TemporaryRedirect(url string) {
	r.doRedirect(http.StatusTemporaryRedirect, url)
}

// RedirectToGet sends a `StatusSeeOther` (303) response to redirect a POST to
// a GET request.
func (r *Response) RedirectToGet(url string) {
	r.doRedirect(http.StatusSeeOther, url)
}

// PermanentRedirect sends a `StatusMovedPermanently` (301) response.
func (r *Response) PermanentRedirect(url string) {
	r.doRedirect(http.StatusMovedPermanently, url)
}

func (r *Response) doRedirect(code int, url string) {
	if !wasSent(r) {
		r.state |= sent

		//		r.sendCookies()

		http.Redirect(r.response, r.request, url, code)
	}
}

// URL returns the requested `URL` for the current route.
func (r *Response) URL() *url.URL {
	return r.request.URL
}

// ParseForm parses any form data sent with the request.
func (r *Response) ParseForm() error {
	return r.request.ParseForm()
}

// Form returns the post form for the current request.
func (r *Response) Form() url.Values {
	return r.request.PostForm
}

// FormValue returns the value of the given key from the post form for the
// current request.
func (r *Response) FormValue(key string) string {
	return r.request.PostFormValue(key)
}

// Method returns the HTTP method of the current request.
func (r *Response) Method() string {
	return r.request.Method
}

// SetCookie sets the given cookie to be sent with the response.
func (r *Response) SetCookie(c *http.Cookie) {
	http.SetCookie(r.response, c)
}

// GetCookie gets the cookie from the current request.
func (r *Response) GetCookie(name string) *http.Cookie {
	c, err := r.request.Cookie(name)
	if err != nil && err != http.ErrNoCookie {
		log.Printf("GetCookie error: %q\n", err)
	}

	return c // Return the Cookie or nil
}

// ExpireCookie expires the given cookie.
func (r *Response) ExpireCookie(c *http.Cookie) {
	cc := *c
	cc.MaxAge = -1
	http.SetCookie(r.response, &cc)
}

/********************

	Content insertion

********************/

func wasSent(r *Response) bool {
	return r.state&sent == sent
}

// setMarkerIndices moves progress ahead to the given marker
func setMarkerIndices(r *Response, target uint16) bool {
	if wasSent(r) {
		return false
	}

	curr := r.idx
	if DEBUG && curr > target {
		r.Send503(fmt.Errorf("attempt to access previous marker"))
		return false
	}

	if curr != target { // If they're equal, there's no content to send.
		for curr < target { // Move ahead to the target
			r.writer.Write(r.htmlArray[curr])
			curr++
		}
		r.idx = target
	}

	return true
}

var (
	bytDblQuot = []byte("&#34;")
	bytSglQuot = []byte("&#39;")
	bytAmp     = []byte("&amp;")
	bytLssThan = []byte("&lt;")
	bytGtrThan = []byte("&gt;")
)

// addEscapeHTMLString adds `s` to the Response but escapes characters that
// could allow an XSS attack.
func addEscapeHTMLString(r *Response, s string) {
	var last = 0
	var escaped []byte

	for i, c := range s {
		switch c {
		case '"':
			escaped = bytDblQuot
		case '\'':
			escaped = bytSglQuot
		case '&':
			escaped = bytAmp
		case '<':
			escaped = bytLssThan
		case '>':
			escaped = bytGtrThan
		default:
			continue
		}

		r.writer.Write(util.StrToBytes(s[last:i]))

		r.writer.Write(escaped)
		last = i + 1
	}

	if last != len(s) {
		r.writer.Write(util.StrToBytes(s[last:]))
	}
}

/********************

	CSS/JS injection

********************/

// AddCSS inserts the CSS link, which links to the generated CSS for the current
// page.
func (r *Response) AddCSS(m Marker) {
	if setMarkerIndices(r, m.marker[0]) {
		d := resourceTags[r.rootIndex].CSS
		http2Push(r, d.Path, sentCSS)
		r.writer.Write(d.Tags)
	}
}

// AddCSSToEnd is the same as `AddCSS`, but inserts it at the end position of
// the marker.
func (r *Response) AddCSSToEnd(m Marker) {
	if setMarkerIndices(r, m.marker[1]) {
		d := resourceTags[r.rootIndex].CSS
		http2Push(r, d.Path, sentCSS)
		r.writer.Write(d.Tags)
	}
}

// AddJS inserts the CSS link, which links to the generated CSS for the current
// page.
func (r *Response) AddJS(m Marker) {
	if setMarkerIndices(r, m.marker[0]) {
		d := resourceTags[r.rootIndex].Js
		http2Push(r, d.Path, sentJS)
		r.writer.Write(d.Tags)
	}
}

// AddJSToEnd is the same as `AddCSS`, but inserts it at the end position of the
// marker.
func (r *Response) AddJSToEnd(m Marker) {
	if setMarkerIndices(r, m.marker[1]) {
		d := resourceTags[r.rootIndex].Js
		http2Push(r, d.Path, sentJS)
		r.writer.Write(d.Tags)
	}
}

// AddCSSJS combines the functionality of `AddCSS` and `AddJS`
func (r *Response) AddCSSJS(m Marker) {
	if setMarkerIndices(r, m.marker[0]) {
		d := resourceTags[r.rootIndex]
		http2Push(r, d.CSS.Path, sentCSS)
		http2Push(r, d.Js.Path, sentJS)
		r.writer.Write(d.CSS.Tags)
		r.writer.Write(d.Js.Tags)
	}
}

// AddCSSJSToEnd is the same as `AddCSSJS`, but inserts it at the end position
// of the marker.
func (r *Response) AddCSSJSToEnd(m Marker) {
	if setMarkerIndices(r, m.marker[1]) {
		d := resourceTags[r.rootIndex]
		http2Push(r, d.CSS.Path, sentCSS)
		http2Push(r, d.Js.Path, sentJS)
		r.writer.Write(d.CSS.Tags)
		r.writer.Write(d.Js.Tags)
	}
}

func http2Push(r *Response, url string, flag uint16) {
	if r.request.ProtoMajor != 2 || r.state&flag != 0 {
		return
	}
	r.state |= flag
	_ = r.response.(http.Pusher).Push(url, nil)
}

/***************************

	General content insertion

***************************/

// Add adds `value` to the start position of the given marker.
func (r *Response) Add(m Marker, value string) {
	if setMarkerIndices(r, m.marker[0]) {
		addEscapeHTMLString(r, value)
	}
}

// AddToEnd adds `value` to the end position of the given marker.
func (r *Response) AddToEnd(m Marker, value string) {
	if setMarkerIndices(r, m.marker[1]) {
		addEscapeHTMLString(r, value)
	}
}

// AddNoEscape adds `value` to the start position of the given marker, without
// escaping any provided HTML. This opens the possibility to XSS attacks, so
// use with caution.
func (r *Response) AddNoEscape(m Marker, value string) {
	if setMarkerIndices(r, m.marker[0]) {
		r.writer.Write(util.StrToBytes(value))
	}
}

// AddToEndNoEscape is the same as `AddNoEscape`, but inserts `value` at the end
// position of the given marker. The same "use with caution" warning applies.
func (r *Response) AddToEndNoEscape(m Marker, value string) {
	if setMarkerIndices(r, m.marker[1]) {
		r.writer.Write(util.StrToBytes(value))
	}
}

// Skip will, when called from the start position of a marker that has content,
// pass over the content (both static and dynamic) of that marker to its end
// position.
func (r *Response) Skip(m Marker) {
	_ = setMarkerIndices(r, m.marker[0])
	r.idx = m.marker[1]
}

/*********************

	Component insertion

*********************/

// AddComponent is the same as `Add`, but receives a `Component` instead of a
// `string`.
func (r *Response) AddComponent(m Marker, value Component) {
	doComponent(r, m.marker[0], value)
}

// AddComponentToEnd is the same as `Add`, but receives a `Component` instead
// of a `string`.
func (r *Response) AddComponentToEnd(m Marker, value Component) {
	doComponent(r, m.marker[1], value)
}

func doComponent(r *Response, target uint16, value Component) {
	if setMarkerIndices(r, target) {
		orig := r.htmlArray
		r.idx, r.htmlArray = 0, value.Initialize____()

		value.Make(r)

		if !wasSent(r) { // Write any remaining content
			for i, arr := r.idx, r.htmlArray; i < uint16(len(arr)); i++ {
				r.writer.Write(arr[i])
			}
		}

		r.htmlArray = orig
	}

	r.idx = target
}

/****************************************

	Methods to support Partial Application

****************************************/

type ApplyData interface {
	getStartEnd() (string, string)
}

type Apply struct {
	Start, End string
}

func (a Apply) getStartEnd() (string, string) {
	return a.Start, a.End
}

type ApplyComponent struct {
	Start, End Component
}

func (a ApplyComponent) getStartEnd() (string, string) {
	start, end := "", ""

	if a.Start != nil {
		startR := getResponse(&respRenderer{
			b: bytes.Buffer{},
		}, nil, rootPage, "", 0, false)
		defer putResponse(startR, false)

		start = startR.RenderComponent(a.Start)
	}

	if a.End != nil {
		endR := getResponse(&respRenderer{
			b: bytes.Buffer{},
		}, nil, rootPage, "", 0, false)
		defer putResponse(endR, false)

		end = endR.RenderComponent(a.End)
	}

	return start, end
}

type ApplyData____ struct {
	V ApplyData // Value to apply
	M [2]uint16 // Marker indices
}

func ApplyPartial____(origHtmlArray [][]byte, data ...ApplyData____) [][]byte {
	newArr := make([][]byte, 0, len(data)+1)

	// The pointer is to distinguish between an empty string and no marker, since
	// a marker position will have its adjacent HTML joined either way.
	idxToData := make([]*string, len(origHtmlArray))

	for _, d := range data {
		if d.V == nil {
			continue
		}

		start, end := d.V.getStartEnd()

		idxToData[d.M[0]] = &start

		if d.M[0] == d.M[1] {
			*idxToData[d.M[0]] += end
		} else {
			idxToData[d.M[1]] = &end
		}
	}

	i := 0

	for j, s := range idxToData {
		for ; i < j; i++ {
			newArr = append(newArr, origHtmlArray[i])
		}
		if s == nil {
			continue
		}

		// Add the partial data and the next chunk of HTML to the previous chunk...
		newArr[len(newArr)-1] = append(append(
			newArr[len(newArr)-1], *s...),
			origHtmlArray[i]...,
		)
		i++ // ...and move past the next chunk.
	}

	// Append any trailing data.
	for ; i < len(origHtmlArray); i++ {
		newArr = append(newArr, origHtmlArray[i])
	}

	return newArr
}

// CompleteAppliedFuncForRoot____ is for internal use on ApplyGroups only.
func (r *Response) CompleteAppliedFuncForRoot____(
	fn func(int) Component,
	htmlArrays [][][]byte,
	eachLen uint16,
) {
	r.CompleteAppliedFunc____(fn, htmlArrays[r.rootIndex], eachLen)
}

// CompleteAppliedFuncForRoot____ is for internal use on ApplyGroups only.
func (r *Response) CompleteAppliedFuncForRootWith____(
	fn func(int) (Component, Component),
	htmlArrays [][][]byte,
	eachLen uint16,
) {
	r.CompleteAppliedFuncWith____(fn, htmlArrays[r.rootIndex], eachLen)
}

// CompleteAppliedFunc____ is for internal use on ApplyGroups only.
func (r *Response) CompleteAppliedFunc____(
	fn func(int) Component,
	htmlArrays [][]byte,
	eachLen uint16,
) {

	start, end := uint16(0), uint16(0)
	i := 0
	maxComps := 0

	if eachLen != 0 {
		maxComps = len(htmlArrays) / int(eachLen)
	}

	var c Component

	for i < maxComps {
		c = fn(i)

		// Advance to the start and end of the next component HTML
		start, end, i = end, end+eachLen, i+1

		if c == nil { // `nil` means to skip this entry entirely
			continue
		}

		r.idx, r.htmlArray = 0, htmlArrays[start:end]
		c.Make(r)

		// Write the remaining HTML for the component.
		for start += r.idx; start < end; start++ {
			r.writer.Write(htmlArrays[start])
		}

		if wasSent(r) {
			break
		}
	}

	r.htmlArray = nil // Set nil because r.idx may be behind.
}

// CompleteAppliedFuncWith____ is for internal use on ApplyGroups only.
func (r *Response) CompleteAppliedFuncWith____(
	fn func(int) (Component, Component),
	htmlArrays [][]byte,
	eachLen uint16,
) {

	start, end := uint16(0), uint16(0)
	i, maxComps, doJoin := 0, len(htmlArrays)/int(eachLen), false

	var c, joiner Component

	for i < maxComps {
		c, joiner = fn(i)

		// Advance to the start and end of the next component HTML
		start, end, i = end, end+eachLen, i+1

		if c == nil { // `nil` means to skip this entry entirely
			continue
		}

		if joiner != nil {
			if doJoin { // do the joiner
				joinerHTML := joiner.Initialize____()
				r.idx, r.htmlArray = 0, joinerHTML

				joiner.Make(r)

				if wasSent(r) {
					return
				}

				for curr := r.idx; curr < uint16(len(joinerHTML)); curr++ {
					r.writer.Write(joinerHTML[curr])
				}

			} else {
				doJoin = true
			}
		}

		r.idx, r.htmlArray = 0, htmlArrays[start:end]
		c.Make(r)

		// Write the remaining HTML for the component.
		for arr := r.htmlArray; r.idx < uint16(len(arr)); r.idx++ {
			r.writer.Write(arr[r.idx])
		}

		if wasSent(r) {
			break
		}
	}

	r.htmlArray = nil // Set nil, just in case.
}

/**********************

	Component rendering

**********************/

// A bare-bones ResponseWriter used for a faux req/resp cycle.
type respRenderer struct {
	b bytes.Buffer
	h http.Header
}

// Implement the ResponseWriter interface
func (rr *respRenderer) Write(b []byte) (int, error) {
	return rr.b.Write(b)
}
func (rr *respRenderer) Header() http.Header {
	if rr.h == nil {
		rr.h = http.Header(map[string][]string{})
	}
	return rr.h
}
func (*respRenderer) WriteHeader(int) {
}

// Implement the http.Pusher interface
func (*respRenderer) Push(string, *http.PushOptions) error {
	return nil
}

/*
	RenderComponent renders the given Component to a `string` of its HTML output.
	This action will not write to the http response, and will ignore any
	redirects, writing of headers, etc..
*/
func (r *Response) RenderComponent(c Component) string {
	return doRender(r, c).String()
}

/*
RenderComponentToBytes is the same as RenderComponent except that the
result is a `[]byte` instead of a `string`.
*/
func (r *Response) RenderComponentToBytes(c Component) []byte {
	return doRender(r, c).Bytes()
}

func doRender(r *Response, c Component) *bytes.Buffer {
	origResp := r.response
	origState := r.state

	tempResp := respRenderer{
		b: bytes.Buffer{},
	}
	r.response = &tempResp
	r.state = 0 // No gzipping, so start with a clean state

	// The `r.idx` ensures the index doesn't move before the `.Make()`
	doComponent(r, r.idx, c)

	r.response = origResp
	r.state = origState

	return &tempResp.b
}

