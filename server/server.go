package frost

import (
	"bytes"
	"compress/gzip"
	"crypto/sha1"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"
)

//const long_cache_control = "max-age=31536000, s-maxage=31536000"

const (
	_acceptEncoding  = "Accept-Encoding"
	_contentEncoding = "Content-Encoding"
	_contentType     = "Content-Type"
	_gzip            = "gzip"
	_etag            = "Etag"
	_msie6           = "MSIE 6"
	_userAgent       = "User-Agent"

	_htmlContent = "text/html; charset=utf-8"

	deadline    = 10 * time.Second
	respBufSize = 2048
)

func logErr(err error) bool {
	if err != nil && err != io.EOF {
		log.Println(err.Error())
		return true
	}
	return false
}

func loggingFileRemover(pth string) {
	if err := os.Remove(pth); err != nil {
		const msg = "Error removing file at: %q\n    ERROR: %s"
		log.Printf(msg, pth, err.Error())
	}
}
func loggingCloser(c io.Closer, pth string) {
	if err := c.Close(); err != nil {
		if pth == "" {
			const msg = "Error closing item\n    ERROR: %s"
			log.Printf(msg, err.Error())
		} else {
			const msg = "Error closing file at: %q\n    ERROR: %s"
			log.Printf(msg, pth, err.Error())
		}
	}
}

var fileExt = map[string]struct {
	canGzip bool
	mime    []string
}{
	".css":  {true, []string{"text/css; charset=utf-8"}},
	".htm":  {true, []string{_htmlContent}},
	".html": {true, []string{_htmlContent}},
	".js":   {true, []string{"application/x-javascript"}},
	".json": {true, []string{"application/json"}},
	".xml":  {true, []string{"text/xml; charset=utf-8"}},
	".gif":  {false, []string{"image/gif"}},
	".jpg":  {false, []string{"image/jpeg"}},
	".pdf":  {false, []string{"application/pdf"}},
	".png":  {false, []string{"image/png"}},
}

const (
	_poolSize   = 10000
	_bufMaxSize = 50000
)

var respPool = make(chan *Response, _poolSize)

func init() { // Prime the pool to 10% with Response objects
	if _poolSize <= 0 {
		return
	}

	const prime = _poolSize / 10
	for i := 0; i < prime; i++ {
		defer putResponse(newResponse(), false)
	}
}

var (
	htmlContentHeader = fileExt[".html"].mime
	gzipHeader        = []string{_gzip}
)

func (cr *CompRequest) serve(
	resp http.ResponseWriter, req *http.Request, tail string,
) {
	respHdrs := resp.Header()
	respHdrs[_contentType] = htmlContentHeader

	doGzip := strings.Contains(req.Header.Get(_acceptEncoding), _gzip) &&
		!strings.Contains(req.Header.Get(_userAgent), _msie6)

	r := getResponse(resp, req, cr.siteMapNode, tail, cr.RootIndex, doGzip)

	if doGzip {
		respHdrs[_contentEncoding] = gzipHeader
	}

	defer putResponse(r, true)

	c := cr.handler(r) // Call the handleRequest(r) for the route

	r.htmlArray = c.Initialize____() // Get the component's htmlArray

	c.Make(r) // Call its Make(r) method
}

func getResponse(
	resp http.ResponseWriter,
	req *http.Request,
	node *SiteMapNode,
	tail string,
	rootIdx uint16,
	doGzip bool,
) (r *Response) {

	select {
	case r = <-respPool:

	default:
		r = newResponse()
	}

	r.buf.Reset()
	r.gzip.Reset(&r.buf)

	// Set up gzipping, if needed
	if doGzip {
		r.state |= acceptsGzip
		r.writer = &r.gzip

	} else {
		r.writer = &r.buf
	}

	r.response = resp
	r.request = req
	r.rootIndex = rootIdx
	r.siteMapNode = node
	r.tail = tail

	return
}

// putResponse puts the *Response object back in the pool. If `completeReponse`
// is `true`, it first writes any trailing HTML that has not yet been written.
func putResponse(r *Response, completeResponse bool) {
	if completeResponse && !wasSent(r) {
		_ = setMarkerIndices(r, uint16(len(r.htmlArray))) // Finalize

		if r.state&acceptsGzip != 0 {
			r.gzip.Close()
		}

		r.response.Write(r.buf.Bytes())
	}

	// Clear data and put back into the pool.
	r.cookiesToSend = r.cookiesToSend[0:0]
	r.responseClear = responseClear{}

	select {
	case respPool <- r: // Successfully placed back into pool
		b := r.buf.Bytes()
		if cap(b) > _bufMaxSize {
			r.buf = *bytes.NewBuffer(b[0:0:_bufMaxSize])
		}

	default: // let overflow get GC'd
	}
}

type staticFile string

func (sf staticFile) serve(r http.ResponseWriter, req *http.Request, _ string) {
	http.ServeFile(r, req, string(sf))
}

/**
Serves a static file from the filesystem for the given path.
If the content type can be zipped, it first looks for a pre-zipped version
of the file. If none is found, it attempts to zip and save the file.
*/
func (s *server) serveFile(resp http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case "GET", "POST": // Do nothing
	default:
		http.NotFound(resp, req)
		return
	}

	if etg := req.Header.Get(_etag); etg != "" {
		if _, ok := etagMap.Load(etg); ok {
			resp.WriteHeader(http.StatusNotModified)
			return
		}
	}

	urlpath := filepath.Join(s.dataPath, filepath.FromSlash(req.URL.Path))
	urlpathgz := ""
	err := error(nil)
	doGzip := false
	respHdrs := resp.Header()
	file := (*os.File)(nil)
	buf := []byte(nil)

	if info, ok := fileExt[filepath.Ext(urlpath)]; ok {
		respHdrs[_contentType] = info.mime

		doGzip = info.canGzip &&
			strings.Contains(req.Header.Get(_acceptEncoding), _gzip) &&
			!strings.Contains(req.Header.Get(_userAgent), _msie6)

		// First look for a gzip version of the file, if possible
		if doGzip {
			urlpathgz = urlpath + ".gz"

			if file, err = os.Open(urlpathgz); err == nil {
				respHdrs[_contentEncoding] = gzipHeader

				goto SEND_FILE
			}
		}
	}

	// No file yet, so try with the original path
	if file, err = os.Open(urlpath); err != nil {
		http.NotFound(resp, req) // No original, so 404.
		return
	}

	buf = bytesPool.Get().([]byte)
	defer bytesPool.Put(buf[0:0])

	if doGzip { // Gzip the file to a new file and close, reopen, and send it.
		var gzFile *os.File

		if gzFile, err = os.Create(urlpathgz); err == nil {
			var z *gzip.Writer

			z, err = gzip.NewWriterLevel(gzFile, gzip.BestCompression)
			if logErr(err) {
				doGzip = false
				loggingCloser(gzFile, urlpathgz)
				loggingFileRemover(urlpathgz)
				goto SEND_FILE // Original file is still open, so send that one
			}

			for tw := io.TeeReader(file, z); err == nil; {
				_, err = tw.Read(buf[0:bytesSizeInPool])
			}

			loggingCloser(z, "")             // Flush the gzipper...
			loggingCloser(gzFile, urlpathgz) // ...then close the new file...
			loggingCloser(file, urlpath)     // ...and close the original

			if !logErr(err) {
				doGzip = false
				if file, err = os.Open(urlpathgz); !logErr(err) { // Open gzip file
					respHdrs[_contentEncoding] = gzipHeader
					goto SEND_FILE
				}
			}

			// Zipping failed, so grab the original again
			if file, err = os.Open(urlpath); logErr(err) { // Unable to reopen file
				http.Error(
					resp, "Internal server error", http.StatusInternalServerError,
				)
				return
			}
		}
	}

SEND_FILE:
	if doGzip {
		defer loggingCloser(file, urlpathgz)
	} else {
		defer loggingCloser(file, urlpath)
	}

	stat, err := file.Stat()
	if logErr(err) {
		http.Error(resp, "Internal server error", http.StatusInternalServerError)
		return
	}

	if stat.IsDir() { // Make sure the file isn't a directory
		http.Error(resp, "Not authorized", http.StatusUnauthorized)
		return
	}
	mod := stat.ModTime()

	// First the path and | separator...
	buf = append(append(buf[0:0], urlpath...), '|')

	// ...then the mod time...
	for unx := mod.Unix(); unx != 0; unx /= 10 {
		buf = append(buf, byte(unx%10)|0x30)
	}

	buf = append(buf, '|')

	// ...then the size
	for siz := stat.Size(); siz != 0; siz /= 10 {
		buf = append(buf, byte(siz%10)|0x30)
	}

	sum := sha1.Sum(buf)

	/*
		for _, b := range sum {
			data = append(data, toHex(b>>4), toHex(b&0xF))
		}
	*/
	data := string(append(append(append(append(append(append(append(append(append(append(append(append(append(append(append(append(append(append(append(append(append(append(buf[0:0],
		'"'),
		toHex(sum[0]>>4), toHex(sum[0]&0xF)),
		toHex(sum[1]>>4), toHex(sum[1]&0xF)),
		toHex(sum[2]>>4), toHex(sum[2]&0xF)),
		toHex(sum[3]>>4), toHex(sum[3]&0xF)),
		toHex(sum[4]>>4), toHex(sum[4]&0xF)),
		toHex(sum[5]>>4), toHex(sum[5]&0xF)),
		toHex(sum[6]>>4), toHex(sum[6]&0xF)),
		toHex(sum[7]>>4), toHex(sum[7]&0xF)),
		toHex(sum[8]>>4), toHex(sum[8]&0xF)),
		toHex(sum[9]>>4), toHex(sum[9]&0xF)),
		toHex(sum[10]>>4), toHex(sum[10]&0xF)),
		toHex(sum[11]>>4), toHex(sum[11]&0xF)),
		toHex(sum[12]>>4), toHex(sum[12]&0xF)),
		toHex(sum[13]>>4), toHex(sum[13]&0xF)),
		toHex(sum[14]>>4), toHex(sum[14]&0xF)),
		toHex(sum[15]>>4), toHex(sum[15]&0xF)),
		toHex(sum[16]>>4), toHex(sum[16]&0xF)),
		toHex(sum[17]>>4), toHex(sum[17]&0xF)),
		toHex(sum[18]>>4), toHex(sum[18]&0xF)),
		toHex(sum[19]>>4), toHex(sum[19]&0xF)),
		'"'))

	etagMap.Store(data, "")
	resp.Header().Set(_etag, data)

	http.ServeContent(resp, req, urlpath, mod, file)
}

var etagMap sync.Map

const etagClearSchedule = 5 * time.Minute

// The etagMap gets cleared on an interval just in case some resource was
// updtaed. This just means that the tag will be regenerated in the map at
// its next request. If nothing changed, the tag will be the same.
func init() {
	_ = time.AfterFunc(etagClearSchedule, clearEtagMap)
}

func clearEtagMap() {
	etagMap.Range(func(k, _ interface{}) bool {
		etagMap.Delete(k)
		return true
	})

	_ = time.AfterFunc(etagClearSchedule, clearEtagMap)
}

const bytesSizeInPool = 512

var bytesPool = sync.Pool{
	New: func() interface{} {
		return make([]byte, 0, bytesSizeInPool)
	},
}

func toHex(b byte) byte {
	if b < 10 {
		return b | 0x30 // '0'-'9'
	}
	return (b - 10) + 0x41 // 'A'-'F'
}
