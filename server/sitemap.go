package frost

type SiteMapNode struct {
	children  []*SiteMapNode
	ancestors []*SiteMapNode

	path        string
	displayName string
	description string

	pathEsc        string
	displayNameEsc string
	descriptionEsc string
}

var rootPage *SiteMapNode

func RootPage() *SiteMapNode {
	return rootPage
}

func NewSiteMapNode____(
	isRoot bool, ch []*SiteMapNode, path, dispName, desc string,
) *SiteMapNode {
	if len(desc) == 0 {
		desc = dispName
	}

	var res = new(SiteMapNode)
	res.children = ch

	res.path = path
	res.displayName = dispName
	res.description = desc

	res.pathEsc = EscapeHTML(path)
	res.displayNameEsc = EscapeHTML(dispName)
	res.descriptionEsc = EscapeHTML(desc)

	if isRoot {
		rootPage = res
	}

	compRequests[path].siteMapNode = res

	return res
}

// Called before the server starts
func (self *SiteMapNode) setup() {
	for _, child := range self.children {
		child.ancestors = append(append([]*SiteMapNode{}, self.ancestors...), self)
		child.setup()
	}
}

func (self *SiteMapNode) Path() string {
	return self.path
}
func (self *SiteMapNode) Description() string {
	return self.description
}
func (self *SiteMapNode) DisplayName() string {
	return self.displayName
}
func (self *SiteMapNode) Parent() *SiteMapNode {
	if len(self.ancestors) > 0 {
		return self.ancestors[len(self.ancestors)-1]
	}
	return nil
}

func (self *SiteMapNode) EachChild(fn func(*SiteMapNode, int) bool) {
	for i, child := range self.children {
		if fn(child, i) == false {
			break
		}
	}
}
func (self *SiteMapNode) GetChild(idx int) *SiteMapNode {
	if idx < 0 {
		idx = len(self.children) + idx
	}
	if 0 <= idx && idx < len(self.children) {
		return self.children[idx]
	}
	return nil
}
func (self *SiteMapNode) LenChildren() int {
	return len(self.children)
}

func (self *SiteMapNode) EachAncestor(fn func(*SiteMapNode, int) bool) {
	for i, anc := range self.ancestors {
		if fn(anc, i) == false {
			break
		}
	}
}
func (self *SiteMapNode) GetAncestor(idx int) *SiteMapNode {
	if idx < 0 {
		idx = len(self.ancestors) + idx
	}
	if 0 <= idx && idx < len(self.ancestors) {
		return self.ancestors[idx]
	}
	return nil
}
func (self *SiteMapNode) LenAncestors() int {
	return len(self.ancestors)
}
