package types

import (
	"bytes"
	"fmt"
	"log"
	"strings"
	"unicode"
	"unicode/utf8"
)

//HTML Elements that shouldn't contain child nodes
var EmptyElements = map[string]bool{
	// HTML4
	"AREA":     true,
	"BASE":     true,
	"BASEFONT": true,
	"BR":       true,
	"COL":      true,
	// "FRAME": true,
	"HR":      true,
	"IMG":     true,
	"INPUT":   true,
	"ISINDEX": true,
	"LINK":    true,
	"META":    true,
	"PARAM":   true,
	"EMBED":   true,
	// HTML5
	"COMMAND": true,
	"KEYGEN":  true,
	"SOURCE":  true,
}

type NodeI interface {
	ToString(*bytes.Buffer, bool)
}

type ParentI interface {
	GetChildNodes() []NodeI
	AddChildren(...NodeI) []NodeI
}

// Acts as the separator between Attr name and value
type EqualSign struct {
}

func (EqualSign) ToString(buf *bytes.Buffer, doMarkers bool) {
	buf.WriteByte('=')
}

// Attributes are more complex than one may expect. Because we need to account
// for markers in the name and/or value, both the name and value are stored
// together as a slice of *Node, just like an Element. The attribute's name and
// value are separated by a special `EqualSign` node. This lets us use the same
// code for processing nested markers of arbitrary depth for all nodes.
type AttrNode struct {
	ParentNode
	Quote byte
}

// If a quote hasn't been set and if there are markers, use double quote.
// If a quote has been set but there are no markers and only unquotable chars,
// remove the quote.
func (an *AttrNode) SetOrElideQuote() {
	var foundEq bool

	for i, n := range an.ChildNodes {
		switch nn := n.(type) {
		case EqualSign, *EqualSign:
			foundEq = true

		case *Marker:
			if foundEq {
				if an.Quote == 0 { // A Marker with no quote set gets an automatic '"'
					an.Quote = '"'
				}
				return
			}

		case *TextNode:
			if foundEq {
				if i == len(an.ChildNodes)-1 { // If this is the only text...
					if CanElideAttrQuote(nn.Data) { // ...and quotes can be elided...
						an.Quote = 0 // ...elide the quote.
					}

				} else if an.Quote == 0 { // More nodes to come, so must be markers...
					an.Quote = '"' // ...so it gets an automatic '"'
				}
				return
			}
		}
	}
}

func CanElideAttrQuote(s string) bool {
	if len(s) == 0 {
		return false
	}

	for _, r := range s {
		if unicode.IsSpace(r) {
			return false
		}
		switch r {
		case '"', '\'', '`', '=', '<', '>':
			return false
		}
	}
	return true
}

func (an *AttrNode) ToString(buf *bytes.Buffer, doMarkers bool) {
	var foundEq bool

	for _, child := range an.ChildNodes {

		child.ToString(buf, doMarkers)

		switch child.(type) {
		case EqualSign, *EqualSign:
			foundEq = true

			if an.Quote != 0 {
				buf.WriteByte(an.Quote)
			}
		}
	}

	if foundEq && an.Quote != 0 {
		buf.WriteByte(an.Quote)
	}
}

func (an *AttrNode) trav(fn func(NodeI)) {

	var doTrav func([]NodeI)

	doTrav = func(nodes []NodeI) {
		for _, child := range nodes {

			fn(child)

			if m, ok := child.(*Marker); ok {
				doTrav(m.ChildNodes)
			}
		}
	}

	doTrav(an.ChildNodes)
}

func (an *AttrNode) getTextParts(
	forName bool,
) (res []string, foundEq, foundMarker bool) {

	var foundFirst bool
	var gather = forName // Start gathering immediately if for name.

	an.trav(func(node NodeI) {
		switch n := node.(type) {
		case EqualSign, *EqualSign:
			if foundEq {
				var buf bytes.Buffer
				an.ToString(&buf, true)

				panic(fmt.Sprintf("Invalid attribute. Multiple equal signs found. %q",
					buf.String()))
			}

			foundEq = true
			gather = !gather

		case *TextNode:
			if gather {
				res = append(res, n.Data)
				foundFirst = true
			}

		case *Marker:
			foundMarker = true
			if gather && !foundFirst { // If the first item is a marker, add an empty
				res = append(res, "") // string to show there was a marker there.
				foundFirst = true
			}
		default:
			panic(fmt.Sprintf("Internal error; Invalid type found in AttrNode: %#v\n",
				n))
		}
	})

	var ln = len(an.ChildNodes)

	if ln != 0 && (len(res) == 0 || res[len(res)-1] != "") {
		if _, ok := an.ChildNodes[ln-1].(*Marker); ok {
			res = append(res, "") // If last item is a marker, add empty string
		} // to show a marker was there (unless there already is one)
	}
	return
}

func (an *AttrNode) GetNameTextParts() ([]string, bool, bool) {
	return an.getTextParts(true)
}

func (an *AttrNode) GetValueTextParts() ([]string, bool, bool) {
	return an.getTextParts(false)
}

// Only needs to store top level nodes. It's like a DocumentFragment.
type GenericRoot struct {
	ParentNode
}

func (an *GenericRoot) ToString(buf *bytes.Buffer, doMarkers bool) {
	an.ParentNode.ToString(buf, doMarkers)
}

// For nodes that can have child nodes
type ParentNode struct {
	ChildNodes []NodeI
}

func (pn *ParentNode) GetChildNodes() []NodeI {
	return pn.ChildNodes
}
func (pn *ParentNode) AddChildren(ch ...NodeI) []NodeI {
	pn.ChildNodes = append(pn.ChildNodes, ch...)
	return ch
}
func (pn *ParentNode) ToString(buf *bytes.Buffer, doMarkers bool) {
	for _, child := range pn.ChildNodes {
		child.ToString(buf, doMarkers)
	}
}

// CommentNodes currently ignore markers within.
type CommentNode struct {
	DataNode
}

func (cn *CommentNode) ToString(buf *bytes.Buffer, doMarkers bool) {
	fmt.Fprintf(buf, "<!--%s-->", cn.Data)
}

// TextNodes have no nested markers. They hold fragments of text between markers
type TextNode struct {
	DataNode
}

func (tn *TextNode) ToString(buf *bytes.Buffer, doMarkers bool) {
	buf.WriteString(tn.Data)
}

// Doctypes currently ignore markers within.
type Doctype struct {
	DataNode
}

func (d *Doctype) ToString(buf *bytes.Buffer, doMarkers bool) {
	fmt.Fprintf(buf, "<!DOCTYPE %s>", d.Data)
}

type DataNode struct {
	Data string
}

// Needs only a name and a collection of ChildNodes
type Marker struct {
	ParentNode
	DataNode        // for marker comment
	Name            string
	IsPartial       bool   // Used for "partial application" fields
	StaticHTMLPath  string // Used to inject static HTML directly
	MarkersToFill   string // JSON to fill markers of statically injected HTML
}

func (m *Marker) ToString(buf *bytes.Buffer, doMarkers bool) {
	if doMarkers {
		fmt.Fprintf(buf, "{{%s", m.Name)
		if len(m.ChildNodes) > 0 {
			buf.WriteString("::")
		}
	}
	m.ParentNode.ToString(buf, doMarkers)
	if doMarkers {
		buf.WriteString("}}")
	}
}

// Like a marker, but can also have attributes.
type Element struct {
	ParentNode
	Attributes []*AttrNode
	Name       string
}

func (e *Element) SimpleAddAttr(name, value string) {
	e.Attributes = append(e.Attributes, &AttrNode{
		ParentNode: ParentNode{
			ChildNodes: []NodeI{
				&TextNode{DataNode: DataNode{Data: name}},
				EqualSign{},
				&TextNode{DataNode: DataNode{Data: value}},
			},
		},
	})
}

// TODO: warn about and remove attributes with duplicate names (case-insensitive)

func (e *Element) RemoveDuplicateAttrNames() {
	names := make(map[string]bool, len(e.Attributes))

	for i := 0; i < len(e.Attributes); i++ {
		p, _, hasMarker := e.Attributes[i].GetNameTextParts()

		if hasMarker { // Don't bother with names that have markers.
			continue
		}

		lower := strings.ToLower(p[0])
		if names[lower] { // Warn about duplicate and remove it
			log.Printf("Duplicate attribute names are not valid. Removing: %q\n", lower)

			copy(e.Attributes[i:], e.Attributes[i+1:])

			last := len(e.Attributes) - 1
			e.Attributes = e.Attributes[0:last:last]
			i--

		} else {
			names[lower] = true
		}
	}
}

func (e *Element) ToString(buf *bytes.Buffer, doMarkers bool) {
	var upperName = strings.ToUpper(e.Name)

	buf.WriteString("<" + upperName)
	for _, attr := range e.Attributes {
		buf.WriteByte(' ')
		attr.ToString(buf, doMarkers)
	}
	buf.WriteString(">")

	if EmptyElements[upperName] == false {
		e.ParentNode.ToString(buf, doMarkers)
		fmt.Fprintf(buf, "</%s>", upperName)
	}
}

//  Tag eliding
//
//  http://www.w3.org/html/wg/drafts/html/master/syntax.html#syntax-tag-omission

func (e *Element) FirstChild() NodeI {
	if e.IsEmpty() {
		return nil
	}
	return e.ChildNodes[0]
}

func IsElemNamed(node NodeI, s ...string) bool {
	var elem, ok = node.(*Element)
	if ok == false {
		return false
	}
	var name = strings.ToUpper(elem.Name)

	for _, item := range s {
		if strings.ToUpper(item) == name {
			return true
		}
	}
	return false
}

func (e *Element) Next(parent NodeI, skipCommentEmptyTxt bool) NodeI {
	par, ok := parent.(ParentI)
	children := par.GetChildNodes()

	if ok == false || len(children) < 2 {
		return nil
	}

	selfIdx := -1
	for i, ch := range children[0 : len(children)-1] {
		if chE, ok := ch.(*Element); ok && chE == e {
			selfIdx = i
		}
	}
	if selfIdx == -1 {
		return nil
	}

	if !skipCommentEmptyTxt {
		return children[selfIdx+1]
	}

	// Return the next that is neither a comment nor an empty text node.
	for selfIdx++; selfIdx < len(children); selfIdx++ {
		switch n := children[selfIdx].(type) {
		case *CommentNode:
			continue
		case *TextNode:
			if len(n.DataNode.Data) == 0 {
				continue
			}
		}

		return children[selfIdx]
	}

	return nil
}

var xx = 0

// NextOrParentIsMarker returns `true` if the next node is a Marker or if the
// parent is a Marker and the Element is the last child, which would mean that
// then end of that parent Marker follows this element.
func (e *Element) NextOrParentIsMarker(parent NodeI) bool {
	n := e.Next(parent, true)

	if n == nil { // Must be the last child, so return `true` if parent is Marker
		_, parMrkr := parent.(*Marker)
		return parMrkr
	}

	_, nxtMrkr := n.(*Marker)
	return nxtMrkr // Returns `true` if the next item is a Marker
}

func TextStartsWithWhite(node NodeI) bool {
	var tn, ok = node.(*TextNode)
	if ok == false {
		return false
	}
	return len(tn.Data) > 0 && byteIsWhite(tn.Data[0])
}
func IsComment(node NodeI) bool {
	_, ok := node.(*CommentNode)
	return ok
}
func IsElement(node NodeI) bool {
	_, ok := node.(*Element)
	return ok
}

func (e *Element) IsEmpty() bool {
	return len(e.ChildNodes) == 0
}

func (e *Element) CanElideOpenTag(parent NodeI) bool {
	if parent == nil {
		return false
	}

	var name = strings.ToUpper(e.Name)

	switch n := parent.(type) {
	case *GenericRoot:
		// An HTML element's start tag may be omitted if the first thing inside the
		// HTML element is not a comment.
		return name == "HTML" && !IsComment(e.Next(parent, false))

	case *Element:
		if strings.ToUpper(n.Name) != "HTML" {
			return false
		}

		switch name {
		case "HEAD":
			// A HEAD element's start tag may be omitted if the element is empty, or
			// if the first thing inside the HEAD element is an element.
			return e.IsEmpty() || IsElement(e.FirstChild())

		case "BODY":
			// A body element's start tag may be omitted if the element is empty, or
			// if the first thing inside the body element is not a space character or
			// a comment, except if the first thing inside the body element is a
			// script or style element.
			if e.IsEmpty() {
				return true
			}
			var fc = e.FirstChild()
			return !TextStartsWithWhite(fc) && !IsComment(fc) &&
				!IsElemNamed(fc, "META", "LINK", "SCRIPT", "STYLE", "TEMPLATE")
		}
	}
	return false
}

func (e *Element) CanElideCloseTag(parent NodeI) bool {
	if parent == nil || e.NextOrParentIsMarker(parent) {
		return false
	}

	// An html element's end tag may be omitted if the html element is not
	// immediately followed by a comment.
	switch strings.ToUpper(e.Name) {
	case "HTML":
		return !IsComment(e.Next(parent, false))

		// A head element's end tag may be omitted if the head element is not
		// immediately followed by a space character or a comment.
	case "HEAD":
		// The not `FORM` test is for the sake of IE9 and lower
		var n = e.Next(parent, false)
		return !IsElemNamed(n, "FORM") && !IsComment(n) && !TextStartsWithWhite(n)

		// A body element's end tag may be omitted if the body element is not
		// immediately followed by a comment.
	case "BODY":
		return !IsComment(e.Next(parent, false))

		// An li element's end tag may be omitted if the li element is immediately
		// followed by another li element or if there is no more content in the parent
		// element.
	case "LI":
		var n = e.Next(parent, true)
		return n == nil || IsElemNamed(n, "LI")

		// A dt element's end tag may be omitted if the dt element is immediately
		// followed by another dt element or a dd element.
	case "DT":
		return IsElemNamed(e.Next(parent, true), "DT", "DD")

		// A dd element's end tag may be omitted if the dd element is immediately
		// followed by another dd element or a dt element, or if there is no more
		// content in the parent element.
	case "DD":
		var n = e.Next(parent, true)
		return n == nil || IsElemNamed(n, "DD", "DT")

		// A p element's end tag may be omitted if the p element is immediately followed
		// by an address, article, aside, blockquote, dir, div, dl, fieldset, footer,
		// form, h1, h2, h3, h4, h5, h6, header, hgroup, hr, menu, nav, ol, p, pre,
		// section, table, or ul, element, or if there is no more content in the parent
		// element and the parent element is not an a element.
	case "P":
		var n = e.Next(parent, true)
		return parent != nil && !IsElemNamed(parent, "A") &&
			(n == nil || IsElemNamed(n,
				"ADDRESS", "ARTICLE", "ASIDE", "BLOCKQUOTE", "DIR",
				"DIV", "DL", "FIELDSET", "FOOTER",
				// "FORM" (IE HAS TROUBLE) ,
				"H1", "H2", "H3", "H4", "H5", "H6", "HEADER",
				"HGROUP", "HR", "MENU", "NAV", "OL", "P", "PRE",
				"SECTION", "TABLE", "UL"))

		// An rt element's end tag may be omitted if the rt element is immediately
		// followed by an rt or rp element, or if there is no more content in the parent
		// element.
	case "RT":
		var n = e.Next(parent, true)
		return n == nil || IsElemNamed(n, "RT", "RP")

		// An rp element's end tag may be omitted if the rp element is immediately
		// followed by an rt or rp element, or if there is no more content in the parent
		// element.
	case "RP":
		var n = e.Next(parent, true)
		return n == nil || IsElemNamed(n, "RT", "RP")

		// An optgroup element's end tag may be omitted if the optgroup element is
		// immediately followed by another optgroup element, or if there is no more
		// content in the parent element.
	case "OPTGROUP":
		var n = e.Next(parent, true)
		return n == nil || IsElemNamed(n, "OPTGROUP")

		// An option element's end tag may be omitted if the option element is
		// immediately followed by another option element, or if it is immediately
		// followed by an optgroup element, or if there is no more content in the parent
		// element.
	case "OPTION":
		var n = e.Next(parent, true)
		return n == nil || IsElemNamed(n, "OPTION", "OPTGROUP")

		// A colgroup element's start tag may be omitted if the first thing inside the
		// colgroup element is a col element, and if the element is not immediately
		// preceded by another colgroup element whose end tag has been omitted. (It
		// can't be omitted if the element is empty.)

		// A colgroup element's end tag may be omitted if the colgroup element is not
		// immediately followed by a space character or a comment.
	case "COLGROUP":
		var n = e.Next(parent, false)
		return !IsComment(n) && !TextStartsWithWhite(n)

		// A thead element's end tag may be omitted if the thead element is immediately
		// followed by a tbody or tfoot element.
	case "THEAD":
		return IsElemNamed(e.Next(parent, true), "TBODY", "TFOOT")

		// A tbody element's start tag may be omitted if the first thing inside the
		// tbody element is a tr element, and if the element is not immediately preceded
		// by a tbody, thead, or tfoot element whose end tag has been omitted. (It can't
		// be omitted if the element is empty.)

		// A tbody element's end tag may be omitted if the tbody element is immediately
		// followed by a tbody or tfoot element, or if there is no more content in the
		// parent element.
	case "TBODY":
		var n = e.Next(parent, true)
		return n == nil || IsElemNamed(n, "TBODY", "TFOOT")

		// A tfoot element's end tag may be omitted if the tfoot element is immediately
		// followed by a tbody element, or if there is no more content in the parent
		// element.
	case "TFOOT":
		var n = e.Next(parent, true)
		return n == nil || IsElemNamed(n, "TBODY")

		// A tr element's end tag may be omitted if the tr element is immediately
		// followed by another tr element, or if there is no more content in the parent
		// element.
	case "TR":
		var n = e.Next(parent, true)
		return n == nil || IsElemNamed(n, "TR")

		// A td element's end tag may be omitted if the td element is immediately
		// followed by a td or th element, or if there is no more content in the parent
		// element.
	case "TD":
		var n = e.Next(parent, true)
		return n == nil || IsElemNamed(n, "TD", "TH")

		// A th element's end tag may be omitted if the th element is immediately
		// followed by a td or th element, or if there is no more content in the parent
		// element.
	case "TH":
		var n = e.Next(parent, true)
		return n == nil || IsElemNamed(n, "TD", "TH")
	}
	return false
}

func byteIsWhite(b byte) bool {
	return utf8.ValidString(string(b)) && unicode.IsSpace(rune(b))
}
func trimL(s string) string {
	return strings.TrimLeftFunc(s, unicode.IsSpace)
}
func trimR(s string) string {
	return strings.TrimRightFunc(s, unicode.IsSpace)
}

/*
func getSemanticParent(name string) []string {
	switch name {
	case "HEAD",
		"BODY":
		return []string{"HTML"}
	case "LI":
		return []string{"UL", "OL"}
	case "DT",
		"DD":
		return []string{"DL"}
	case "RT",
		"RP":
		return []string{"RUBY"}
	case "OPTGROUP":
		return []string{"SELECT"}
	case "OPTION":
		return []string{"OPTGROUP", "SELECT"}
	case "COLGROUP",
		"THEAD",
		"TBODY",
		"TFOOT":
		return []string{"TABLE"}
	case "TR":
		return []string{"TBODY", "THEAD", "TFOOT", "TABLE"}
	case "TD",
		"TH":
		return []string{"TR"}
	default:
		return nil
	}
}
*/

/*
   UNKNOWN = (1, "[[UNKNOWN]]")
   ROOT = "#root" // This is a generic container to hold the result
   TEXT = "#text"
   COMMENT = "#comment"
   DOCTYPE = "#doctype"
   FRAGMENT = "#document-fragment"
   PLACEHOLDER = "#placeholder" //custom
   A, ABBR, ACRONYM, ADDRESS, APPLET, AREA, ARTICLE, ASIDE, AUDIO
   B, BASE, BASEFONT, BDI, BDO, BGSOUND, BIG, BLINK, BLOCKQUOTE, BODY, BR,
     BUTTON
   CANVAS, CAPTION, CENTER, CITE, CODE, COL, COLGROUP, COMMAND, CONTENT
   DATA, DATALIST, DD, DEL, DETAILS, DFN, DIALOG, DIR, DIV, DL, DT
   ELEMENT, EM, EMBED
   FIELDSET, FIGCAPTION, FIGURE, FONT, FOOTER, FORM, FRAME, FRAMESET
   H1, H2, H3, H4, H5, H6, HEAD, HEADER, HGROUP, HR, HTML
   I, IFRAME, IMAGE, IMG, INPUT, INS, ISINDEX
   KBD, KEYGEN, LABEL, LEGEND, LI, LINK, LISTING
   MAIN, MAP, MARK, MARQUEE, MENU, MENUITEM, META, METER, MULTICOL
   NAV, NOBR, NOEMBED, NOFRAMES, NOSCRIPT
   OBJECT, OL, OPTGROUP, OPTION, OUTPUT
   P, PARAM, PICTURE, PLAINTEXT, PRE, PROGRESS
   Q
   RP, RT, RTC, RUBY
   S, SAMP, SCRIPT, SECTION, SELECT, SHADOW, SMALL, SOURCE, SPACER, SPAN
     STRIKE, STRONG, STYLE, SUB, SUMMARY, SUP
   TABLE, TBODY, TD, TEMPLATE, TEXTAREA, TFOOT, TH, THEAD, TIME, TITLE, TR
     TRACK, TT
   U, UL
   VAR, VIDEO
   WBR
   XMP
*/

/*
const (
	HTML = ResultKind(iota)
	FullPh
	StartPh
	EndPh
)

type ResultKind int

type ResultItem struct {
	Kind ResultKind `json:"kind"`
	Data string     `json:"data"`
}
*/

type MarkerData struct {
	Name, Comment, DefValue string
	End                     int
	IsPartial               bool
	StaticHTMLPath          string
	MarkersToFill           string
}

// MarkerData stores the name and start/end indices for a HTML marker
type MarkerIndices struct {
	Name      string
	Pair      *[2]uint16
	IsPartial bool
}

type StaticHTMLInjection__ struct {
	LocalIndex    int    // Needs the component's start_index added to it
	Data          string // Path to the statically inserted component
	MarkersToFill string // JSON describing values for markers
}

func GetMarkerData(root *GenericRoot) (
	markers []*MarkerIndices,
	htmlArray []string,
	staticHTMLInjections []StaticHTMLInjection__) {

	const errInvalidAttr = "Internal error; Only Marker, Text and EqualSign are allowed in Attributes"

	uniqueMarkers := make(map[string]bool, len(markers))
	buf := bytes.Buffer{}

	markers = []*MarkerIndices{}

	var doNode func(n NodeI, parent NodeI, inAttr bool, attrQuot byte)

	doNode = func(n NodeI, parent NodeI, inAttr bool, attrQuot byte) {

		switch nn := n.(type) {

		case *Marker:
			htmlArray = append(htmlArray, buf.String())
			buf.Reset()

			if len(nn.StaticHTMLPath) > 0 { // Path to Static HTML to inject
				staticHTMLInjections = append(staticHTMLInjections, StaticHTMLInjection__{
					LocalIndex:    len(htmlArray) - 1,
					Data:          nn.StaticHTMLPath,
					MarkersToFill: nn.MarkersToFill,
				})
				return
			}

			// Otherwise, create the needed Marker indices.
			pair := [2]uint16{
				uint16(len(htmlArray)),
				uint16(len(htmlArray)),
			}

			lName := strings.ToLower(nn.Name)

			if uniqueMarkers[lName] {
				panic(fmt.Sprintf(
					"Markers must be unique case-insensitive. Found multiple: %q", lName))
			} else {
				uniqueMarkers[lName] = true
			}

			markers = append(markers, &MarkerIndices{
				Name:      nn.Name,
				Pair:      &pair,
				IsPartial: nn.IsPartial,
			})

			if len(nn.ChildNodes) > 0 {
				for _, child := range nn.ChildNodes {
					doNode(child, n, inAttr, attrQuot)
				}

				// Even if it's an empty string, it's needed
				htmlArray = append(htmlArray, buf.String())
				buf.Reset()

				pair[1] = uint16(len(htmlArray))
			}

		case *TextNode:
			nn.ToString(&buf, false)

		case EqualSign, *EqualSign:
			if !inAttr {
				panic(errInvalidAttr)
			}
			nn.ToString(&buf, false)
			if attrQuot != 0 {
				buf.WriteByte(attrQuot)
			}

		case *CommentNode:
			if inAttr {
				panic(errInvalidAttr)
			}
			nn.ToString(&buf, false)

		case *Doctype:
			if inAttr {
				panic(errInvalidAttr)
			}
			nn.ToString(&buf, false)

		case *Element:
			if inAttr {
				panic(errInvalidAttr)
			}

			upperName := strings.ToUpper(nn.Name)

			if len(nn.Attributes) > 0 || !nn.CanElideOpenTag(parent) {
				fmt.Fprintf(&buf, "<%s", upperName)

				for _, attr := range nn.Attributes {
					buf.WriteByte(' ')

					for _, attrPart := range attr.ChildNodes { // process each attr part
						doNode(attrPart, nn, true, attr.Quote)
					}

					if attr.Quote != 0 { // Write the closing quote, if any
						buf.WriteByte(attr.Quote)
					}
				}

				buf.WriteByte('>')
			}

			for _, child := range nn.ChildNodes {
				doNode(child, nn, false, 0)
			}

			if EmptyElements[upperName] == false && !nn.CanElideCloseTag(parent) {

				fmt.Fprintf(&buf, "</%s>", upperName)
			}

		default:
			panic(fmt.Sprintf("Internal error; Unknown NodeI type: %#v\n", nn))
		}
	}
	for _, node := range root.GetChildNodes() {
		doNode(node, root, false, 0)
	}

	// Needed even if it's an empty string
	htmlArray = append(htmlArray, buf.String())

	return
}
