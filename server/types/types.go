package types

type CSSJsTagData struct {
	Tags []byte
	Path string
}

type BasicCompRequest struct {
	ServerPath          string // for server route and SiteMap
	ValidationJSON      string
	RootIndex           uint16
	IsPage              bool
	AllowTails          bool
	SubRoutesAllowTails bool
}

type SiteMapNode struct {
	// Sort order of sibling nodes in SiteMap
	Order int

	// DisplayName in SiteMapNode
	DisplayName string

	// Description in SiteMapNode
	Description string
}

type FullSiteMapNode struct {
	SiteMapNode
	Path     string
	Children []*FullSiteMapNode
}

// TODO: How to handle response type (HTML,JSON,etc)
type Config struct {
	SiteMapNode

	// Marks a CSS/JS accumulation point
	IsRoot bool

	// Respond to paths deeper than the path of this route when no route to
	// the deeper path exists.
	AllowTails bool
}
