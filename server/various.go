package frost

import (
	util "Frost/utilities"
)

/********************

	Markers

********************/

// Marker represents position data corresponding to a user defined marker.
type Marker struct {
	marker [2]uint16
}

func (m Marker) hasContent() bool {
	return m.marker[0] != m.marker[1]
}

// NewMarker____ is to called internally only; never in user code.
func NewMarker____(a, b uint16) Marker {
	return Marker{marker: [2]uint16{a, b}}
}

/********************

	Components

********************/

/*
Component describes the base methods required to be a valid component. The
methods of Component are to be called internally only; never directly within
user code.
*/
type Component interface {
	Initialize____() [][]byte
	Make(*Response)
}

// String lets you use a string where a component is expected.
type String string

func (s String) Initialize____() [][]byte {
	return nil
}
func (s String) Make(r *Response) {
	if setMarkerIndices(r, r.idx) {
		addEscapeHTMLString(r, string(s))
	}
}

// StringNoEscape lets you use a string where a Component is expected. The string
// does not get HTML escaped.
type StringNoEscape string

func (s StringNoEscape) Initialize____() [][]byte {
	return nil
}
func (s StringNoEscape) Make(r *Response) {
	if setMarkerIndices(r, r.idx) {
		r.writer.Write(util.StrToBytes(string(s)))
	}
}

/*
Bytes lets you use a []byte where a component is expected.
*/
type Bytes []byte

func (Bytes) Initialize____() [][]byte {
	return nil
}
func (b Bytes) Make(r *Response) {
	if setMarkerIndices(r, r.idx) {
		addEscapeHTMLString(r, util.BytesToStr(b))
	}
}

/*
BytesNoEscape lets you use a []byte where a component is expected. The bytes
do not get HTML escaped.
*/
type BytesNoEscape []byte

func (BytesNoEscape) Initialize____() [][]byte {
	return nil
}
func (b BytesNoEscape) Make(r *Response) {
	if setMarkerIndices(r, r.idx) {
		r.writer.Write(b)
	}
}

/********************

  Utilities

********************/

/*
EscapeHTML receives a string, replaces characters that may cause an XSS
attack with the appropriate HTML entity, and returns the resulting string.
*/
func EscapeHTML(original string) string {
	var result []byte
	var last = 0
	var escaped string

	for i, c := range original {
		switch c {
		case '"':
			escaped = "&#34;"
		case '\'':
			escaped = "&#39;"
		case '&':
			escaped = "&amp;"
		case '<':
			escaped = "&lt;"
		case '>':
			escaped = "&gt;"
		default:
			continue
		}

		// We don't allocate unless at least one character was found
		if result == nil {
			result = make([]byte, 0, len(original)+240)
		}
		result = append(append(result, original[last:i]...), escaped...)
		last = i + 1
	}

	if result == nil {
		return original
	}

	if last != len(original) {
		result = append(result, original[last:]...)
	}
	return util.BytesToStr(result)
}
