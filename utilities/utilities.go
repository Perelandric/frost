package utilities

import (
	"archive/tar"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"reflect"
	"runtime"
	"strings"
	"unsafe"
)

const (
	// Pages is the name of the directory that holds page components
	Pages = "pages"
	// Frags is the name of the directory that holds fragment components
	Frags = "frags"
	// App is the name of the directory that holds settings and arbitrary code
	App = "app"
)

// DirPerms is a constant used when opening directory files
const DirPerms = os.ModeDir | os.ModePerm

var ErrServerCommunication = fmt.Errorf("trouble communicating with front end server")
var ErrInvalidCommunication = fmt.Errorf("invalid data from front end server")

func StrToBytes(s string) []byte {
	return *(*[]byte)(unsafe.Pointer(
		&reflect.SliceHeader{
			Data: (*reflect.StringHeader)(unsafe.Pointer(&s)).Data,
			Len:  len(s),
			Cap:  len(s),
		}),
	)
}

func BytesToStr(b []byte) string {
	return *(*string)(unsafe.Pointer(&b))
}

// Trace Functions
func Enter() string {
	fnName := "<unknown>"
	// Skip this function, and fetch the PC and file for its parent
	pc, _, _, ok := runtime.Caller(1)
	if ok {
		fnName = runtime.FuncForPC(pc).Name()
		idx := strings.LastIndexByte(fnName, '.')

		if idx != -1 {
			idx2 := strings.LastIndexByte(fnName[0:idx], '.')

			if idx2 != -1 {
				fnName = fnName[idx2:]
			} else {
				fnName = fnName[idx:]
			}
		}
	}

	fmt.Printf("Entering %s\n", fnName)
	return fnName
}

func Exit(s string) {
	fmt.Printf("Exiting  %s\n", s)
}

// TODO gives a standard message for features not yet implemented
var todoMap = map[string]bool{}

func TODO(feature string) {
	var s = fmt.Sprintf("TODO: %q is not yet implemented", feature)

	var pc, file, line, ok = runtime.Caller(1)
	if ok {
		var fn = runtime.FuncForPC(pc)
		if fn != nil {
			s += fmt.Sprintf("\n    %s", fn.Name())
		}
		s += fmt.Sprintf("\n    (file:%s, line:%d)", file, line)
	}
	if todoMap[s] == false {
		todoMap[s] = true
		fmt.Println(s)
	}
}

// Untar the `tarPath` into the `dest` path
func Untar(tarPath string, dest string) error {
	t, err := os.Open(tarPath)
	if err != nil {
		return err
	}

	r := tar.NewReader(t)

	err = os.Mkdir(filepath.Join(dest, "server"), DirPerms)
	if err != nil && !os.IsExist(err) {
		return err
	}

	var n *tar.Header

	for n, err = r.Next(); err == nil; n, err = r.Next() {
		destName := filepath.Join(dest, n.Name)
		var dstFile *os.File

		if n.FileInfo().IsDir() {
			err = os.Mkdir(destName, DirPerms)
			if os.IsExist(err) {
				err = nil
			}

		} else if dstFile, err = os.Create(destName); err != nil {

		} else if _, err = io.Copy(dstFile, r); err != nil {

		} else {
			err = dstFile.Close()
		}

		if err != nil {
			return err
		}
	}

	if err != io.EOF {
		return err
	}

	// Make sure the server binary is executable
	return os.Chmod(filepath.Join(dest, "server", "server"), 0777)
}

func WriteFileToTar(
	tw *tar.Writer,
	info os.FileInfo,
	leadingPath string,
	localPath string,
	remove bool,
) error {

	header, err := tar.FileInfoHeader(info, "")
	if err != nil {
		return err
	}
	header.Name = filepath.Join("server", localPath)

	if err = tw.WriteHeader(header); err != nil {
		return err
	}

	if info.IsDir() {
		return nil
	}

	fullPath := filepath.Join(leadingPath, localPath)

	file, err := os.Open(fullPath)
	if err != nil {
		return err
	}
	defer file.Close()

	if _, err = io.Copy(tw, file); err != nil {
		return err
	}

	if remove {
		return os.Remove(fullPath)
	}
	return nil
}

func WriteDirToTar(tw *tar.Writer, fullPath string, remove bool) error {
	fullPath = strings.TrimRight(fullPath, string(filepath.Separator))
	if len(fullPath) == 0 {
		return fmt.Errorf("can not 'tar' a root directory")
	}

	var leadingPath = filepath.Dir(fullPath)
	var stripLen = len(leadingPath) + 1

	err := filepath.Walk(
		fullPath,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}

			return WriteFileToTar(tw, info, leadingPath, path[stripLen:], false)
		},
	)

	if err != nil {
		return err
	}

	if remove {
		return os.RemoveAll(fullPath)
	}
	return nil
}

// Empty is a utility to clear the content of the given directory path.
func Empty(dir string) error {
	var file, err = GetDir(dir, true, false)
	if Check(err) {
		return err
	}
	children, err := file.Readdirnames(-1)
	if CheckNot(err, io.EOF) {
		return err
	}
	for _, name := range children {
		var e = os.RemoveAll(filepath.Join(dir, name))
		if Check(e) && err == nil {
			// Hold the first error found for the return, but keep going.
			err = e
		}
	}
	return err
}

// VerifyDir verifies that a directory for the given path exists and is empty if
// the `mustBeEmpty` flag is set
func VerifyDir(pth string, mustBeEmpty bool) error {
	var f, err = GetDir(pth, true, mustBeEmpty)
	if Check(err) {
		return err
	}
	f.Close()
	return nil
}

// FileExists verifies that the file exists and is not a directory
func FileExists(pth string) bool {
	var f, err = os.Open(pth)
	if os.IsNotExist(err) {
		return false
	}

	if err != nil {
		log.Printf("FileExists found error: %s\n", err.Error())
		return false
	}
	defer f.Close()

	info, err := f.Stat()
	if err != nil {
		log.Printf("FileExists found error: %sw\n", err.Error())
		return false
	}

	return !info.IsDir()
}
func DirExistsDirEmpty(pth string) (bool, bool) {
	if d, wasEmpty, err := getDir(pth, true); err != nil || d == nil {
		return false, false
	} else {
		d.Close()
		return true, wasEmpty
	}
}

var ErrDirNotEmpty = fmt.Errorf("directory is not empty")

// GetDir gets the directory File for the path. If `mustExist` is false, the
// directory is created and returned. It returns an error if the `mustBeEmpty`
// flag is set and the directory was not empty
func GetDir(pth string, mustExist, mustBeEmpty bool) (*os.File, error) {
	d, wasEmpty, err := getDir(pth, mustExist)
	if err != nil {
		return nil, err
	}

	if mustBeEmpty && !wasEmpty {
		return nil, ErrDirNotEmpty
	}
	return d, nil
}

func getDir(pth string, mustExist bool) (*os.File, bool, error) {
	var file, err = os.Open(pth)

	if os.IsNotExist(err) {
		if mustExist {
			return nil, false, fmt.Errorf("Directory %q does not exist.\n", pth)
		}

		err = os.MkdirAll(pth, DirPerms)
		if Check(err) {
			return nil, false, fmt.Errorf("Unexpected error: %s\n", err)
		}

		return getDir(pth, true) // Grab the newly made directory
	}
	if Check(err) {
		return nil, false, fmt.Errorf("Unexpected error: %s\n", err)
	}

	stat, err := file.Stat()
	if Check(err) {
		file.Close()
		return nil, false, fmt.Errorf("Unexpected error: %s\n", err)
	}

	if stat.IsDir() == false {
		file.Close()
		return nil, false, fmt.Errorf("%q is not a directory.\n", pth)
	}

	names, err := file.Readdirnames(1)
	if CheckNot(err, io.EOF) {
		file.Close()
		return nil, false, fmt.Errorf("Unexpected error: %s\n", err)
	}

	return file, len(names) == 0, nil
}

// SimpleCopy copies the file at `origFile` to the `newFile` path. The target
// directory must exist.
func SimpleCopy(origFile, newFile string) error {
	var f, err = os.Open(origFile)
	defer f.Close()
	if Check(err) {
		return err
	}

	if err := os.MkdirAll(filepath.Dir(newFile), DirPerms); err != nil {
		return err
	}

	newF, err := os.Create(newFile)
	if Check(err) {
		return err
	}
	defer newF.Close()

	_, err = io.Copy(newF, f)
	if Check(err) {
		return err
	}
	return nil
}

// DeepCopy copies the content of `origDir` into the `targetDir` path. If the
// `targetDir` does not exist, it is created. If `includeOuterDir` is `true`,
// then the top directory of `origDir` is also created inside `targetDir`.
// It skips hidden files and directories.
func DeepCopy(origDir, targetDir string, includeOuterDir bool) error {

	var f, err = os.Open(origDir)
	defer f.Close()
	if Check(err) {
		return err
	}
	fStat, err := f.Stat()
	if Check(err) {
		return err
	}

	if fStat.IsDir() == false {
		return fmt.Errorf("expected directory")
	}

	if includeOuterDir {
		targetDir = filepath.Join(targetDir, filepath.Base(origDir))
		err = os.MkdirAll(targetDir, DirPerms)
		if Check(err) {
			return err
		}
	}

	return filepath.Walk(origDir, func(fp string, info os.FileInfo, err error) error {
		if Check(err) {
			return err
		}
		var localResource = fp[len(origDir):]
		var target = filepath.Join(targetDir, localResource)

		var hidden = strings.HasPrefix(info.Name(), ".")
		var isDir = info.IsDir()

		if hidden {
			if isDir {
				return filepath.SkipDir
			}
			return nil
		}

		if isDir {
			return os.MkdirAll(target, DirPerms)
		}
		return SimpleCopy(fp, target)
	})
}

type FileStructure struct {
	Path     string
	Content  []byte
	IsDir    bool
	Children []FileStructure
}

func endsWithAny(s string, any []string) bool {
	if len(any) == 0 {
		return true
	}
	for _, ext := range any {
		if strings.HasSuffix(s, ext) {
			return true
		}
	}
	return false
}

func Check(err error) bool {
	return Error{}.Check(err)
}

type ErrorChecker interface {
	error
	Check(interface{}, ...interface{}) bool
}

type Error struct {
	Err error
	msg []string
}

func (err Error) Check(arg interface{}, args ...interface{}) bool {
	var last interface{}
	if len(args) == 0 {
		last = arg
	} else {
		last = args[len(args)-1]
	}
	if last == nil {
		return false
	}
	var chk, ok = last.(Error)
	if ok {
		if chk.Err == nil {
			return false
		}
		err.addMsg()
		return true
	}
	e, ok := last.(error)
	if ok {
		err.Err = e
		err.addMsg()
		return true
	}
	panic("Expected `error` as last argument")
}

// Check check the given `error` and provides info to locate the issue.
func (err *Error) addMsg() {
	if len(err.msg) == 0 {
		if err.Err == nil {
			panic("Internal error")
		}
		err.msg = append(err.msg, fmt.Sprintf("Error: %s\n", err.Err))
	}
	pc, file, line, ok := runtime.Caller(1)
	if ok {
		var s = ""
		var fn = runtime.FuncForPC(pc)
		if fn != nil {
			s += fmt.Sprintf("    %s::", fn.Name())
		}
		s += fmt.Sprintf(" (FILE:%s, LINE:%d)\n", file, line)
		err.msg = append(err.msg, s)
	}
}

func CheckIs(err error, any ...error) bool {
	for _, e := range any {
		if err == e {
			return Error{}.Check(err)
		}
	}
	return false
}

func CheckNot(err error, any ...error) bool {
	for _, e := range any {
		if err == e {
			return false
		}
	}
	return Error{}.Check(err)
}

func (err Error) String() string {
	return strings.Join(err.msg, "")
}

/*
func SendNetstr(w io.Writer, data []byte) (err error) {
	var leng = len(data)
	if leng == 0 {
		return nil
	}

	var b = make([]byte, 0, 4)

	for leng != 0 {
		b = append(b, byte((leng%10)+48))
		leng = int(leng / 10)
	}

	for i, j := 0, len(b)-1; i < j; i, j = i+1, j-1 {
		b[i], b[j] = b[j], b[i]
	}

	if _, err = w.Write(append(b, ':')); err != nil {
		return err
	}

	_, err = w.Write(data)
	return err
}

func ReadNetstringWithBuf(r *bufio.Reader, buf []byte) ([]byte, error) {

	const NETSTR_SIZE_MAX_LEN = 12 // 12 is plenty long for the netstring's `123:`
	const INVALID_NETSTR = "Invalid netstring %s: %q\n"

	var s, err = r.ReadSlice(':')
	var length = 0

	if err != nil {
		return nil, err
	}

	if len(s) < 2 || NETSTR_SIZE_MAX_LEN < len(s) {
		return nil, fmt.Errorf(INVALID_NETSTR, "size", s)
	}

	for _, c := range s { // parse the netstring length
		if '0' <= c && c <= '9' {
			length = (length * 10) + (int(c) - 48)

		} else if c == ':' {
			break

		} else {
			return nil, fmt.Errorf(INVALID_NETSTR, "length digits", s)
		}
	}

	if length > cap(buf) { // Size the buf to the size of the netstring
		buf = make([]byte, length, length)

	} else {
		buf = buf[0:length]
	}

	n, err := r.Read(buf) // Read the netstring payload
	if err != nil {
		return nil, err
	}
	if n < length {
		return nil, fmt.Errorf(
			"Netstring length, expected: %d, received: %d, data: %q\n",
			length, n, buf[0:n])
	}

	return buf[0:length], nil
}

// Reads a single netstring and passes its ...\u0000 segments to the callback
func NetstringSegmentLoop(
	r *bufio.Reader, buf []byte, cb func(byte, []byte) error) error {

	buf, err := ReadNetstringWithBuf(r, buf)
	if err != nil {
		return err
	}

	// read the `flag:content\u0000` segements from the `buf`
	var i = 0
	var idx = 0

	for i < len(buf) {
		idx = bytes.IndexByte(buf[i:], '\u0000')

		switch idx {
		case -1: // If not found...
			return ErrInvalidCommunication

		case 0: // Adjacent NULL bytes. Shouldn't happen, so just log and continue
			log.Printf("Adjacent NULL bytes received: %q", buf)

		default:
			if err := cb(buf[i], buf[i+1:i+idx]); err != nil { // Invoke `cb` with segment.
				return err
			}

			i += idx + 1
		}
	}
	return nil
}

func Union(arrays ...[]string) (res []string) {
	var n = 0
	for _, arr := range arrays {
		n += len(arr)
	}
	res = make([]string, 0, n)

	for _, arr := range arrays {
	OUTER:
		for _, item := range arr {
			for _, resItem := range res {
				if item == resItem {
					continue OUTER
				}
			}
			res = append(res, item)
		}
	}
	return res[0:len(res):len(res)]
}
*/
